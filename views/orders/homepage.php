<div ng-controller="OrdersController" ng-init="initializeController()">
	 <div class="row">
		<div class="col-lg-12">
		   
		<h1 class="pull-left">Orders</h1>
		<h3 class="pull-right text-right total"><span>Total</span> <div>{{OrdersAmount  | currency:""}}</div></h3>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="input-group input-group-lg">
				 <div class="input-group-btn">
					<button type="button" class="btn btn-default" ng-click="toggleEdit('edit')" ng-hide="editMode" ng-disabled="deleteMode || !OrdersItems.length"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-click="toggleEdit('save')" ng-show="editMode"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-click="toggleDelete('delete')" ng-hide="deleteMode" ng-disabled="editMode || !OrdersItems.length"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-click="toggleDelete('save')" ng-show="deleteMode"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-click="resetVendorObject()" ng-disabled="VendorObject.id==undefined"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></button>
				</div>
				<div class=" typeahead-wrapper">
				 <input class="form-control input-lg" type="text" options="typeAheadOptions" datasets="typeAheadData" ng-readonly="VendorObject.id!=undefined" ng-model="VendorObjectTypeahead" ng-blur="validateVendor(VendorObject)" class="typeahead input-lg" sf-typeahead  placeholder="Supplier Name" />
				 </div>
				  <div class="input-group-btn">
				 <button type="button" class="btn btn-default" ng-click="resetOrders()"  ng-disabled="editMode||deleteMode||!OrdersItems.length"><span class="glyphicon glyphicon-remove"></span></button>
				 </div>
			  </div>
			</div>
		</div>
		<div class="row table-data">
			<div class="col-sm-12">
			 <table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th ng-show="deleteMode" >&nbsp;</th>
						<th>Product</th>
						<th style="width:20%">Capital</th>
						<th>Quantity</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="item in OrdersItems" ng-class="{danger:item.is_delete}">
						<td ng-show="deleteMode" ><input type="checkbox" ng-model="item.is_delete" ng-checked="item.is_delete" /></td>
						<td>{{item.display_title}}</td>
						<td class="numeric">
							<span ng-hide="editMode">{{item.capital | currency:""}}</span>
							<input  ng-show="editMode" type="number" auto-select ng-model="item.capital" class="input-sm" />
						</td>
						<td class="numeric"> 
							<span ng-hide="editMode">{{item.quantity}}</span>
							<input  ng-show="editMode" type="number" auto-select ng-model="item.quantity" class="input-sm" />
						</td>
						<td class="numeric">
							<span  ng-hide="editMode">{{item.amount | currency:""}}</span>
							<span  ng-show="editMode">{{(item.quantity * item.capital)| currency:"" }}</span>
						</td>
					</tr>
				</tbody>
				<tfoot ng-if="!OrdersItems.length"> 
					<tr>
						<td class="text-center" colspan="5">No items added yet.</td>
					</tr>
				</tfoot>
			   </table>
			</div>
		</div>
		<button type="button" class="btn btn-primary btn-md btn-fab top right" ng-click="openTransactionModal()" ng-disabled="transactionInProgress || (!OrdersAmount || !VendorObject) || (deleteMode||editMode)"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
		<div ng-include="'views/shared/transactionModal.php'"></div>
	   <div ng-include="'views/shared/productTypeahead.php'"></div>
	 </div>
</div>