<div ng-controller="SalesController" ng-init="initializeController()">
<div class="row">
	<div class="col-lg-12">
	   
		<h1 class="pull-left">Sales</h1>
		<h3 class="pull-right text-right total"><span>Total</span> <div>{{SalesAmount  | currency:""}}</div></h3>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="input-group input-group-lg">
					 <div class="input-group-btn">
						<button type="button" class="btn btn-default" ng-click="toggleEdit('edit')" ng-hide="editMode" ng-disabled="deleteMode || !SalesItems.length"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
						<button type="button" class="btn btn-default" ng-click="toggleEdit('save')" ng-show="editMode"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
						<button type="button" class="btn btn-default" ng-click="toggleDelete('delete')" ng-hide="deleteMode" ng-disabled="editMode || !SalesItems.length"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
						<button type="button" class="btn btn-default" ng-click="toggleDelete('save')" ng-show="deleteMode"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
						<button type="button" class="btn btn-default" ng-click="resetCustomerObject()" ng-disabled="CustomerObject.id==undefined"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></button>
					</div>
					<div class="typeahead-wrapper">
					 <input type="text" class="form-control input-lg" options="typeAheadOptions" datasets="typeAheadData" ng-readonly="CustomerObject.id!=undefined"  ng-model="CustomerObjectTypeAhead" class="typeahead " sf-typeahead ng-blur="validateCustomer(CustomerObject)" placeholder="Customer Name" />
					 </div>
					 <div class="input-group-btn">
					 <button type="button" class="btn btn-default" ng-click="resetSales()"  ng-disabled="editMode||deleteMode||!SalesItems.length"><span class="glyphicon glyphicon-remove"></span></button>
					 </div>
				  </div>
			</div>
		</div>
		<div class="row table-data">
			<div class="col-sm-12">
			   <table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th ng-show="deleteMode" >&nbsp;</th>
						<th>Product</th>
						<th>Unit</th>
						<th>Dimension</th>
						<th>Price</th>
						<th>Quantity</th>
						<th ng-if="0">Code</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="item in SalesItems" ng-class="{danger:item.is_delete}">
						<td ng-show="deleteMode" ><input type="checkbox" ng-model="item.is_delete" ng-checked="item.is_delete" /></td>
						<td>{{item.particular}} {{item.part_no}}</td>
						<td>{{item.unit}}</td>
						<td>{{item.length}}mm &times; {{item.width}}mm</td>
						<td class="numeric">
							<span ng-hide="editMode">{{item.price | currency:""}} <span ng-show="!item.markup">  /  <span  ng-class="{'invalid-amount':!item.markup}">{{item.capital | currency:""}}</span></span></span>
							<input  ng-show="editMode" type="number" ng-model="item.price" class="input-sm" auto-select />
						</td>
						<td class="numeric"> 
							<span ng-hide="editMode">{{item.quantity}}</span>
							<input  ng-show="editMode" type="number" ng-model="item.quantity" class="input-sm" auto-select />
						</td>
						<td ng-if="0" class="numeric"> 
							<span ng-hide="editMode">{{item.code_lord}}</span>
							<input  ng-show="editMode" type="text" ng-model="item.code_lord" class="input-sm" disabled />
						</td>
						<td class="numeric">
							<span  ng-hide="editMode">{{item.amount | currency:""}}</span>
							<span  ng-show="editMode">{{(item.quantity * item.price)| currency:"" }}</span>
						</td>
					</tr>
				</tbody>
				<tfoot ng-if="!SalesItems.length"> 
					<tr>
						<td class="text-center" colspan="7">No items added yet.</td>
					</tr>
				</tfoot>
			   </table>
		</div>
	   </div>
	   <button type="button" class="btn btn-primary btn-md btn-fab top right" ng-click="openTransactionModal()" ng-disabled="transactionInProgress || (!SalesAmount || !CustomerObject) || (deleteMode||editMode)"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
	   <div ng-include="'views/shared/transactionModal.php'"></div>
	   <div ng-include="'views/shared/productTypeahead.php'"></div>
	</div>
</div>
</div>
          