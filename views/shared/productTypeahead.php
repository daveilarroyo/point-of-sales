<footer class="navbar navbar-default navbar-fixed-bottom" id="productTypeahead" ng-controller="productTypeaheadController" ng-init="initializeController()">
	<div class="row">
		<div class="col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
			<form action="" class="form-horizontal">
				<div class="input-group input-group-lg">
				<span class="input-group-btn" ng-if="AllowAddTemporaryProduct">
				 <button type="button" class="btn btn-default custom" ng-click="addTemporaryProduct()"><i class="glyphicon glyphicon-plus"></i></button>
				</span> 
				<div class="form-control typeahead-wrapper drop-up  input-lg ">
				 <input type="text" ng-disabled="transactionInProgress || ProductLoading" options="typeAheadOptions" datasets="typeAheadData" ng-model="productObject" class="typeahead" sf-typeahead placeholder="{{ProductLoading ? 'Loading...':'Enter product description'}}" />
				 </div>
				 <span class="input-group-btn dropup custom" ng-class="{open : quantityEntry}">
					 <button type="button" class="btn btn-default custom" ng-click="openQuantity()">x{{defaultQuantity}}</button>
					 <ul class="dropdown-menu">
					  <li ><a>Qty <input type="text" ng-model="defaultQuantity" placeholder="Quantity" ng-blur="quantityEntry = false"/></a></li>
					 
					</ul>
				</span>
				<span class="input-group-btn dropup custom" ng-class="{open : dimensionEntry}">
					 <button type="button" class="btn btn-default custom" ng-click="openDimension()">
					 	<i ng-if="!defaultLength || !defaultWidth" class="glyphicon" ng-class="'glyphicon-fullscreen'"></i>
					 	<span ng-if="defaultLength && defaultWidth">
					 		{{defaultLength}}mm &times; {{defaultWidth}}mm
					 	</span>
					 </button>
					 <ul class="dropdown-menu">
					  <li ><a>L <input type="text" ng-model="defaultLength" placeholder="Length" /></a></li>
					  <li ><a>W <input type="text" ng-model="defaultWidth" placeholder="Width" ng-blur="dimensionEntry = false"/></a></li>
					  
					 
					</ul>
				</span>
				
				 <span class="input-group-btn dropup custom" ng-class="{open : dropDownToggle}">
					<button type="button" class="btn btn-default"><i class="glyphicon " ng-class="'glyphicon-' + filterIcon"></i></button>
					<button type="button" class="btn btn-default dropdown-toggle" ng-click="toggleDropDown()" >
					  <span class="caret"></span>
					  <span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu">
					  <li  ng-click="setFilterKey('barcode')"><a><i class="glyphicon glyphicon-barcode"></i> Barcode</a></li>
					  <li  ng-click="setFilterKey('description')"><a><i class="glyphicon glyphicon-font"></i> Description</a></li>
					</ul>
				  </span><!-- /btn-group -->
				</div><!-- /input-group -->
			</form>
		</div>
	</div>
</footer>
<style type="text/css"> 
	#productTypeahead{ padding:2rem 0;}
	.custom{
		-webkit-border-radius: 0px !important;
		-moz-border-radius: 0px !important;
		border-radius: 0px !important;
	}
	#page-content-wrapper >.container-fluid>div>div.row{
		margin-bottom:55px;
	}
</style>