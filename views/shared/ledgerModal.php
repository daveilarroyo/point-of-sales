<script type="text/ng-template" id="ledgerModalContent.html">

    <div ng-controller="ledgerModalController" ng-init="initializeController()">
	
	  <div class="modal-header">
		<h4 class="modal-title">Ledger Adjustment</h4>
	  </div>
	  <div class="modal-body">
		<form action="" class="form-verical">
			<div class="row">
				<div class="col-md-12 col-xs-12"><div class="form-group">
					<label for="">
						Transactee
						<div class="pull-right">
							<div class="checkbox-inline">
							  <input type="radio" value="supplier" name="transacteeType" ng-checked="transacteeType=='supplier'" ng-model="transacteeSupplier" ng-click="setTransacteeType('supplier')"/> Supplier
							</div>
							<div class="checkbox-inline">
							  <input type="radio" value="customer" name="transacteeType" ng-checked="transacteeType=='customer'"  ng-model="transacteeCustomer"  ng-click="setTransacteeType('customer')" /> Customer
							</div>
						</div>
					</label>
					<div class="input-group">
						<div class="typeahead-wrapper" ng-show="transacteeType=='customer'"><input type="text" class="form-control" ng-readonly="CustomerObject.id!=undefined"  options="typeAheadOptions" datasets="typeAheadCustomerData"  sf-typeahead  ng-model="CustomerObjectTypeAhead"  placeholder="Select Customer"/></div>
						<div class="typeahead-wrapper" ng-show="transacteeType=='supplier'"><input type="text" class="form-control"  ng-readonly="VendorObject.id!=undefined"  ng-model="VendorObjectTypeAhead" class="form-control"  options="typeAheadOptions" datasets="typeAheadVendorData"  sf-typeahead placeholder="Select Supplier"/></div>
						<div class="input-group-btn">
							<button type="button" class="btn btn-default" ng-click="resetTransactee()" ng-disabled="!VendorObject && !CustomerObject"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
						</div>
					</div>
				</div></div>
				<div class="col-md-4 col-xs-4"><div class="form-group"><label for="">Date</label><input type="date" auto-select  ng-required="true"  ng-model="transaction_date" class="form-control" /></div></div>
				<div class="col-md-4 col-xs-4"><div class="form-group"><label for="">Time</label><input type="time" auto-select  ng-required="true"  ng-model="transaction_time" class="form-control" /></div></div>
				<div class="col-md-4 col-xs-4"><div class="form-group"><label for="">Ref No</label><input type="text" auto-select  ng-required="true"  ng-model="ref_no" class="form-control" /></div></div>
				<div class="col-md-12 col-xs-12"><div class="form-group"><label for="">Particulars</label><input type="text" auto-select  ng-required="true"  ng-model="particulars" class="form-control" /></div></div>
				<div class="col-md-12 col-xs-12"><div class="form-group"><label for="">Amount</label>
					<div class="input-group">
						<span class="input-group-addon">{{flag=='+'?'Charge':'&nbsp;&nbsp;&nbsp;Pay&nbsp;&nbsp;'}}</span>
						<input type="number" auto-select  ng-required="true"  ng-model="amount" class="form-control input-lg" />
						<div class="input-group-btn">
							<button type="button" class="btn btn-default btn-lg" ng-class="{'btn-success':flag=='+'}" ng-click="setFlag('+')"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
							<button type="button" class="btn btn-default btn-lg" ng-class="{'btn-danger':flag=='-'}" ng-click="setFlag('-')"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
						</div>
					</div>
				</div></div>
			</div>
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
		<button type="button" class="btn btn-primary" ng-click="confirm(id)">Confirm</button>
	  </div>
	</div>

</script>