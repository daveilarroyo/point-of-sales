<?php date_default_timezone_set('Asia/Manila');?>
<script type="text/ng-template" id="createTransactionModalContent.html">

    <div ng-controller="transactionModalController" ng-init="initializeController()">
	
	  <div class="modal-header">
		<h4 class="modal-title">Transaction Information</h4>		
	  </div>
	  <div class="modal-body">
		<form action="" name="TransactionAddForm" class="form-horizontal">
			<div class="row">
				<input type="hidden" ng-model="id"/>
				<div class="col-md-7 col-xs-7">
					<div class="form-group" ng-if="ReadOnly">
						<label class="control-label col-sm-4 ">Type</label><div class="col-sm-8"><p class="form-control-static">{{header.type}} <span ng-if="header.status=='cancelled'" class="label label-danger">CANCELLED</span></p></div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4 ">Date</label>
						<div class="col-sm-8">
							<p ng-if="!BackLogEnable" class="form-control-static">{{header.display_date | date: 'mediumDate'}}</p>
							<input  ng-if="BackLogEnable" type="date" class="form-control" ng-required="BackLogEnable" name="backLogDate" ng-model="backLogDate" ng-change="updateBackLog(backLogDate)" max="<?php echo date('Y-m-d',time());?>" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4 " ng-show="header.entity_type==='customer'">Customer</label>
						<label class="control-label col-sm-4 " ng-show="header.entity_type==='supplier'">Supplier</label>
						<div class="col-sm-8">
							<p class="form-control-static" ng-if="!header.entity.name">{{header.entity}}</p>
							<p class="form-control-static" ng-if="header.entity.name">{{header.entity.name}} <span ng-if="header.entity.status=='close'" class="label label-danger">ACCOUNT CLOSED</span></p>
						</div>						
					</div>
				</div>
				<div class="col-md-5 col-xs-5 text-right">					
					<label class="total-label">Total</label>
					<div class="total-amount">{{TotalCharges | currency:""}}</div>
					<div ng-hide="ReadOnly" className="change-amount">Change <strong>P{{CashChange | currency:""}}</strong></div>
					<div class="total-adjustment">
						<div class="input-group">
							<div class="input-group-btn">
								<span class="btn btn-xs btn-success" ng-if="Commission"> P{{Commission | currency:""}}</span>
								<span class="btn btn-xs btn-warning" ng-if="Tax"> P{{Tax | currency:""}}</span>
								<span class="btn btn-xs btn-danger" ng-if="Discount"> P{{Discount | currency:""}}</span>
								<span class="btn btn-xs btn-default" ng-if="Commission||Tax||Discount">Adjustment <strong>P{{ (TotalCharges+Commission) | currency:""}}</strong></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="payment-information" ng-show="!OptionalPayment">
					<ul class="nav nav-tabs">
					  <li role="presentation" ng-class="{active:ActiveTransactionTab=='adjustment'}"><a  ng-click="ActiveTransactionTab='adjustment'" >Adjustment</a></li>
					  <li role="presentation" ng-class="{active:ActiveTransactionTab=='payment'}"><a  ng-click="ActiveTransactionTab='payment'" >Payment</a></li>
					</ul>
				<section ng-show="ActiveTransactionTab=='adjustment'">
					<div class="row-fluid">
						<div class="col-md-6 col-xs-6">
							<div class="form-group"><label class="control-label col-sm-6 ">Gross Comm</label><div class="col-sm-6"><input ng-model="GrossCommission" class="form-control numeric" ng-keyup="updateTax()" ng-change="updateCharges()" type="number" auto-select placeholder="Amount added"/></div></div>
							<div class="form-group"><label class="control-label col-sm-6 ">Tax</label><div class="col-sm-6"><input ng-model="Tax" class="form-control numeric" ng-keyup="updateCommission()" ng-change="updateCharges()"  type="number" auto-select placeholder="Amount taxed"/></div></div>
							<div class="form-group"><label class="control-label col-sm-6 ">Net Comm</label><div class="col-sm-6"><input ng-model="Commission" class="form-control numeric" ng-change="updateCharges()" type="number" auto-select placeholder="Amount added"/></div></div>
						</div>
						<div class="col-md-6 col-xs-6">
						<div class="form-group"><label class="control-label col-sm-6 ">Discount</label><div class="col-sm-6"><input ng-model="Discount" class="form-control numeric" ng-blur="checkDiscount()"  type="number" auto-select placeholder="Amount discounted"/></div></div>
						<div class="form-group"><label class="control-label col-sm-6 ">Interest</label><div class="col-sm-6"><input ng-model="Interest" class="form-control numeric" ng-change="updateCharges()"  type="number" auto-select placeholder="Amount interested"/></div></div>
						</div>
						
					</div>
				</section>
				<section ng-show="ActiveTransactionTab=='payment'">
						<div class="row">
							<div class="col-md-12 col-xs-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">Types</label>
									<div class="col-sm-10">
										<label class="checkbox-inline">
										  <input type="checkbox" value="cash" ng-model="payCash" /> Cash
										</label>
										<label class="checkbox-inline">
										  <input type="checkbox" value="card" ng-model="payCheque"/> Check
										</label>
										<label class="checkbox-inline">
										  <input type="checkbox" value="card" ng-model="payCard"/> Card
										</label>
										<label class="checkbox-inline">
										  <input type="checkbox" value="charge" ng-model="payCharge" ng-disabled="header.entity.status=='close'" /> Charge
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row-fluid" ng-show="payCheque">
							<div class="col-md-6 col-xs-6"><div class="form-group"><label class="control-label col-sm-4 "><i class="glyphicon glyphicon-saved pull-left"></i> <div class="pull-right">Check</div></label><div class="col-sm-8"><input  ng-required="payCheque"  ng-model="ChequeReceived" class="form-control numeric" type="number" auto-select   ng-change="updatePayments('cheque')"  placeholder="Amount received"/></div></div></div>
							<div class="col-md-6 col-xs-6"><div class="form-group"><label class="control-label col-sm-4 ">Details</label><div class="col-sm-8"><input type="text" ng-model="ChequeDetails" ng-required="payCheque" name="ChequeDetail" class="form-control numeric" placeholder="Check details"/></div></div></div>
						</div>
						<div class="row-fluid" ng-show="payCard">
							<div class="col-md-6 col-xs-6"><div class="form-group"><label class="control-label col-sm-4 "><i class="glyphicon glyphicon-credit-card pull-left"></i> <div class="pull-right">Card</div> </label><div class="col-sm-8"><input  ng-required="payCard"  ng-model="CardReceived" class="form-control numeric" type="number" auto-select ng-change="updatePayments('card')" placeholder="Amount received"/></div></div></div>
							<div class="col-md-6 col-xs-6"><div class="form-group"><label class="control-label col-sm-4 ">Details</label><div class="col-sm-8"><input type="text" ng-model="CardDetails"  ng-required="payCard" name="CardDetail"  class="form-control numeric" placeholder="Card details"/></div></div></div>
						</div>
						<div class="row-fluid" ng-show="payCharge">
							<div class="col-md-6 col-xs-6"><div class="form-group"><label class="control-label col-sm-4 "><i class="glyphicon glyphicon-user pull-left"></i> <div class="pull-right">Charge</div> </label><div class="col-sm-8"><input  ng-required="payCharge"  ng-model="ChargeReceived" class="form-control numeric" type="number" auto-select ng-change="updatePayments('charge')" placeholder="Amount received"/></div></div></div>
							<div class="col-md-6 col-xs-6"><div class="form-group"><label class="control-label col-sm-4 ">Details</label><div class="col-sm-8"><input type="text" ng-model="ChargeDetails"  ng-required="payCharge" name="ChargeDetail"  class="form-control numeric" placeholder="Charge details"/></div></div></div>
						</div>
						<div class="row-fluid" ng-show="payCash">
							<div class="col-md-6 col-xs-6"><div class="form-group"><label class="control-label col-sm-4 "><i class="glyphicon glyphicon-usd pull-left"></i>  <div class="pull-right">Cash</div></label><div class="col-sm-8"><input ng-model="CashReceived" class="form-control numeric" type="number" auto-select  ng-required="payCash"  ng-change="updatePayments('cash')"  placeholder="Amount received"/></div></div></div>
							<div class="col-md-6 col-xs-6"><div class="form-group"><label class="control-label col-sm-4 ">Details</label><div class="col-sm-8"><input type="text" ng-model="CashDetails"  ng-required="payCash" name="ChargeDetail"  class="form-control numeric" placeholder="Cash details"/></div></div></div>
						</div>
						<div class="row-fluid" ng-if="header.entity.status=='close'">
							<div class="col-md-12"> 
								<div class="alert alert-warning">
									<strong>Note:</strong> This account has been closed, charge is not possible. Please refer to the higher management for more information.
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
				</section>
			</div>
		</form>
	  </div>
	  <div class="modal-footer">
		<div ng-if="!ReadOnly">
			<button type="button" class="btn btn-danger pull-left" ng-if="!BackLogEnable" ng-click="enableBackLog(true)">Change Date</button>
			<button type="button" class="btn btn-success pull-left" ng-if="BackLogEnable" ng-disabled="!TransactionAddForm.backLogDate.$valid" ng-click="enableBackLog(false)">Confirm Date</button>
			<button type="button" class="btn btn-default" ng-click="cancel()"  ng-disabled="BackLogEnable" >Cancel</button>
			<button type="button" class="btn btn-primary" ng-click="confirm()" ng-disabled="(!PaymentValid&&!OptionalPayment) || BackLogEnable">Confirm</button>
		</div>
		<div ng-if="ReadOnly">
			<button type="button" class="btn btn-danger pull-left" ng-click="cancel(id)" ng-disabled="header.status=='cancelled'|| !cancellable" >Cancel</button>
			<button type="button" class="btn btn-default" ng-click="close()">Close</button>
		</div>
	  </div>
	</div>
</script>
<script type="text/ng-template" id="viewTransactionModalContent.html">

    <div ng-controller="transactionModalController" ng-init="initializeController()">
	
	  <div class="modal-header">
		<h4 class="modal-title">Transaction Information</h4>		
	  </div>
	  <div class="modal-body">
		<form action="" class="form-horizontal">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<div class="row"><div class="form-group"><label class="control-label col-sm-3 ">Transaction Id</label><div class="col-sm-8"><input type="text" ng-model="TransactionId" class="form-control numeric" placeholder="Enter transaction id"/></div></div></div>
					</div>
				</div>
			</div>
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
		<button type="button" class="btn btn-primary" ng-click="confirm(TransactionId)" ng-disabled="false">Confirm</button>
	  </div>
	</div>
</script>
<style type="text/css">
.total-label{
	display: block;
	margin-bottom: 0;
	text-transform: uppercase;
	  font-size: 24px;
	  font-weight: 100;
}
.total-amount{
	font-size:3.3rem;
}
</style>