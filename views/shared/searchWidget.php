<button type="button" class="btn btn-primary btn-md btn-fab top right" ng-click="toggleSearch()"><span class="glyphicon" ng-class="{'glyphicon-search':!SearchActive,'glyphicon-remove':SearchActive}" aria-hidden="true"></span></button>
	<div class="search-box" ng-controller="SearchController" ng-show="SearchActive" ng-init="initializeController()">
		<form id="search-form">
			<div class="input-group input-group-lg">
				<div class="input-group-btn">
					<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="searchFor(productSearchBox)"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
				</div>
				<input type="text" class="form-control" placeholder="Search Product" ng-focus="productSearchBoxFocused=true" ng-blur="productSearchBoxFocused=false" ng-disabled="SearchEnabled || FilterEnabled"   ng-model="productSearchBox"/>
				<div class="input-group-btn">
					<button type="button" class="btn btn-default"  ng-disabled="!SearchEnabled" ng-click="resetSearch()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
				</div>
			</div>
		</form>
		<div id="search-results">
			<div class="list-group">
				<div class="list-group-item">
					<small ng-show="!productSearchBox && !LoadingProducts" ng-hide="productSearchBox">RECENT PRODUCTS</small>
					<small ng-show="LoadingProducts || ( !Products.length && productSearchBox)" ng-hide="!LoadingProducts && !Products.length">FETCHING PRODUCTS</small>
					<small ng-show="Products.length && productSearchBox && !LoadingProducts && !SearchEnabled">
						Click the <span class="glyphicon glyphicon-search" aria-hidden="true"></span> to look further.
					</small>
					<small ng-show="Products.length && productSearchBox && SearchEnabled && !LoadingProducts">
						Search result(s) for <b><i>{{productSearchBox}}</i></b>. Click <span class="glyphicon glyphicon-remove"></span> to cancel.
					</small>
					<small ng-show="!Products.length && productSearchBox && SearchEnabled && !LoadingProducts">
						No search result(s) for <b><i>{{productSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.
					</small>
				</div>
				<div class="list-group-search-results">
					<div class="list-group-item" ng-repeat="Product in Products | filter:productSearchFilter">
						<div class="pull-left">
						<h4>{{Product.part_no}} {{Product.particular}}</h4>
						<p>{{Product.description}}</p>
						</div>
						<div class="pull-right">
						<h4 class="amount text-right" ng-class="{'invalid-amount':Product.capital==Product.srp}">P{{Product.srp|currency:""}}</h4>
						<span class="label label-success pull-right" ng-show="Product.soh_quantity>=1">AVAILABLE</span>
						<span class="label label-warning pull-right" ng-show="Product.soh_quantity==1">LOW</span>
						<span class="label label-danger pull-right" ng-show="Product.soh_quantity<1">OUT OF STOCK</span>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="list-group-item" ng-show="LoadingProducts">
						<h4 class="text-center">Loading...</h4>
					</div>
					<div class="list-group-item text-center" ng-show="MoreProducts && !LoadingProducts" ng-hide="productSearchBoxFocused || productSearchBox">
						<button  class="btn btn-default" ng-click="loadMore()">Load More</button>
					</div>

				</div>
			</div>
		</div>
	</div>
	