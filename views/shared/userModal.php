<script type="text/ng-template" id="userModalContent.html">

    <div ng-controller="userModalController" ng-init="initializeController()">
	
	  <div class="modal-header">
		<h4 class="modal-title">User Information</h4>
	  </div>
	  <div class="modal-body">
		<form action="" class="form-verical">
			<div class="row">
				<div class="col-md-6 col-xs-6">
					<div class="form-group">
						<label for="">
							User
							<div class="pull-right">
								<div class="checkbox-inline">
								  <input type="radio" value="supplier" name="type" ng-checked="type=='staff'"   ng-click="setUserType('staff')"/> Staff
								</div>
								<div class="checkbox-inline">
								  <input type="radio" value="customer" name="type" ng-checked="type=='admin'"  ng-click="setUserType('admin')" /> Administrator
								</div>
							</div>
						</label>
						<input type="text" ng-model="username" placeholder="Username" class="form-control" />
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Password</label><input type="password" auto-select  ng-required="true"  ng-model="password" class="form-control" /></div></div>
			</div>
			<div class="row">
				<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">First Name</label><input type="text" auto-select  ng-required="true"  ng-model="first_name" class="form-control" /></div></div>
				<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Last Name</label><input type="text" auto-select  ng-required="true"  ng-model="last_name" class="form-control" /></div></div>
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label for="">Access</label>
						<div class="input-group">
							<div class="typeahead-wrapper"><input type="text" auto-select  ng-readonly="AccessObject.id!=undefined"  options="typeAheadOptions" datasets="typeAheadAccessData"  sf-typeahead  ng-model="AccessObjectTypeAhead" class="form-control" /></div>
							<div class="input-group-btn">
								<button type="button" class="btn btn-default" ng-click="addAccess(AccessObject)"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>							
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-xs-12">
					<span class="btn btn-default module"  ng-repeat="module in access track by $index">
						{{module.title}} <span class="badge"><span class="glyphicon glyphicon-remove" aria-hidden="true" ng-click="removeAccess($index)"></span></span>
					</span>
				</div>
			</div>
			
		</form>
	  </div>
	  <div class="modal-footer">
		<div class="pull-left" ng-if="id">
		</div>
		<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
		<button type="button" class="btn btn-primary" ng-click="confirm(id)">Confirm</button>
	  </div>
	</div>
</script>