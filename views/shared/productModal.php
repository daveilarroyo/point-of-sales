<script type="text/ng-template" id="productModalContent.html">

    <div ng-controller="productModalController" ng-init="initializeController()">
	
	  <div class="modal-header">
		<h4 class="modal-title">Product Information</h4>
	  </div>
	  <div class="modal-body">
		<form action="" class="form-verical">
			<ul class="nav nav-tabs">
			  <li role="presentation" ng-class="{active:ActiveProductTab=='specification'}"><a  ng-click="ActiveProductTab='specification'" >Specification</a></li>
			  <li role="presentation" ng-hide="StockRoomMode" ng-class="{active:ActiveProductTab=='pricing'}"><a  ng-click="ActiveProductTab='pricing'" >Pricing</a></li>
			  <li role="presentation" ng-class="{active:ActiveProductTab=='quantity'}"><a  ng-click="ActiveProductTab='quantity'" >Quantity</a></li>
			</ul>
			<section ng-show="ActiveProductTab=='specification'">
				<div class="row">
				<input type="hidden" ng-model="id"/>
				<div class="col-md-12 col-xs-12"><div class="form-group"><label for="">Category</label>
					<div ng-if="StockRoomMode && category_id"><p class="form-control-static" ng-repeat="category in Categories|filter:{id:category_id}:true">{{category.name}}</p></div>
					<select ng-if="!StockRoomMode" class="form-control" ng-model="$parent.category_id" ng-required="true">
						<option>Select one</option>
						<option ng-selected="category.id==$parent.category_id"  ng-repeat="category in Categories" value="{{category.id}}">{{category.name}}</option>
					</select>
				</div></div>
				
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label for="">Particular</label>
						<div ng-if="StockRoomMode"><p class="form-control-static">{{particular}}</p></div>
						<input ng-if="!StockRoomMode" type="text" ng-required="true"  ng-model="$parent.particular" class="form-control" />
					</div>
				</div>
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<label for="">Description</label>
						<div ng-if="StockRoomMode"><p class="form-control-static">{{description}}</p></div>
						<input ng-if="!StockRoomMode" type="text"  ng-model="$parent.description" class="form-control" />
					</div>
				</div>
				<div class="col-md-6 col-xs-6">
					<div class="form-group">
						<label for="">Part No</label>
						<div ng-if="StockRoomMode"><p class="form-control-static">{{part_no}}</p></div>
						<input  ng-if="!StockRoomMode" type="text" ng-model="$parent.part_no" class="form-control" />
					</div>
				</div>
				<div class="col-md-6 col-xs-6">
					<div class="form-group"><label for="">Unit</label>
						<div ng-if="StockRoomMode"><p class="form-control-static">{{unit}}</p></div>
						<input ng-if="!StockRoomMode" type="text" ng_model="$parent.unit" class="form-control" />
					</div>
				</div>
				
				<div class="col-md-3 col-xs-3">
					<div class="form-group"><label for="">Length</label>
						<div ng-if="StockRoomMode"><p class="form-control-static">{{length}}</p></div>
						<input  ng-if="!StockRoomMode" type="text" ng-model="$parent.length" class="form-control" />
					</div>
				</div>
				<div class="col-md-3 col-xs-3">
					<div class="form-group"><label for="">Width</label>
						<div ng-if="StockRoomMode"><p class="form-control-static">{{width}}</p></div>
						<input ng-if="!StockRoomMode" type="text" ng_model="$parent.width" class="form-control" />
					</div>
				</div>
				<div class="col-md-6 col-xs-6">
					<div class="form-group"><label for="">Area</label>
						<div ng-if="StockRoomMode"><p class="form-control-static">{{length*width}}</p></div>
						<div class="input-group">
							<input ng-if="!StockRoomMode" type="text" readonly ng_model="$parent.length*$parent.width" class="form-control" />
							<span class="input-group-btn">
								<button class="btn btn-default" ng-disabled="!adjustment" ng-click="copyToSOH($parent.length*$parent.width)" type="button">Copy as SOH</button>
							</span>
						</div>
					</div>
				</div>
			</section>
			<section ng-show="ActiveProductTab=='pricing' && !StockRoomMode">
				<div class="row">
					<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Capital</label><input type="number" auto-select ng-required="true"  ng-model="capital" ng-change="updateMarkup()"  class="form-control" /></div></div>
					<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Markup</label><input type="number" auto-select ng-required="true"  ng-model="markup" ng-change="updateSrp()" class="form-control" /></div></div>
					<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Price</label><input type="number" auto-select ng-model="srp"  ng-change="updateMarkup()"   class="form-control" /></div></div>
					<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Discountable</label>
					<select class="form-control" ng-model="discountable">
						<option>Select one</option>
						<option ng-selected="flag.value==discountable" ng-repeat="flag in [{value:1,label:'Yes'},{value:0,label:'No'}]" value="{{flag.value}}">{{flag.label}}</option>
					</select>
				</div></div>
					<div class="col-md-12 col-xs-12" ng-show="!StockRoomMode"><div class="form-group"><label for="">Price Adjustment</label><div class="input-group"><input type="number" auto-select ng-readonly="!StockRoomMode"  ng-model="adjustment.tmp_srp" class="form-control" /> 
						<span class="input-group-btn">
							<button class="btn btn-default" ng-disabled="!adjustment" ng-click="applyAdjustment('price')" type="button">Apply</button>
							</span>
						</div>
					  </div>
					 </div>
					
				</div>
			</section>
			<section ng-show="ActiveProductTab=='quantity'">
				<div class="row">
					<div class="col-md-4 col-xs-4"><div class="form-group"><label for="">Stock On Hand</label><input type="number" auto-select ng-readonly="StockRoomMode" ng-required="true"  ng-model="soh_quantity" class="form-control" /></div></div>
					<div class="col-md-4 col-xs-4"><div class="form-group"><label for="">Max</label><input type="number" auto-select ng-readonly="StockRoomMode"  ng-model="max_quantity" class="form-control" /></div></div>
					<div class="col-md-4 col-xs-4"><div class="form-group"><label for="">Min</label><input type="number" auto-select ng-readonly="StockRoomMode"  ng-model="min_quantity" class="form-control" /></div></div>
					<div class="col-md-4 col-xs-4" ng-show="!StockRoomMode"><div class="form-group"><label for="">Last Adjustment On</label><input type="text" ng-readonly="!StockRoomMode" ng-required="true"  ng-model="adjustment.adj_qty_timestamp" class="form-control" /></div></div>
					<div class="col-md-4 col-xs-4" ng-show="!StockRoomMode"><div class="form-group"><label for="">Initial Adjustment</label><input type="number" auto-select ng-readonly="!StockRoomMode" ng-required="true"  ng-model="adjustment.tmp_quantity" class="form-control" /></div></div>
					<div class="col-md-4 col-xs-4" ng-show="!StockRoomMode"><div class="form-group"><label for="">Current Adjustment</label><div class="input-group"><input type="number" auto-select ng-readonly="!StockRoomMode"  ng-model="adjustment.adj_quantity" class="form-control" /> 
						<span class="input-group-btn">
							<button class="btn btn-default" ng-disabled="!adjustment" ng-click="applyAdjustment('quantity')" type="button">Apply</button>
							</span>
						</div>
					  </div>
					 </div>
					<div class="col-md-12 col-xs-12" ng-show="StockRoomMode" ><div class="form-group"><label for="">Physical Count</label><input type="number" auto-select  ng-model="tmp_quantity" class="form-control input-lg" /></div></div>
				</div>
			</section>
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-success pull-left" ng-click="activate(id)" ng-show="id && !DisableDelete && status =='archive'">Activate</button>
		<button type="button" class="btn btn-danger pull-left" ng-click="archive(id)" ng-show="id && !DisableDelete && status =='active'">Archive</button>
		<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
		<button type="button" class="btn btn-primary" ng-click="confirm(id)">Confirm</button>
	  </div>
	</div>

</script>