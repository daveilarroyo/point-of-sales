<script type="text/ng-template" id="accountModalContent.html">

    <div ng-controller="accountModalController" ng-init="initializeController()">
	
	  <div class="modal-header">
		<h4 class="modal-title">Account Information</h4>
	  </div>
	  <div class="modal-body">
		<form action="" class="form-verical">
			<div class="row">
				<div class="col-md-12 col-xs-12"><div class="form-group">
					<label for="">
						Account
						<div class="pull-right">
							<div class="checkbox-inline">
							  <input type="radio" value="supplier" name="accountType" ng-checked="accountType=='supplier'"  ng-disabled="id" ng-click="setAccountType('supplier')"/> Supplier
							</div>
							<div class="checkbox-inline">
							  <input type="radio" value="customer" name="accountType" ng-checked="accountType=='customer'"  ng-disabled="id" ng-click="setAccountType('customer')" /> Customer
							</div>
						</div>
					</label>
					<div class="input-group">
						<input type="text" ng-model="name" placeholder="Account Name" class="form-control" />
						 <div class="input-group-btn"  ng-class="{open : DropdownOpen}">
							<ul class="dropdown-menu pull-right">
							  <li class="dropdown-header"><i class="glyphicon glyphicon-save-file"></i> Export As</li>
							  <li><a ng-click="exportData(name,'csv')"> CSV</a></li>
							  <li><a ng-click="exportData(name,'pdf')"> PDF</a></li>
							</ul>
							<button type="button" class="btn btn-default" ng-disabled="!id" ng-click="DropdownOpen=!DropdownOpen" ><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span>SOA</button>
						 </div>
					</div>
					
				</div></div>
				<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Beginning Balance</label><input type="number" auto-select  ng-required="true"  ng-model="begin_balance" class="form-control" /></div></div>
				<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Current Balance</label><input type="number" auto-select  ng-readonly="true"  ng-model="current_balance" class="form-control" /></div></div>
				<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Charges</label><input type="number" auto-select  ng-readonly="true"  ng-model="charges" class="form-control" /></div></div>
				<div class="col-md-6 col-xs-6"><div class="form-group"><label for="">Payments</label><input type="number" auto-select  ng-readonly="true"  ng-model="payments" class="form-control" /></div></div>
				<div class="col-xs-12 text-right" ng-show="id"><small>* Charges and payments for {{cutoff}}.</small></div>
			</div>
		</form>
	  </div>
	  <div class="modal-footer">
		<div class="pull-left" ng-if="id">
			<button type="button" class="btn btn-danger" ng-show="status=='open'" ng-click="closeAccount(id)">Close Account</button>
			<button type="button" class="btn btn-success" ng-show="status=='close'"  ng-click="openAccount(id)">Open Account</button>
			<button type="button" class="btn btn-warning" ng-disabled="!allow_posting" ng-click="postAccount(id)">Post Account</button>
		</div>
		<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
		<button type="button" class="btn btn-primary" ng-click="confirm(id)">Confirm</button>
	  </div>
	</div>

</script>