<div class="row" ng-controller="cashflowController" ng-init="initializeController()">
	<div class="col-lg-12">
		<h1>Cash Flow</h1>
		<div class="row">
				<div class="col-sm-12">
				<div class="input-group input-group-lg">
					 <div class="input-group-btn" ng-class="{open : FilterEnabled}">
						<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="toggleFilter()">
							<span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
						</button>
						<ul class="dropdown-menu">
						  <li class="dropdown-header"><i class="glyphicon glyphicon-user"></i> Transactee</li>
						  <li ng-repeat="entity in Entities" ng-class="{active :entity.id == FilterKeys.entity}"><a ng-click="setFilterKey('entity',entity.id)">{{entity.value}}</a></li>
						  <li class="dropdown-header"><i class="glyphicon glyphicon-transfer"></i> Transaction Types</li>
						  <li ng-class="{active :!FilterKeys.type || FilterKeys.type=='ALL'}"><a ng-click="setFilterKey('type','ALL')">All</a></li>
						  <li ng-repeat="type in TransactionTypes" ng-class="{active :type.id == FilterKeys.type}"><a ng-click="setFilterKey('type',type.id)">{{type.value}}</a></li>
						  <li class="dropdown-header"><i class="glyphicon glyphicon-calendar"></i> Coverage</li>
						  <li ng-class="{active :!FilterKeys.coverage || FilterKeys.coverage=='ALL'}"><a ng-click="setFilterKey('coverage','ALL')">All</a></li>
						  <li ng-repeat="coverage in Coverages" ng-class="{active :coverage.id == FilterKeys.coverage}"><a ng-click="setFilterKey('coverage',coverage.id)">{{coverage.value}}</a></li>
						    <li role="separator" class="divider"></li>
						   <li class="dropdown-header">
							<button class="btn btn-default" ng-click="cancelFilter()">Cancel</button>
							<button class="btn btn-primary" ng-click="confirmFilter()">Confirm</button>
						   </li>
						</ul>
						<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="searchFor(CashflowsearchBox)"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
					</div>
					 <input type="text" class="form-control" placeholder="Search {{FilterKeys.entity}} cashflow" ng-disabled="SearchEnabled || FilterEnabled"   ng-model="CashflowsearchBox"/>
					  <div class="input-group-btn">
						<button type="button" class="btn btn-default"  ng-disabled="!SearchEnabled" ng-click="resetSearch()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
						<button type="button" class="btn btn-default" ng-click="exportData(SearchKeyword,FilterKeys)"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span></button>
					</div>
				  </div>
				 </div>
			</div>
		<div class="row table-data">
				<div class="col-sm-12">
				   <table class="table table-hover table-bordered inventory">
					<thead>
						<tr>
							<th>Timestamp</th>
							<th>Particulars</th>
							<th>Income</th>
							<th>Expense</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-show="( FilterKeys.type|| FilterKeys.coverage ) && !LoadingProducts && !FilterEnabled"  class="text-center"> 
							<td colspan="6">
								<span ng-if="!FilterKeys.type || FilterKeys.type=='ALL'">All items</span>
								<span  ng-repeat="type in TransactionTypes">
									<span ng-if="FilterKeys.type == type.id">All {{type.value}}</span>
								</span>
								<span ng-if="FilterKeys.coverage != 'ALL' && FilterKeys.coverage"> covered </span>
								<span ng-if="FilterKeys.coverage =='today'">today</span>
								<span ng-if="FilterKeys.coverage =='7D'">last 7 days</span>
								<span ng-if="FilterKeys.coverage =='30D'">last 30 days</span>
								.
								 Click <i class="glyphicon glyphicon-filter"></i> to modify.
							</td>
						</tr>
						<tr ng-show="Cashflows.length && CashflowsearchBox && !LoadingCashflows && !SearchEnabled"  class="text-center"> 
							<td colspan="6">Click the <span class="glyphicon glyphicon-search" aria-hidden="true"></span> to look further.</td>
						</tr>
						<tr ng-show="Cashflows.length && CashflowsearchBox && SearchEnabled"  class="text-center"> 
							<td colspan="6">Search result(s) for <b><i>{{CashflowsearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
						</tr>
						<tr ng-show="!Cashflows.length && CashflowsearchBox && SearchEnabled && !LoadingCashflows"  class="text-center"> 
							<td colspan="6">No search result(s) for <b><i>{{CashflowsearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
						</tr>
						<tr ng-show="LoadingCashflows"  class="text-center"> 
							<td colspan="6">Loading..</td>
						</tr>
						<tr ng-class="{'active':FetchCashflowId == cashflow.id, 'success':NewCashflow  == cashflow.id }"style="opacity:{{LoadingCashflows||FetchingCashflow || FetchCashflowId == cashflow.id ?0.5:1}}" ng-repeat="cashflow in Cashflows | filter:CashflowsearchFilter" ng-click="editCashflow(cashflow.id)">
							<td>{{cashflow.timestamp | date: "mediumDate"}} {{cashflow.timestamp | date: "shortTime"}}</td>
							<td>{{cashflow.particulars}}</td>
							<td class="numeric"><span ng-if="cashflow.flag=='c'">{{cashflow.amount | currency:""}}</span></td>
							<td class="numeric"><span ng-if="cashflow.flag=='d'">{{cashflow.amount | currency:""}}</span></td>
							
						</tr>
						<tr ng-repeat="fillers in Fillers track by $index">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				   </table>
			   </div>
		   </div>
		   <nav class="row">
				<div class="col-sm-4">
					<div class="input-group">
						 <span class="input-group-addon">Go to page</span>
						 <input type="text" class="form-control" placeholder="Page number"  ng-disabled="Pages.length==0" type="number" ng-model="GoToPage" />
						 <div class="input-group-btn">
							<button class="btn btn-default" ng-disabled="LoadingCashflows"  ng-disabled="Pages.length==0" ng-click="movePage(GoToPage,SearchKeyword)">Go</button>
						</div>
					</div>
				</div>
				<div class="col-sm-8 text-right" ng-hide="Pages.length==0">
					<div class="input-group">
						 <div class="input-group-btn">
							<button class="btn btn-default" ng-disabled="LoadingCashflows || CurrentPage==1" ng-click="movePage(CurrentPage-1,SearchKeyword,FilterKeys)">&laquo;</button>
							<button class="btn btn-default" ng-disabled="LoadingCashflows"  ng-repeat="page in ActivePages track by $index" ng-class="{'btn-primary':page===CurrentPage}"  ng-click="movePage(page,SearchKeyword,FilterKeys)">{{page}}</button>
							<button class="btn btn-default" ng-disabled="LoadingCashflows || CurrentPage==LastPage" ng-click="movePage(CurrentPage+1,SearchKeyword,FilterKeys)">&raquo;</button>
						</div>
					</div>
				</div>
		   </nav>
		<button type="button" class="btn btn-primary btn-md btn-fab top right" ng-click="openCashflowModal()" ng-disabled="cashflowInProgress"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
		<div ng-include="'views/shared/cashflowModal.php'"></div>
	</div>
</div>