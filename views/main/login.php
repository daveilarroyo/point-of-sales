<div class="container" ng-controller="loginController" ng-init="initializeController()">
	<div class="login" ng-include="'views/shared/searchWidget.php'"></div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
			<div class="account-wall">
				
				<form class="form-signin"  autocomplete="off">				
					<input type="text" class="hide"  />
					<input type="password" class="hide"  />
					<div class="text-center text-center col-sm-8 col-sm-offset-2">
						<h2>EasySales</h2>
						<p>Sales made easy.</p>
						<br/>
						<br/>
						<div id="login-form"  ng-show="!ValidUser">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Username"  ng-model="username" required autofocus  />
							</div>
							<div class="form-group">
							<input type="password"   class="form-control" placeholder="Password" ng-model="password"  required />
							</div>
							<label class="checkbox pull-left hide" style="margin-left: 20px;">
								<input type="checkbox" value="remember-me">
								Remember me
							</label>
							<a class="pull-right need-help hide">Forgot password? </a><span class="clearfix"></span>
							
							<button class="btn btn-primary btn-block" type="button" ng-click="signIn()" ng-disabled="Validating" >Log in</button>
							<br />
							<div class="alert alert-warning" ng-show="SignedIn && !ValidUser">
								Invalid login credentials.
							</div>
						</div>	
						<div class="well text-center" ng-show="false">
							<h4>Welcome, {{user}}!</h4>
							<img src="assets/img/temp-logo.png" alt="avatar" class="img-circle" />
							<a class="btn btn-primary btn-block" ng-click="enterDashboard()">Enter Dashboard</a>
						</div>
					</div>
					
					
				
					
				</form>
			</div>
		</div>
	</div>
</div>
