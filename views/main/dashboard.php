<div class="row" ng-controller="dashboardController" ng-init="initializeController()">
	<div class="row">
		<div class="col-lg-6">
			<h1>Dashboard</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-8">
			<div class="list-group">
			  <div class="list-group-item">
			  	<div class="pull-left">
				  	<h4 class="list-group-item-heading">Sales</h4>
				  	<p class="list-group-item-text">Average: P{{Cards.sales.average | currency:""}}</p>
			  	</div>
			  	<div class="pull-right text-right">
			  		<h4 class="list-group-item-heading">P{{Cards.sales.average | currency:""}}</h4>
			  		<p class="list-group-item-text">Today, 7:05 am </p>
			  	</div>
			  	<canvas id="line" class="chart chart-line" data="Cards.sales.data"
				  labels="Cards.sales.labels"
				  click="onClick">
				</canvas>
				<div class="input-group text-center">
					<div class="input-group-btn">
						<button class="btn btn-default" ng-click="setFilter('day')" ng-class={'btn-primary':Dashboard.type==='day'}>Day</button>
						<button class="btn btn-default" ng-click="setFilter('week')" ng-class={'btn-primary':Dashboard.type==='week'}>Week</button>
						<button class="btn btn-default" ng-click="setFilter('month')" ng-class={'btn-primary':Dashboard.type==='month'}>Month</button>
						<button class="btn btn-default" ng-click="setFilter('year')" ng-class={'btn-primary':Dashboard.type==='year'}>Year</button>
					</div>
				</div> 
			  </div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="list-group">
			  <div class="list-group-item">
			  	<h4 class="list-group-item-heading">Trade In</h4>
			  	<p class="list-group-item-text">Average: {{Cards.tradein.average | currency:""}}</p>
			  	<canvas id="line" class="chart chart-line" data="Cards.tradein.data"
				  labels="Cards.tradein.labels"
				  click="onClick">
				</canvas> 
			  </div>
			   <div class="list-group-item">
			  	<h5 class="list-group-item-heading">Deliveries</h5>
			  	<p class="list-group-item-text">Average: {{Cards.deliveries.average | currency:""}}</p>
			  	<canvas id="line" class="chart chart-line" data="Cards.deliveries.data"
				  labels="Cards.deliveries.labels"
				  click="onClick">
				</canvas> 
			  </div>
			 </div>
		</div>
		<div class="col-lg-6">
			<div class="list-group">
			  <div class="list-group-item">
			  	<h4 class="list-group-item-heading">Sales</h4>
			  	<p class="list-group-item-text">Average: {{Cards.sales.average | currency:""}}</p>
			  	<canvas id="line" class="chart chart-line" data="Cards.sales.data"
				  labels="Cards.sales.labels"
				  click="onClick">
				</canvas>
			  </div>
			</div>
		</div>
	</div>
</div>