﻿<div ng-controller="DefaultController">
	<div ng-include="'views/shared/searchWidget.php'"></div>
	<div class="row" ng-class="{blur:SearchActive}">
		<div class="col-lg-6">
			<h1>Front desk</h1>
			<div class="list-group">
			  <a href="#/{{module.link}}" class="list-group-item" ng-repeat="module in Modules | filter:{type:'FD'} | orderBy:order">
				<h4 class="list-group-item-heading"><i class="glyphicon glyphicon-{{module.icon}}"></i> {{module.title}}</h4>
				<p class="list-group-item-text">
					{{module.description}}
				</p>
			  </a>
			  </div>
		</div>
		<div class="col-lg-6">
			<h1>Back room</h1>
			<div class="list-group">
			   <a href="#/{{module.link}}" class="list-group-item" ng-repeat="module in Modules | filter:{type:'BR'} | orderBy:order">
				<h4 class="list-group-item-heading"><i class="glyphicon glyphicon-{{module.icon}}"></i> {{module.title}}</h4>
				<p class="list-group-item-text">
					{{module.description}}
				</p>
			  </a>
			 </div>
		
		</div>
	</div>
</div>
<style>
	
</style>