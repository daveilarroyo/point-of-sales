<div ng-controller="DeliveriesController" ng-init="initializeController()">
<div class="row">
	<div class="col-lg-12">
	   
		<h1 class="pull-left">Deliveries</h1>
		<h3 class="pull-right text-right total"><span>Total</span> <div>{{DeliveriesAmount  | currency:""}}</div></h3>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-sm-12">
			<div class="input-group input-group-lg">
				 <div class="input-group-btn ">
					<button type="button" class="btn btn-default" ng-click="toggleEdit('edit')" ng-hide="editMode" ng-disabled="deleteMode || !DeliveriesItems.length"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-click="toggleEdit('save')" ng-show="editMode"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-click="toggleDelete('delete')" ng-hide="deleteMode" ng-disabled="editMode || !DeliveriesItems.length"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-click="toggleDelete('save')" ng-show="deleteMode"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-hide="true" ng-click="openPurchaseOrderModal()" ng-disabled="DeliveriesItems.length || (deleteMode||editMode)"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></button>
					<button type="button" class="btn btn-default" ng-click="resetVendorObject()" ng-disabled="VendorObject.id==undefined"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></button>
				</div>
				 <div class=" typeahead-wrapper">
					<input class="form-control input-lg" type="text" options="typeAheadOptions"  ng-readonly="VendorObject.id!=undefined" datasets="typeAheadData" ng-model="VendorObjectTypeahead"  ng-blur="validateVendor(VendorObject)"  class="typeahead " sf-typeahead  placeholder="Supplier Name" />
				 </div>
				   <div class="input-group-btn">
				 <button type="button" class="btn btn-default" ng-click="resetDeliveries()"  ng-disabled="editMode||deleteMode||!DeliveriesItems.length"><span class="glyphicon glyphicon-remove"></span></button>
				 </div>
			  </div>
			</div>
		</div>
		<div class="row table-data">
			<div class="col-sm-12">
			    <table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th rowspan="2"ng-show="deleteMode" >&nbsp;</th>
						<th rowspan="2">Product</th>
						<th rowspan="2">Unit</th>
						<th rowspan="2">Price</th>
						<th rowspan="2">Quantity</th>
						<th colspan="2">Discount <span ng-if="DiscountType=='peso'">(P)</span> <span ng-if="DiscountType=='percent'">(%)</span></th>
						<th rowspan="2">Amount</th>
						<th rowspan="2">Capital</th>
					</tr>
					<tr ng-if="editMode">
						<td class="text-center"> <input type="radio" name="DiscountType" ng-checked="DiscountType=='peso'" value="peso" ng-click="applyDiscountType('peso')"/> Peso (P)</td>
						<td class="text-center"> <input type="radio" name="DiscountType" ng-checked="DiscountType=='percent'" value="percent"  ng-click="applyDiscountType('percent')" /> Percent (%)</td>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="item in DeliveriesItems" ng-class="{danger:item.is_delete}">
						<td ng-show="deleteMode" ><input type="checkbox" ng-model="item.is_delete" ng-checked="item.is_delete" /></td>
						<td>{{item.display_title}}</td>
						<td>{{item.unit}}</td>
						<td class="numeric"> 
							<span ng-hide="editMode">{{item.list_price}}</span>
							<input  ng-show="editMode" type="number" auto-select ng-model="item.list_price"  ng-change="updateAmount($index)"  class="input-sm" />
						</td>
						<td class="numeric"> 
							<span ng-hide="editMode">{{item.delivered}}</span>
							<input  ng-show="editMode" type="number" auto-select ng-model="item.delivered"  ng-change="updateAmount($index)"  class="input-sm" />
						</td>
						<td class="numeric" colspan="2"> 
							<span ng-hide="editMode">{{item.discount}}</span>
							<input  ng-show="editMode" type="text" auto-select ng-model="item.discount"  ng-change="updateAmount($index)"  class="input-sm text-right" />
						</td>
						<td class="numeric">
							<span  ng-hide="editMode">{{item.amount ==0?'FREE':(item.amount | currency:"")}}</span>
							<input  ng-show="editMode" type="number" auto-select ng-model="item.amount" ng-change="updateCapital($index)" class="input-sm" />
						</td>
						<td class="numeric">
							<span ng-hide="editMode">{{item.capital | currency:""}}</span>
							<input  ng-show="editMode" type="number" auto-select ng-model="item.capital"  ng-blur="checkCapital($index)"  class="input-sm" />
						</td>
					</tr>
				</tbody>
				<tfoot ng-if="!DeliveriesItems.length"> 
					<tr>
						<td class="text-center" colspan="8">No items added yet.</td>
					</tr>
				</tfoot>
			   </table>
			</div>
	   </div>
	   <button type="button" class="btn btn-primary btn-md btn-fab top right" ng-click="openTransactionModal()" ng-disabled="transactionInProgress || (!DeliveriesAmount || !VendorObject) || (deleteMode||editMode)"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>
		<div ng-include="'views/shared/transactionModal.php?<?php echo substr(md5(rand()),0,5)?>'"></div>
	   <div ng-include="'views/shared/productTypeahead.php'"></div>
	   <div ng-include="'views/shared/productModal.php'"></div>
	 </div>
</div>
</div>
</div>