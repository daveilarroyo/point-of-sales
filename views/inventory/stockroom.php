 <div ng-controller="StockRoomController" ng-init="initializeController()">
	 <div class="row">
		<div class="col-lg-12">
		   
			<h1 class="pull-left">Stock Room</h1>
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-sm-12">
				<div class="input-group input-group-lg">
					 <div class="input-group-btn" ng-class="{open : FilterEnabled}">
						<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="toggleFilter()">
							<span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
						</button>
						<ul class="dropdown-menu">
						  <li class="dropdown-header"><i class="glyphicon glyphicon-th-large"></i> Category</li>
						  <li ng-class="{active :!FilterKeys.category || FilterKeys.category=='ALL'}"><a ng-click="setFilterKey('category','ALL')">All</a></li>
						  <li ng-repeat="category in Categories" ng-class="{active :category.id == FilterKeys.category}"><a ng-click="setFilterKey('category',category.id)">{{category.value}}</a></li>
						    <li role="separator" class="divider"></li>
						   <li class="dropdown-header">
							<button class="btn btn-default" ng-click="cancelFilter()">Cancel</button>
							<button class="btn btn-primary" ng-click="confirmFilter()">Confirm</button>
						   </li>
						</ul>
						<button type="button" class="btn btn-default" ng-disabled="SearchEnabled" ng-click="searchFor(productSearchBox)"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
					</div>
					 <input type="text" class="form-control" placeholder="Search Product" ng-disabled="SearchEnabled"   ng-model="productSearchBox"/>
					  <div class="input-group-btn">
						<button type="button" class="btn btn-default"  ng-disabled="!SearchEnabled" ng-click="resetSearch()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
					</div>
				  </div>
				 </div>
			</div>
		   <div class="row table-data">
				<div class="col-sm-12">
				   <table class="table table-hover table-bordered inventory">
					<thead>
						<tr>
							<th>Product</th>
							<th>Stock On Hand</th>
							<th>Physical Count</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-show="( FilterKeys.category|| FilterKeys.quantity ) && !LoadingProducts && !FilterEnabled"  class="text-center"> 
							<td colspan="3">
								<span ng-if="!FilterKeys.category || FilterKeys.category=='ALL'">All items</span>
								<span  ng-repeat="category in Categories">
									<span ng-if="FilterKeys.category == category.id">All {{category.value}}</span>
								</span>.
								 Click <i class="glyphicon glyphicon-filter"></i> to modify.
							</td>
						</tr>
						<tr ng-show="Products.length && productSearchBox && !LoadingProducts && !SearchEnabled"  class="text-center"> 
							<td colspan="3">Click the <span class="glyphicon glyphicon-search" aria-hidden="true"></span> to look further.</td>
						</tr>
						<tr ng-show="Products.length && productSearchBox && SearchEnabled"  class="text-center"> 
							<td colspan="3">Search result(s) for <b><i>{{productSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
						</tr>
						<tr ng-show="!Products.length && productSearchBox && SearchEnabled && !LoadingProducts"  class="text-center"> 
							<td colspan="3">No search result(s) for <b><i>{{productSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
						</tr>
						<tr ng-show="LoadingProducts"  class="text-center"> 
							<td colspan="3">Loading..</td>
						</tr>
						<tr ng-class="{'active':FetchProductId == product.id, 'success':NewProduct  == product.id }"style="opacity:{{LoadingProducts||FetchingProduct || FetchProductId == product.id ?0.5:1}}" ng-repeat="product in Products | filter:productSearchFilter" ng-click="editProduct(product.id)">
							<td>{{product.display_title}}</td>
							<td class="numeric">{{product.soh_quantity}}</td>
							<td class="numeric">{{product.tmp_quantity}}</td>
						</tr>
						<tr ng-repeat="fillers in Fillers track by $index">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				   </table>
			   </div>
		   </div>
		   <nav class="row">
				<div class="col-sm-4">
					<div class="input-group">
						 <span class="input-group-addon">Go to page</span>
						 <input type="text" class="form-control" placeholder="Page number"  ng-disabled="Pages.length==0" type="number" auto-select ng-model="GoToPage" />
						 <div class="input-group-btn">
							<button class="btn btn-default" ng-disabled="LoadingProducts"  ng-disabled="Pages.length==0" ng-click="movePage(GoToPage,SearchKeyword)">Go</button>
						</div>
					</div>
				</div>
				<div class="col-sm-8 text-right" ng-hide="Pages.length==0">
					<div class="input-group">
						 <div class="input-group-btn">
							<button class="btn btn-default" ng-disabled="LoadingProducts || CurrentPage==1" ng-click="movePage(CurrentPage-1,SearchKeyword,FilterKeys)">&laquo;</button>
							<button class="btn btn-default" ng-disabled="LoadingProducts"  ng-repeat="page in ActivePages track by $index" ng-class="{'btn-primary':page===CurrentPage}"  ng-click="movePage(page,SearchKeyword,FilterKeys)">{{page}}</button>
							<button class="btn btn-default" ng-disabled="LoadingProducts || CurrentPage==LastPage" ng-click="movePage(CurrentPage+1,SearchKeyword,FilterKeys)">&raquo;</button>
						</div>
					</div>
				</div>
		   </nav>
		</div>
	</div>
	<button type="button" class="btn btn-primary btn-md btn-fab top right" ng-hide="true" ng-click="openProductModal()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
	 <div ng-include="'views/shared/productModal.php'"></div>
</div>
