<div class="row" ng-controller="AccountsController" ng-init="initializeController()">
	<div class="col-lg-12">
		<h1>Accounts</h1>
		<div class="row">
				<div class="col-sm-12">
				<div class="input-group input-group-lg">
					 <div class="input-group-btn" ng-class="{open : FilterEnabled}">
						<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="toggleFilter()">
							<span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
						</button>
						<ul class="dropdown-menu">
						  <li class="dropdown-header"><i class="glyphicon glyphicon-user"></i> Account Type</li>
						  <li ng-repeat="entity in Entities" ng-class="{active :entity.id == FilterKeys.entity}"><a ng-click="setFilterKey('entity',entity.id)">{{entity.value}}</a></li>
						    <li role="separator" class="divider"></li>
						   <li class="dropdown-header">
							<button class="btn btn-default" ng-click="cancelFilter()">Cancel</button>
							<button class="btn btn-primary" ng-click="confirmFilter()">Confirm</button>
						   </li>
						</ul>
						<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="searchFor(accountSearchBox)"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
					</div>
					 <input type="text" class="form-control" placeholder="Search {{FilterKeys.entity}} account" ng-disabled="SearchEnabled || FilterEnabled"   ng-model="accountSearchBox"/>
					  <div class="input-group-btn">
						<button type="button" class="btn btn-default hide" ng-click="exportData(SearchKeyword,FilterKeys)"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span></button>
						<button type="button" class="btn btn-default"  ng-disabled="!SearchEnabled" ng-click="resetSearch()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
					</div>
				  </div>
				 </div>
			</div>
		<div class="row table-data">
				<div class="col-sm-12">
				   <table class="table table-hover table-bordered inventory">
					<thead>
						<tr>
							<th>Name</th>
							<th>Balance</th>
						</tr>
					</thead>
					<tbody>
						
						<tr ng-show="Accounts.length && accountSearchBox && !LoadingAccounts && !SearchEnabled"  class="text-center"> 
							<td colspan="6">Click the <span class="glyphicon glyphicon-search" aria-hidden="true"></span> to look further.</td>
						</tr>
						<tr ng-show="Accounts.length && accountSearchBox && SearchEnabled"  class="text-center"> 
							<td colspan="6">Search result(s) for <b><i>{{accountSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
						</tr>
						<tr ng-show="!Accounts.length && accountSearchBox && SearchEnabled && !LoadingAccounts"  class="text-center"> 
							<td colspan="6">No search result(s) for <b><i>{{accountSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
						</tr>
						<tr ng-show="LoadingAccounts"  class="text-center"> 
							<td colspan="6">Loading..</td>
						</tr>
						<tr ng-class="{'active':FetchAccountId == account.id, 'success':NewAccount  == account.id }"style="opacity:{{LoadingAccounts||FetchingAccount || FetchAccountId == account.id ?0.5:1}}" ng-repeat="account in Accounts | filter:accountSearchFilter" ng-click="editAccount(account.id)">
							<td>{{account.name}} <span class="label label-danger" ng-if="account.status=='close'">CLOSED</span></td>
							<td class="numeric">{{account.current_balance | currency:""}}</td>
						</tr>
						<tr ng-repeat="fillers in Fillers track by $index">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				   </table>
			   </div>
		   </div>
		   <nav class="row">
				<div class="col-sm-4">
					<div class="input-group">
						 <span class="input-group-addon">Go to page</span>
						 <input type="text" class="form-control" placeholder="Page number"  ng-disabled="Pages.length==0" type="number" ng-model="GoToPage" />
						 <div class="input-group-btn">
							<button class="btn btn-default" ng-disabled="LoadingAccounts"  ng-disabled="Pages.length==0" ng-click="movePage(GoToPage,SearchKeyword)">Go</button>
						</div>
					</div>
				</div>
				<div class="col-sm-8 text-right" ng-hide="Pages.length==0">
					<div class="input-group">
						 <div class="input-group-btn">
							<button class="btn btn-default" ng-disabled="LoadingAccounts || CurrentPage==1" ng-click="movePage(CurrentPage-1,SearchKeyword,FilterKeys)">&laquo;</button>
							<button class="btn btn-default" ng-disabled="LoadingAccounts"  ng-repeat="page in ActivePages track by $index" ng-class="{'btn-primary':page===CurrentPage}"  ng-click="movePage(page,SearchKeyword,FilterKeys)">{{page}}</button>
							<button class="btn btn-default" ng-disabled="LoadingAccounts || CurrentPage==LastPage" ng-click="movePage(CurrentPage+1,SearchKeyword,FilterKeys)">&raquo;</button>
						</div>
					</div>
				</div>
		   </nav>
		<button type="button" class="btn btn-primary btn-md btn-fab top right" ng-click="openAccountModal()" ng-disabled="accountInProgress"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
		<div ng-include="'views/shared/accountModal.php'"></div>
	</div>
</div>