<div class="row" ng-controller="UsersController" ng-init="initializeController()">
	<div class="col-lg-12">
		<h1>Users</h1>
		<div class="row">
				<div class="col-sm-12">
				<div class="input-group input-group-lg">
					 <div class="input-group-btn" ng-class="{open : FilterEnabled}">
						<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="searchFor(UserSearchBox)"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
					</div>
					 <input type="text" class="form-control" placeholder="Search user" ng-disabled="SearchEnabled || FilterEnabled"   ng-model="UserSearchBox"/>
					  <div class="input-group-btn">
						<button type="button" class="btn btn-default hide" ng-click="exportData(SearchKeyword,FilterKeys)"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span></button>
						<button type="button" class="btn btn-default"  ng-disabled="!SearchEnabled" ng-click="resetSearch()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
					</div>
				  </div>
				 </div>
			</div>
		<div class="row table-data">
				<div class="col-sm-12">
				   <table class="table table-hover table-bordered inventory">
					<thead>
						<tr>
							<th>User</th>
							<th>Name</th>
							<th>Access</th>
						</tr>
					</thead>
					<tbody>
						
						<tr ng-show="Users.length && UserSearchBox && !LoadingUsers && !SearchEnabled"  class="text-center"> 
							<td colspan="6">Click the <span class="glyphicon glyphicon-search" aria-hidden="true"></span> to look further.</td>
						</tr>
						<tr ng-show="Users.length && UserSearchBox && SearchEnabled"  class="text-center"> 
							<td colspan="6">Search result(s) for <b><i>{{UserSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
						</tr>
						<tr ng-show="!Users.length && UserSearchBox && SearchEnabled && !LoadingUsers"  class="text-center"> 
							<td colspan="6">No search result(s) for <b><i>{{UserSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
						</tr>
						<tr ng-show="LoadingUsers"  class="text-center"> 
							<td colspan="6">Loading..</td>
						</tr>
						<tr ng-class="{'active':FetchUserId == User.id, 'success':NewUser  == User.id }"style="opacity:{{LoadingUsers||FetchingUser || FetchUserId == User.id ?0.5:1}}" ng-repeat="User in Users | filter:UserSearchFilter" ng-click="editUser(User.id)">
							<td>{{User.username}}</td>
							<td>{{User.first_name}} {{User.last_name}}</td>
							<td><span ng-if="!User.Module.length">No access</span><span ng-if=" User.Module.length">{{ User.Module[0].title}}</span> <span ng-if=" User.Module.length>1">and {{User.Module.length-1}} more</a></td>
						</tr>
						<tr ng-repeat="fillers in Fillers track by $index">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</tbody>
				   </table>
			   </div>
		   </div>
		   <nav class="row">
				<div class="col-sm-4">
					<div class="input-group">
						 <span class="input-group-addon">Go to page</span>
						 <input type="text" class="form-control" placeholder="Page number"  ng-disabled="Pages.length==0" type="number" ng-model="GoToPage" />
						 <div class="input-group-btn">
							<button class="btn btn-default" ng-disabled="LoadingUsers"  ng-disabled="Pages.length==0" ng-click="movePage(GoToPage,SearchKeyword)">Go</button>
						</div>
					</div>
				</div>
				<div class="col-sm-8 text-right" ng-hide="Pages.length==0">
					<div class="input-group">
						 <div class="input-group-btn">
							<button class="btn btn-default" ng-disabled="LoadingUsers || CurrentPage==1" ng-click="movePage(CurrentPage-1,SearchKeyword,FilterKeys)">&laquo;</button>
							<button class="btn btn-default" ng-disabled="LoadingUsers"  ng-repeat="page in ActivePages track by $index" ng-class="{'btn-primary':page===CurrentPage}"  ng-click="movePage(page,SearchKeyword,FilterKeys)">{{page}}</button>
							<button class="btn btn-default" ng-disabled="LoadingUsers || CurrentPage==LastPage" ng-click="movePage(CurrentPage+1,SearchKeyword,FilterKeys)">&raquo;</button>
						</div>
					</div>
				</div>
		   </nav>
		<button type="button" class="btn btn-primary btn-md btn-fab top right" ng-click="openUserModal()" ng-disabled="UserInProgress"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
		<div ng-include="'views/shared/userModal.php'"></div>
	</div>
</div>