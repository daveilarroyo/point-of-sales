﻿<div ng-controller="TransactionsController" ng-init="initializeController()">
	<div class="row">
		<div class="col-lg-12">
			<h1>Transactions</h1>
			<div class="row">
					<div class="col-sm-12">
					<div class="input-group input-group-lg">
						 <div class="input-group-btn" ng-class="{open : FilterEnabled}">
							<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="toggleFilter()">
								<span class="glyphicon glyphicon-filter" aria-hidden="true"></span>
							</button>
							<ul class="dropdown-menu">
							  <li class="dropdown-header"><i class="glyphicon glyphicon-transfer"></i> Transaction Types</li>
							  <li ng-class="{active :!FilterKeys.type || FilterKeys.type=='ALL'}"><a ng-click="setFilterKey('type','ALL')">All</a></li>
							  <li ng-repeat="type in TransactionTypes" ng-class="{active :type.id == FilterKeys.type}"><a ng-click="setFilterKey('type',type.id)">{{type.value}}</a></li>
							  <li class="dropdown-header"><i class="glyphicon glyphicon-calendar"></i> Coverage</li>
							  <li ng-class="{active :!FilterKeys.coverage || FilterKeys.coverage=='ALL'}"><a ng-click="setFilterKey('coverage','ALL')">All</a></li>
							  <li ng-repeat="coverage in Coverages" ng-class="{active :coverage.id == FilterKeys.coverage}"><a ng-click="setFilterKey('coverage',coverage.id)">{{coverage.value}}</a></li>
							  <li ng-show="FilterKeys.coverage==='custom'" class="dropdown-header custom-date">
									<div class="form-group">
										<label for="">From</label> 
										<input type="date" class="form-control"   ng-model="CustomDate.from" />
									</div>
									<div class="clearfix"></div>
							  </li>
							  <li ng-show="FilterKeys.coverage==='custom'" class="dropdown-header custom-date">
									<div class="form-group">
									<label for="">To</label>
									<input type="date" class="form-control" ng-model="CustomDate.to" min="{{CustomDate.from}}" />
									</div>
									<div class="clearfix"></div>
							  </li>
								<li role="separator" class="divider"></li>
					
							   <li class="dropdown-header">
								<button class="btn btn-default" ng-click="cancelFilter()">Cancel</button>
								<button class="btn btn-primary" ng-click="confirmFilter()">Confirm</button>
							   </li>
							</ul>
							<button type="button" class="btn btn-default" ng-disabled="SearchEnabled || FilterEnabled" ng-click="searchFor(transactionSearchBox)"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
						</div>
						 <input type="text" class="form-control" placeholder="Search Transaction" ng-disabled="SearchEnabled || FilterEnabled"   ng-model="transactionSearchBox"/>
						  <div class="input-group-btn">
							<button type="button" class="btn btn-default"  ng-disabled="!SearchEnabled" ng-click="resetSearch()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
							<button type="button" class="btn btn-default" ng-click="exportData(SearchKeyword,FilterKeys)"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span></button>
						</div>
					  </div>
					 </div>
				</div>
			<div class="row table-data">
					<div class="col-sm-12">
					   <table class="table table-hover table-bordered inventory">
						<thead>
							<tr>
								<th>Timestamp</th>
								<th>Type</th>
								<th>User</th>
								<th>Transactee</th>
								<th>Details</th>
								<th>Adjustments</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-show="( FilterKeys.type|| FilterKeys.coverage ) && !LoadingProducts && !FilterEnabled"  class="text-center"> 
								<td colspan="7">
									<span ng-if="!FilterKeys.type || FilterKeys.type=='ALL'">All items</span>
									<span  ng-repeat="type in TransactionTypes">
										<span ng-if="FilterKeys.type == type.id">All {{type.value}}</span>
									</span>
									<span ng-if="FilterKeys.coverage != 'ALL' && FilterKeys.coverage"> covered </span>
									<span ng-if="FilterKeys.coverage =='today'">today</span>
									<span ng-if="FilterKeys.coverage =='7D'">last 7 days</span>
									<span ng-if="FilterKeys.coverage =='30D'">last 30 days</span>
									.
									 Click <i class="glyphicon glyphicon-filter"></i> to modify.
								</td>
							</tr>
							<tr ng-show="Transactions.length && transactionSearchBox && !LoadingTransactions && !SearchEnabled"  class="text-center"> 
								<td colspan="7">Click the <span class="glyphicon glyphicon-search" aria-hidden="true"></span> to look further.</td>
							</tr>
							<tr ng-show="Transactions.length && transactionSearchBox && SearchEnabled"  class="text-center"> 
								<td colspan="7">Search result(s) for <b><i>{{transactionSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
							</tr>
							<tr ng-show="!Transactions.length && transactionSearchBox && SearchEnabled && !LoadingTransactions"  class="text-center"> 
								<td colspan="7">No search result(s) for <b><i>{{transactionSearchBox}}</i></b>. Click the <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> to cancel.</td>
							</tr>
							<tr ng-show="LoadingTransactions"  class="text-center"> 
								<td colspan="7">Loading..</td>
							</tr>
							<tr ng-class="{'active':FetchTransactionId == transaction.id, 'success':NewTransaction  == transaction.id, 'cancelled' : transaction.status=='cancelled' }"style="opacity:{{LoadingTransactions||FetchingTransaction || FetchTransactionId == transaction.id ?0.5:1}}" ng-repeat="transaction in Transactions | filter:transactionSearchFilter" ng-click="openTransactionModal(transaction)">
								<td>
									{{transaction.timestamp | date: "mediumDate"}} {{transaction.timestamp | date: "shortTime"}}
								</td>
								<td>{{transaction.type}}</td>
								<td>{{transaction.user}}</td>
								<td>{{transaction.entity.name}}</td>
								<td>{{transaction.TransactionDetail[0].Product.display_title}} <span ng-show="transaction.TransactionDetail.length>1"> and {{transaction.TransactionDetail.length-1}} more.</span></td>
								<td>
									<span ng-if="transaction.commission">Commission: {{transaction.commission | currency:""}}</span> 
									<span ng-if="transaction.tax">Tax: {{transaction.tax | currency:""}}</span> 
									<span ng-if="transaction.discount">Discount: {{transaction.discount | currency:""}}</span> 
									<span ng-if="transaction.interest">Interest: {{transaction.interest | currency:""}}</span> 
									<span ng-if="transaction.status=='cancelled'" class="label label-danger">Cancelled</span>
									<span ng-if="transaction.backlogged" class="label label-danger">Backlogged</span>
								</td>
								<td class="numeric">{{transaction.amount | currency:""}}</td>
							</tr>
							<tr ng-repeat="fillers in Fillers track by $index">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					   </table>
				   </div>
			   </div>
			   <nav class="row">
					<div class="col-sm-4">
						<div class="input-group">
							 <span class="input-group-addon">Go to page</span>
							 <input type="text" class="form-control" placeholder="Page number"  ng-disabled="Pages.length==0" type="number" ng-model="GoToPage" />
							 <div class="input-group-btn">
								<button class="btn btn-default" ng-disabled="LoadingTransactions"  ng-disabled="Pages.length==0" ng-click="movePage(GoToPage)">Go</button>
							</div>
						</div>
					</div>
					<div class="col-sm-8 text-right" ng-hide="Pages.length==0">
						<div class="input-group">
							 <div class="input-group-btn">
								<button class="btn btn-default" ng-disabled="LoadingTransactions || CurrentPage==1" ng-click="movePage(CurrentPage-1,SearchKeyword,FilterKeys)">&laquo;</button>
								<button class="btn btn-default" ng-disabled="LoadingTransactions"  ng-repeat="page in ActivePages track by $index" ng-class="{'btn-primary':page===CurrentPage}"  ng-click="movePage(page,SearchKeyword,FilterKeys)">{{page}}</button>
								<button class="btn btn-default" ng-disabled="LoadingTransactions || CurrentPage==LastPage" ng-click="movePage(CurrentPage+1,SearchKeyword,FilterKeys)">&raquo;</button>
							</div>
						</div>
					</div>
			   </nav>
		</div>
		<div ng-include="'views/shared/transactionModal.php'"></div>
	</div>
</div>