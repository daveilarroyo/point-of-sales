<div ng-controller="profileController" ng-init="initializeController()">
	<div class="row">
		<div class="col-md-3">
			<div class="employee-profile text-center">
				<img class="img-thumbnail" ng-src="{{avatar}}"/>
				
				<button class="btn btn-primary btn-sm" ng-click="toggleMode()" ng-disabled="editMode"><i class="fa fa-pencil"></i> Edit Profile</button>
			</div>
		</div>
		<div class="col-md-6">
			<h3>General Information</h3>
			<dl class="dl-horizontal">
			  <dt>First Name</dt>
			  <dd ng-if="editMode"><input type="text" ng-model="employee.first_name" /></dd>
			  <dd ng-if="!editMode">{{employee.first_name}}</dd>
			  <dt>Last Name</dt>
			   <dd ng-if="editMode"><input type="text" ng-model="employee.last_name" /></dd>
			  <dd ng-if="!editMode">{{employee.last_name}}</dd>
			  <dt>Company Email</dt>
			   <dd ng-if="editMode"><input type="text" ng-model="employee.company_email" /></dd>
			  <dd ng-if="!editMode">{{employee.company_email}}</dd>
			  <dt>Contact Number</dt>
			   <dd ng-if="editMode"><input type="text" ng-model="employee.contact_number" /></dd>
			  <dd ng-if="!editMode">{{employee.contact_number}}</dd>
			  <dt>Birthday</dt>
			   <dd ng-if="editMode"><input type="text" ng-model="employee.birthday" /></dd>
			  <dd ng-if="!editMode">{{employee.birthday}}</dd>
			  <dt>Status</dt>
			   <dd ng-if="editMode"><input type="text" ng-model="employee.status" /></dd>
			  <dd ng-if="!editMode">{{employee.status}}</dd>			  
			</dl>
			<div class="controls text-right" ng-show="editMode">
				<button class="btn btn-primary btn-sm" ng-click="saveChanges()"> Save</button>
				<button class="btn btn-primary btn-sm" ng-click="cancelChanges()">Cancel</button>
			</div>
		</div>
	</div>
</div>