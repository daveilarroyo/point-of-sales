"use strict";

define(['config','api'], function (app,api) {
    app.register.controller('profileController',['$scope',  '$rootScope', function ($scope,  $rootScope) {
       $scope.initializeController = function () { 
			$scope.editMode = false;
			$rootScope.moduleName = 'Employee Profile';
			api.GET("/getEmployeeInformation",successEmployeeInformation,errorEmployeeInformation,$scope);
	   }
	   $scope.toggleMode = function(){
		   $scope.editMode = !$scope.editMode;		   
	   }
	   $scope.saveChanges = function(){
		  $scope.cache =  $scope.employee;   
		  $scope.editMode = false;
		  api.POST("/updateEmployeeInformation",successUpdateEmployeeInformation,errorUpdateEmployeeInformation,$scope, $scope.employee);
	   }
	   $scope.cancelChanges = function(){
		   //TODO: Cache not working
		  $scope.employee =   $scope.cache;   
		  $scope.editMode = false;
	   }
	   function successEmployeeInformation($response, $status, $scope){
		  $scope.cache =  $scope.employee =  $response.employee;
	   }
	   function errorEmployeeInformation($response, $status, $scope){}
	   function successUpdateEmployeeInformation($response, $status, $scope){}
	   function errorUpdateEmployeeInformation($response, $status, $scope){}
    }]);
});


