﻿

define(['config','api','searchController'], function (app,api) {

    app.register.controller('DefaultController',['$scope',  '$rootScope', function ($scope,  $rootScope) {

       $scope.initializeController = function () { 
			
	   }
	   $scope.toggleSearch = function(){
	   	$scope.SearchActive =  !$scope.SearchActive;
	   	$scope.$broadcast($scope.SearchActive?'initSearch':'clearSearch');
	   }
    }]);
});


