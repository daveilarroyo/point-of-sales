

define(['config','api','angular-chart','transactionService'], function (app,api) {

    app.register.controller('dashboardController',['$scope',  '$rootScope','transactionService', function ($scope,  $rootScope,transactionService) {

       $scope.initializeController = function () { 
			$rootScope.moduleName = 'Dashboard';
			$scope.Dashboard={type:'week',coverage:'7D'};
			$scope.Cards = {
				sales:{type:'sales',status:'fulfilled'},
				deliveries:{type:'deliveries',status:'delivered'},
				tradein:{type:'tradein',status:'fulfilled'},
				orders:{type:'orders',status:'ordered'},
			};
			$scope.setFilter($scope.Dashboard.type);
	   }
	   $scope.setFilter = function(type){
	   		var coverage;
	   		switch(type){
	   			case 'day':
	   				coverage = 'today';
	   			break;
	   			case 'week':
	   				coverage = '7D';
	   			break;
	   			case 'month':
	   				coverage = '30D';
	   			break;
	   			case 'year':
	   				coverage = 'YTD';
	   			break;
	   		}
	   		$scope.Dashboard.type = type;
	   		$scope.Dashboard.coverage = coverage;
	   		updateCards();
	   }
	   function updateCards(){
	   		for(var i in $scope.Cards){
				$scope.Cards[i].labels=[];
				$scope.Cards[i].data=[[]];
				$scope.Cards[i].average=0;
				var __type =  $scope.Cards[i].type;
				var __status =  $scope.Cards[i].status;
				var __filter  = filter={coverage:$scope.Dashboard.coverage,type:__type,status:__status};
				transactionService.getTransactions(successGetTransactions,errorGetTransactions,$scope,{filter:__filter,dashboard:$scope.Dashboard.type});
			}
	   }
	   function successGetTransactions($response, $status, $scope){
	   	var __labels,__data,__total;
	   	__labels = [];
	   	__data =[];
	   	__total = 0;
	   	for(var i in $response.data){
	   		var datum =  $response.data[i];
	   		__labels.push(datum.date);
	   		__data.push(datum.amount);
	   		__total+=datum.amount;

	   	}
	   	var __type =  $response.meta.type;
	   	var card = $scope.Cards[__type];
	   	if(__data.length){
	   		card.labels =  __labels;
	   		card.data =  [__data];
	   		card.average = __total/__data.length;
	   	}else{
	   		card.labels =  ["NO DATA"];
	   		card.data =  [[0]];
	   		card.average = 0;
	   	}
	   		
	   }
	   function errorGetTransactions($response, $status, $scope){}
	  
    }]);
});


