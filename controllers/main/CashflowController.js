

define(['config', 'api','alertsService','cashflowService','cashflowModalController'], function(app, api) {
    
    app.register.controller('cashflowController', ['$modal', '$scope', '$rootScope','alertsService', 'cashflowService', function($modal, $scope, $rootScope, alertsService,cashflowService) {
        const LIMIT = 10;
        const NAV_PAGE_COUNT = 10;
        $scope.initializeController = function() {
            $rootScope.moduleName = 'Dashboard';
            $scope.FilterKeys = {
                entity: 'customer'
            };
            $scope.Entities = [
            {
                id: 'customer',
                value: 'Customers'
            }, 
            {
                id: 'supplier',
                value: 'Suppliers'
            }, 
            ];
            $scope.TransactionTypes = [
            {
                id: 'sales',
                value: 'Sales'
            }, 
            {
                id: 'tradein',
                value: 'Trade In\'s'
            }, 
            {
                id: 'return',
                value: 'Returns'
            }, 
            {
                id: 'deliveries',
                value: 'Deliveries'
            }, 
            {
                id: 'orders',
                value: 'Orders'
            }, 
            ];
            $scope.Coverages = [
            {
                id: 'today',
                value: 'Today'
            }, 
            {
                id: '7D',
                value: 'Last 7 days'
            }, 
            {
                id: '30D',
                value: 'Last 30 days'
            }, 
            ];
            initCashflow();
            initModal();
            $scope.$on('cashflowSaved',cashflowSaved);
        }
        function initCashflow() {
            $scope.Pages = [];
            $scope.Cashflows = [];
            $scope.FetchCustomerCashflowId = null ;
            $scope.movePage(1);
        }
        $scope.setFilterKey = function(field, value) {
            $scope.FilterKeys[field] = value;
        }
        $scope.cancelFilter = function() {
            $scope.FilterKeys = {
                entity: angular.copy($scope.FilterKeys.entity)
            };
            $scope.Cashflows = [];
            $scope.Pages = [];
            $scope.movePage(1, null , $scope.FilterKeys);
            $scope.FilterEnabled = false;
            //$scope.toggleFilter();
        }
        $scope.confirmFilter = function() {
            $scope.Cashflows = [];
            $scope.Pages = [];
            $scope.movePage(1, null , $scope.FilterKeys);
            $scope.FilterEnabled = false;
            //$scope.toggleFilter();
        }
        $scope.toggleFilter = function() {
            $scope.FilterEnabled = !$scope.FilterEnabled;
        }
        $scope.searchFor = function(keyword) {
            $scope.Cashflows = [];
            $scope.Pages = [];
            $scope.SearchKeyword = angular.copy(keyword);
            var __filter = {
                entity: angular.copy($scope.FilterKeys.entity)
            };
            $scope.movePage(1, $scope.SearchKeyword, __filter);
        }
        $scope.resetSearch = function() {
            $scope.Pages = [];
            $scope.SearchEnabled = false;
            $scope.cashflowSearchBox = null ;
            $scope.SearchKeyword = null ;
            $scope.Cashflows = [];
            $scope.FetchProductId = null ;
            $scope.movePage(1);
        }
        $scope.cashflowSearchFilter = function(cashflow) {
            var searchBox = $scope.cashflowSearchBox;
            var keyword = new RegExp(searchBox,'i');
            var test = keyword.test(cashflow.particulars) || keyword.test(cashflow.entity.name) || keyword.test(cashflow.ref_no);
            return !searchBox || test;
            //Return NO FILTER or filter by patient_name
        }
        $scope.movePage = function(__page, __keyword, __filter) {
            $scope.NewCashflow = null ;
            $scope.SearchEnabled = __keyword ? true : false;
            if (!__keyword)
                $scope.productSearchBox = null ;
            if (!__filter)
                __filter = $scope.FilterKeys = {
                    entity: 'customer'
                };
            if (__page > $scope.LastPage && !__keyword && $scope.LastPage > 0) {
                $scope.GoToPage = $scope.LastPage;
                return alert('Oops! You can only go up to page ' + $scope.LastPage);
            }
            $scope.CurrentPage = parseInt(__page);
            $scope.CurrentOffset = Math.ceil(__page / NAV_PAGE_COUNT) - 1;
            getCashflows(__page, __keyword, __filter, LIMIT);
        }
        function getCashflows(__page, __keyword, __filter, __limit) {
            $scope.LoadingCashflows = true;
            __filter = angular.copy(__filter);
            var entity = __filter.entity;
            cashflowService.getCashflows(successGetCashflows, errorGetCashflows, $rootScope, {
                page: __page,
                keyword: __keyword,
                limit: __limit,
                filter: __filter
            });
        
        }
        function successGetCashflows($response, $status, $rootScope) {
            $scope.Cashflows = [];
            for (var index in $response.data) {
                var entity = $response.data[index];
                entity.created = new Date(entity.created);
                entity.timestamp = new Date(entity.timestamp);
                $scope.Cashflows.push(entity);
            }
            $scope.LoadingCashflows = false;
            $scope.GoToPage = null ;
            paginate($response.meta.pages);
        }
        function errorGetCashflows($response, $status, $scope) {}
        function paginate(__pages) {
            $scope.EnablePagination = true;
            if ($scope.Pages.length == 0) {
                for (var i = 1; i <= __pages; i++) {
                    $scope.Pages.push(i);
                }
                $scope.LastPage = $scope.Pages.length;
            }
            updatePages();
        }
        function updatePages() {
            $scope.ActivePages = [];
            for (var i = $scope.CurrentOffset * NAV_PAGE_COUNT, ctr = 1; i < $scope.Pages.length && ctr <= NAV_PAGE_COUNT; i++,
            ctr++) {
                $scope.ActivePages.push($scope.Pages[i]);
            }
        }
        function initModal() {
            var CashflowModalInstanceController = function($scope, $modalInstance) {
                //Confirm handler
                $scope.confirm = function(cashflowId) {
                    $rootScope.Progress = 50;
                    $rootScope.$broadcast(cashflowId ? 'updateCashflow' : 'addCashflow');
                    $modalInstance.close(cashflowId);
                }
                ;
                //Cancel handler
                $scope.cancel = function() {
                    $modalInstance.dismiss('cancel');
                }
                ;
            
            }
            //openCashflowModal function
            $scope.openCashflowModal = function() {
                var modalInstance = $modal.open({
                    templateUrl: 'cashflowModalContent.html',
                    controller: CashflowModalInstanceController,
                    windowClass: 'app-modal-window',
                });
                modalInstance.result.then(function(data) {
                //console.log(data,'xx');
                }, function(data) {
                    console.log(data);
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            }
            ;
        }
        function cashflowSaved(evt,args){
			var data = args.response.data;
			alertsService.RenderSuccessMessage('New cashflow transaction added successfully!');
			$rootScope.Progress = 100;
			$scope.confirmFilter();
			$scope.NewCashflow = data.id;
		}
    
    }
    ]);
});
