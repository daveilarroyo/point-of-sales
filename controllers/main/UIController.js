define(function(){
	return 	function($scope,  $rootScope, $modal) {	
	   $scope.initializeController = function () { 
		console.log('initializeController');
	   }
	   $scope.toggle = function(){
		   $rootScope.isToggled = !$rootScope.isToggled;
		   
	   }
		//Init Root Modal
		var RootModalInstanceController = function ($scope, $modalInstance, data, args) {
			if(data){
				for(var $index in data){
					$scope[$index] = data[$index];
				}
				
				if(data.hasOwnProperty('callback')) data.callback($scope,$modalInstance);
			}
			$scope.buttons = null;
			if(args){
				console.log(args);
				if(args.buttons){
					var buttons = args.buttons;
					$scope.buttons = [];
					for(var index in buttons){
						$scope.buttons.push({label:index,callback:function(){$modalInstance.close(); buttons[index]();}});
					}
				}
			}
		};
		$scope.$on('openRootModal',function(evt,data,args){
			$scope.openRootModal(data,args);
		});
		$scope.openRootModal = function(data,args){
			
			var modalInstance = $modal.open({
					templateUrl: 'rootModalContent.html',
					controller: RootModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 data: function() {return data;},
						 args: function() {return args;}
					}
				});
		}
	}
});
