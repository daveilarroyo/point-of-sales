
define(['config','api','settings','test','searchController'], function (app,api,settings,test) {
    app.register.controller('loginController',['$scope',  '$rootScope','$location','$cookieStore', function ($scope,  $rootScope,$location,$cookie) {
       $scope.initializeController = function () { 
			$rootScope.IsloggedIn = false;
			$scope.ValidUser = false;
			$rootScope.IsAuthenicated = false;
			if($location.path()=='/logout')  $scope.logout();
	   };
	   
	    $scope.toggleSearch = function(){
			$scope.SearchActive =  !$scope.SearchActive;
			$scope.$broadcast($scope.SearchActive?'initSearch':'clearSearch');
		   }
	   $scope.signIn = function(){
			$scope.Validating = true;
			var data = {username:$scope.username,password:$scope.password}; //Get actual data
			data.forceLive = true;
			api.authenticate(validUserFn,invalidUserFn,$scope,data);
	   };
	   $scope.enterDashboard = function(){
			$rootScope.IsloggedIn = true;
			window.location = '#/';
	   }
	    $scope.logout = function(){
			api.logout(logoutFn,logoutFn,$scope,{forceLive:true});
	   }
	   function logoutFn(){
			$cookie.remove('__user');
			$cookie.remove('__modules');
			$rootScope.IsloggedIn = false;
			$scope.SignedIn = false;
			$scope.ValidUser = null;
			$rootScope.User=null;
			$rootScope.Modules = null;
			$rootScope.isToggled = false;
			api.redirect('#/login');
	   }
	   function validUserFn($response, $status, $scope){
			$rootScope.User = $response.User;
			$rootScope.Modules = $response.Module;
			var __user = $response.User;
			var __modules = [];
			angular.forEach($response.Module,function (object,index){
				var __module = {};
				__module.title = object.title;
				__module.description = object.description;
				__module.link = object.link;
				__module.icon = object.icon;
				__module.type = object.type;
				__modules.push(__module);
			});
			$cookie.put('__user',$response.User);
			$cookie.put('__modules',__modules);
			$scope.SignedIn = true;
			$scope.ValidUser = true;
			$rootScope.IsloggedIn = true;
			$scope.Validating=false;
			window.location = '#/';
	   }
	   function invalidUserFn($response, $status, $scope){
		   $scope.Validating=false;
		   $scope.SignedIn = true;
		   $scope.ValidUser = false;
		   $scope.username = null;
		   $scope.password = null;
	   }
    }]);
});


