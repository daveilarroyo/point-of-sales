

define(['config','accountModalController','customerService','vendorService','alertsService'], function (app) {

    app.register.controller('AccountsController',['$modal','$scope',  '$rootScope','customerService','vendorService','alertsService', function ($modal,$scope,$rootScope,customerService,vendorService,alertsService) {
		const LIMIT = 10;
		const NAV_PAGE_COUNT = 10;
       $scope.initializeController = function () {
		   $scope.FilterKeys = {entity:'customer'};
		   $scope.Entities = [
								{id:'customer',value:'Customers'},
								{id:'supplier',value:'Suppliers'},
							];
			initAccount();
			initModal();
			$scope.$on('accountSaved',accountSaved);
			$scope.$watch('Accounts',function(newvalue){
				$scope.Fillers = [];
				if(newvalue.length<NAV_PAGE_COUNT){
					for(var i=1;i<=NAV_PAGE_COUNT - newvalue.length;i++){
						$scope.Fillers.push('');
					}
					if(newvalue.length==0) $scope.Fillers.splice(0,1);
				}
			});
	   }
	   $scope.setFilterKey = function(field,value){
			$scope.FilterKeys[field] = value;
		}
		$scope.cancelFilter = function(){
			$scope.FilterKeys = {entity:angular.copy($scope.FilterKeys.entity)};
			$scope.Accounts = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.confirmFilter = function(currentPage){
			if(!currentPage) currentPage = 1;
			$scope.Accounts = [];
			$scope.Pages = [];
			$scope.FetchAccountId=null;
			$scope.movePage(currentPage,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.toggleFilter = function(){ 
			$scope.FilterEnabled = !$scope.FilterEnabled;
		}
		$scope.searchFor =function(keyword){
			$scope.Accounts = [];
			$scope.Pages = [];
			$scope.SearchKeyword = angular.copy(keyword);
			var __filter =  {entity:angular.copy($scope.FilterKeys.entity)};
			$scope.movePage(1,$scope.SearchKeyword,__filter);
		}
		$scope.resetSearch =function(){
			$scope.Pages = [];
			$scope.SearchEnabled = false;
			$scope.accountSearchBox=null;
			$scope.SearchKeyword=null;
			$scope.Accounts = [];
			$scope.FetchAccountId=null;
			$scope.movePage(1);
		}
		$scope.accountSearchFilter = function (account){
			var searchBox = $scope.accountSearchBox;
			var keyword = new RegExp(searchBox, 'i');
			var test = keyword.test(account.name);
			return !searchBox || test ; //Return NO FILTER or filter by patient_name
		}
	   $scope.movePage= function(__page,__keyword,__filter){
			$scope.NewAccount = null;
			$scope.SearchEnabled = __keyword? true:false;
			if(!__keyword) $scope.productSearchBox=null;
			if(!__filter) __filter = $scope.FilterKeys={entity:'customer'};
			if(__page>$scope.LastPage&&!__keyword && $scope.LastPage>0){
				$scope.GoToPage = $scope.LastPage;
				return alert('Oops! You can only go up to page '+$scope.LastPage);
			}
			$scope.CurrentPage = parseInt(__page);
			$scope.CurrentOffset = Math.ceil(__page/NAV_PAGE_COUNT) - 1;
			getAccounts(__page,__keyword,__filter,LIMIT);
		}
		$scope.exportData = function(__keyword,__filter){
			__filter =  angular.copy(__filter);
		   var entity = __filter.entity;
		   switch(entity){
			case 'customer':
				customerService.exportData({keyword:__keyword,filter:__filter});
			break;
			case 'supplier':
				vendorService.exportData({keyword:__keyword,filter:__filter});
			break;
			
		   }
		}
		$scope.editAccount =  function(accountId){
			if($scope.LoadingAccounts) return alert('Accounts are still being loaded. Please wait');
			if($scope.FetchingAccount) return alert('An account is still being loaded. Please wait');
			if($scope.FetchAccountId) return alert('An account is still being saved. Please wait');
			$scope.FetchingAccount = true;
			$scope.FetchAccountId = accountId;
			$scope.NewAccount = null;
			var entity = angular.copy($scope.FilterKeys.entity);
			 switch(entity){
				 case 'customer':
					customerService.getCustomer(successGetAccount,errorGetAccount,$scope,accountId);
				 break;
				 case 'supplier':
					vendorService.getVendor(successGetAccount,errorGetAccount,$scope,accountId);
				 break;
			 }
		}
		function initAccount(){
			$scope.Pages = [];
			$scope.Accounts = [];
			$scope.FetchAccountId=null;			
			$scope.movePage(1);
		}
		function initModal(){
			var AccountModalInstanceController = function ($scope, $modalInstance, account) {
				//Confirm handler
				$scope.confirm = function (accountId) {
					$rootScope.Progress = 50; 
					$rootScope.$broadcast(accountId?'updateAccount':'addAccount');
					$modalInstance.close(accountId);
				};
				$scope.openAccount = function(accountId){
					$rootScope.Progress = 50; 
					$rootScope.$broadcast('openAccount');
					$modalInstance.close(accountId);
				}
				$scope.closeAccount = function(accountId){
					$rootScope.Progress = 50; 
					$rootScope.$broadcast('closeAccount');
					$modalInstance.close(accountId);
				}
				$scope.postAccount = function(accountId){
					$rootScope.Progress = 50; 
					$scope.begin_balance =  $scope.current_balance;
					$scope.charges = $scope.payments = 0;
					if(prompt('Enter ' +$scope.current_balance +' to confirm') ==$scope.current_balance){
						$rootScope.$broadcast('postAccount');
						$modalInstance.close(accountId);
					}
				}
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				if(account){
					for(var $index in account){
						$scope[$index] = account[$index];
					}
				}
				
			};
			//openAccountModal function
			$scope.openAccountModal = function (account) {
				account = account || {};
				account.accountType = angular.copy($scope.FilterKeys.entity);
				var modalInstance = $modal.open({
					templateUrl: 'accountModalContent.html',
					controller: AccountModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 account: function() {return account;}
					}
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					$scope.FetchAccountId = null;
					console.log(data);
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
		function accountSaved(evt,args){
			var data = args.response.data;
			alertsService.RenderSuccessMessage('Account successfully saved!');
			$rootScope.Progress = 100;
			var __filter =  {entity:angular.copy($scope.FilterKeys.entity)};
			$scope.movePage($scope.CurrentPage,$scope.SearchKeyword,__filter);
			$scope.FetchAccountId = null;
			$scope.NewAccount = data.id;
		}
	   function getAccounts(__page,__keyword,__filter,__limit){
		   $scope.LoadingAccounts = true;
		   __filter =  angular.copy(__filter);
		   var entity = __filter.entity;
		   switch(entity){
				case 'customer':
					customerService.getCustomers(successGetAccounts,errorGetAccounts,$rootScope,{page:__page,keyword:__keyword,limit:__limit,filter:__filter});
				break;
				case 'supplier':
					vendorService.getVendors(successGetAccounts,errorGetAccounts,$rootScope,{page:__page,keyword:__keyword,limit:__limit,filter:__filter});
				break;
		   }
			
		}
		function successGetAccounts($response, $status, $rootScope){
			$scope.Accounts = $response.data;
			$scope.LoadingAccounts = false; 
			$scope.GoToPage = null;
			paginate($response.meta.pages);
		}
		function errorGetAccounts($response, $status, $scope){ }
		function successGetAccount($response, $status, $scope){
			var data = {};
			$scope.openAccountModal($response.data);
			$scope.FetchingAccount =false;
		}
		function errorGetAccount($response, $status, $scope){ }
		function paginate(__pages){
			$scope.EnablePagination = true;
			if($scope.Pages.length==0){
				for(var i=1;i<=__pages;i++){
					$scope.Pages.push(i);
				}
				$scope.LastPage = $scope.Pages.length;
			}
			updatePages();
		}
		function updatePages(){
			$scope.ActivePages=[];
			for(var i=$scope.CurrentOffset*NAV_PAGE_COUNT,ctr=1;i<$scope.Pages.length&&ctr<=NAV_PAGE_COUNT;i++,ctr++){
				$scope.ActivePages.push($scope.Pages[i]);
			}
		}
    }]);
});


