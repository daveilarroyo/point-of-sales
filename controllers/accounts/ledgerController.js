

define(['config','ledgerModalController','customerService','vendorService','alertsService'], function (app) {

    app.register.controller('LedgersController',['$modal','$scope',  '$rootScope','customerService','vendorService','alertsService', function ($modal,$scope,$rootScope,customerService,vendorService,alertsService) {
		const LIMIT = 10;
		const NAV_PAGE_COUNT = 10;
       $scope.initializeController = function () {
		   $scope.FilterKeys = {entity:'customer'};
		   $scope.Entities = [
								{id:'customer',value:'Customers'},
								{id:'supplier',value:'Suppliers'},
							];
			$scope.TransactionTypes = [
								{id:'sales',value:'Sales'},
								{id:'tradein',value:'Trade In\'s'},
								{id:'return',value:'Returns'},
								{id:'deliveries',value:'Deliveries'},
								{id:'orders',value:'Orders'},
							];
			$scope.Coverages = [
								{id:'today',value:'Today'},
								{id:'7D',value:'Last 7 days'},
								{id:'30D',value:'Last 30 days'},
							];		   
			initLedger();
			initModal();
			$scope.$on('ledgerSaved',ledgerSaved);
			$scope.$watch('Ledgers',function(newvalue){
				$scope.Fillers = [];
				if(newvalue.length<NAV_PAGE_COUNT){
					for(var i=1;i<=NAV_PAGE_COUNT - newvalue.length;i++){
						$scope.Fillers.push('');
					}
					if(newvalue.length==0) $scope.Fillers.splice(0,1);
				}
			});
	   }
	   $scope.setFilterKey = function(field,value){
			$scope.FilterKeys[field] = value;
		}
		$scope.cancelFilter = function(){
			$scope.FilterKeys = {entity:angular.copy($scope.FilterKeys.entity)};
			$scope.Ledgers = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.confirmFilter = function(){
			$scope.Ledgers = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.toggleFilter = function(){ 
			$scope.FilterEnabled = !$scope.FilterEnabled;
		}
		$scope.searchFor =function(keyword){
			$scope.Ledgers = [];
			$scope.Pages = [];
			keyword = encodeURIComponent(keyword);
			$scope.SearchKeyword = angular.copy(keyword);
			var __filter =  {entity:angular.copy($scope.FilterKeys.entity)};
			$scope.movePage(1,$scope.SearchKeyword,__filter);
		}
		$scope.resetSearch =function(){
			$scope.Pages = [];
			$scope.SearchEnabled = false;
			$scope.ledgerSearchBox=null;
			$scope.SearchKeyword=null;
			$scope.Ledgers = [];
			$scope.FetchProductId=null;
			$scope.movePage(1);
		}
		$scope.ledgerSearchFilter = function (ledger){
			var searchBox = $scope.ledgerSearchBox;
			var keyword = new RegExp(searchBox, 'i');
			var test = keyword.test(ledger.particulars)||keyword.test(ledger.entity.name)||keyword.test(ledger.ref_no);
			return !searchBox || test ; //Return NO FILTER or filter by patient_name
		}
	   $scope.movePage= function(__page,__keyword,__filter){
			$scope.NewLedger = null;
			$scope.SearchEnabled = __keyword? true:false;
			if(!__keyword) $scope.productSearchBox=null;
			if(!__filter) __filter = $scope.FilterKeys={entity:'customer'};
			if(__page>$scope.LastPage&&!__keyword && $scope.LastPage>0){
				$scope.GoToPage = $scope.LastPage;
				return alert('Oops! You can only go up to page '+$scope.LastPage);
			}
			$scope.CurrentPage = parseInt(__page);
			$scope.CurrentOffset = Math.ceil(__page/NAV_PAGE_COUNT) - 1;
			getLedgers(__page,__keyword,__filter,LIMIT);
		}
		$scope.exportData = function(__keyword,__filter){
			__filter =  angular.copy(__filter);
		   var entity = __filter.entity;
		   switch(entity){
			case 'customer':
				customerService.exportData({keyword:__keyword,filter:__filter});
			break;
			case 'supplier':
				vendorService.exportData({keyword:__keyword,filter:__filter});
			break;
			
		   }
		}
		function initLedger(){
			$scope.Pages = [];
			$scope.Ledgers = [];
			$scope.FetchCustomerLedgerId=null;			
			$scope.movePage(1);
		}
		function initModal(){
			var LedgerModalInstanceController = function ($scope, $modalInstance, ledger) {
				//Confirm handler
				$scope.confirm = function (ledgerId) {
					$rootScope.Progress = 50; 
					$rootScope.$broadcast(ledgerId?'updateLedger':'addLedger');
					$modalInstance.close(ledgerId);
				};
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				if(ledger){
					for(var $index in ledger){
						$scope[$index] = ledger[$index];
					}
				}
				
			};
			//openLedgerModal function
			$scope.openLedgerModal = function () {
				var ledger = {transacteeType:angular.copy($scope.FilterKeys.entity)};
				var modalInstance = $modal.open({
					templateUrl: 'ledgerModalContent.html',
					controller: LedgerModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 ledger: function() {return ledger;}
					}
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					console.log(data);
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
		function ledgerSaved(evt,args){
			var data = args.response.data;
			alertsService.RenderSuccessMessage('New ledger transaction added successfully!');
			$rootScope.Progress = 100;
			$scope.confirmFilter();
			$scope.NewLedger = data.id;
		}
	   function getLedgers(__page,__keyword,__filter,__limit){
		   $scope.LoadingLedgers = true;
		   __filter =  angular.copy(__filter);
		   var entity = __filter.entity;
		   switch(entity){
				case 'customer':
					customerService.getCustomerLedgers(successGetLedgers,errorGetLedgers,$rootScope,{page:__page,keyword:__keyword,limit:__limit,filter:__filter});
				break;
				case 'supplier':
					vendorService.getVendorLedgers(successGetLedgers,errorGetLedgers,$rootScope,{page:__page,keyword:__keyword,limit:__limit,filter:__filter});
				break;
		   }
			
		}
		function successGetLedgers($response, $status, $rootScope){
			$scope.Ledgers = [];
			for(var index in $response.data){
				var entity =  $response.data[index];
					entity.entity = {};
				var entity_type = entity.hasOwnProperty('Customer')?'customer':'supplier';
				switch(entity_type){
					case 'customer':
						entity.entity.id = entity.Customer.id;
						entity.entity.name = entity.Customer.name;
					break;
					case 'supplier':
						entity.entity.id = entity.Supplier.id;
						entity.entity.name = entity.Supplier.name;
					break;
				}
				entity.created =  new Date(entity.created);
				entity.timestamp =  new Date(entity.timestamp);
				$scope.Ledgers.push(entity);
			}
			$scope.LoadingLedgers = false; 
			$scope.GoToPage = null;
			paginate($response.meta.pages);
		}
		function errorGetLedgers($response, $status, $scope){ }
		function paginate(__pages){
			$scope.EnablePagination = true;
			if($scope.Pages.length==0){
				for(var i=1;i<=__pages;i++){
					$scope.Pages.push(i);
				}
				$scope.LastPage = $scope.Pages.length;
			}
			updatePages();
		}
		function updatePages(){
			$scope.ActivePages=[];
			for(var i=$scope.CurrentOffset*NAV_PAGE_COUNT,ctr=1;i<$scope.Pages.length&&ctr<=NAV_PAGE_COUNT;i++,ctr++){
				$scope.ActivePages.push($scope.Pages[i]);
			}
		}
    }]);
});


