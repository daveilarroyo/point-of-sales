

define(['config','userModalController','userService','alertsService'], function (app) {

    app.register.controller('UsersController',['$modal','$scope',  '$rootScope','userService','alertsService', function ($modal,$scope,$rootScope,userService,alertsService) {
		const LIMIT = 10;
		const NAV_PAGE_COUNT = 10;
       $scope.initializeController = function () {
		   $scope.FilterKeys = {entity:'staff'};
		   $scope.Entities = [
								{id:'admin',value:'Administrator'},
								{id:'staff',value:'Staff'},
							];
			initUser();
			initModal();
			$scope.$on('userSaved',userSaved);
			$scope.$watch('Users',function(newvalue){
				$scope.Fillers = [];
				if(newvalue.length<NAV_PAGE_COUNT){
					for(var i=1;i<=NAV_PAGE_COUNT - newvalue.length;i++){
						$scope.Fillers.push('');
					}
					if(newvalue.length==0) $scope.Fillers.splice(0,1);
				}
			});
	   }
	   $scope.setFilterKey = function(field,value){
			$scope.FilterKeys[field] = value;
		}
		$scope.cancelFilter = function(){
			$scope.FilterKeys = {entity:angular.copy($scope.FilterKeys.entity)};
			$scope.Users = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.confirmFilter = function(currentPage){
			if(!currentPage) currentPage = 1;
			$scope.Users = [];
			$scope.Pages = [];
			$scope.FetchUserId=null;
			$scope.movePage(currentPage,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.toggleFilter = function(){ 
			$scope.FilterEnabled = !$scope.FilterEnabled;
		}
		$scope.searchFor =function(keyword){
			$scope.Users = [];
			$scope.Pages = [];
			$scope.SearchKeyword = angular.copy(keyword);
			var __filter =  {entity:angular.copy($scope.FilterKeys.entity)};
			$scope.movePage(1,$scope.SearchKeyword,__filter);
		}
		$scope.resetSearch =function(){
			$scope.Pages = [];
			$scope.SearchEnabled = false;
			$scope.userSearchBox=null;
			$scope.SearchKeyword=null;
			$scope.Users = [];
			$scope.FetchUserId=null;
			$scope.movePage(1);
		}
		$scope.userSearchFilter = function (user){
			var searchBox = $scope.userSearchBox;
			var keyword = new RegExp(searchBox, 'i');
			var test = keyword.test(user.name);
			return !searchBox || test ; //Return NO FILTER or filter by patient_name
		}
	   $scope.movePage= function(__page,__keyword,__filter){
			$scope.NewUser = null;
			$scope.SearchEnabled = __keyword? true:false;
			if(!__keyword) $scope.productSearchBox=null;
			if(!__filter) __filter = $scope.FilterKeys={entity:'staff'};
			if(__page>$scope.LastPage&&!__keyword && $scope.LastPage>0){
				$scope.GoToPage = $scope.LastPage;
				return alert('Oops! You can only go up to page '+$scope.LastPage);
			}
			$scope.CurrentPage = parseInt(__page);
			$scope.CurrentOffset = Math.ceil(__page/NAV_PAGE_COUNT) - 1;
			getUsers(__page,__keyword,__filter,LIMIT);
		}
		$scope.exportData = function(__keyword,__filter){
			__filter =  angular.copy(__filter);
		   var entity = __filter.entity;
		   switch(entity){
			case 'user':
				userService.exportData({keyword:__keyword,filter:__filter});
			break;
			case 'supplier':
				vendorService.exportData({keyword:__keyword,filter:__filter});
			break;
			
		   }
		}
		$scope.editUser =  function(userId){
			if($scope.LoadingUsers) return alert('Users are still being loaded. Please wait');
			if($scope.FetchingUser) return alert('An user is still being loaded. Please wait');
			if($scope.FetchUserId) return alert('An user is still being saved. Please wait');
			$scope.FetchingUser = true;
			$scope.FetchUserId = userId;
			$scope.NewUser = null;
			userService.getUser(successGetUser,errorGetUser,$scope,userId);
		}
		function initUser(){
			$scope.Pages = [];
			$scope.Users = [];
			$scope.FetchUserId=null;			
			$scope.movePage(1);
		}
		function initModal(){
			var UserModalInstanceController = function ($scope, $modalInstance, user) {
				//Confirm handler
				$scope.confirm = function (userId) {
					$rootScope.Progress = 50; 
					$rootScope.$broadcast(userId?'updateUser':'addUser');
					$modalInstance.close(userId);
				};
				$scope.openUser = function(userId){
					$rootScope.Progress = 50; 
					$rootScope.$broadcast('openUser');
					$modalInstance.close(userId);
				}
				$scope.closeUser = function(userId){
					$rootScope.Progress = 50; 
					$rootScope.$broadcast('closeUser');
					$modalInstance.close(userId);
				}
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				if(user){
					for(var $index in user){
						$scope[$index] = user[$index];
					}
				}
				
			};
			//openUserModal function
			$scope.openUserModal = function (user) {
				user = user || {access:[]};
				user.type = angular.copy($scope.FilterKeys.entity);
				var modalInstance = $modal.open({
					templateUrl: 'userModalContent.html',
					controller: UserModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 user: function() {return user;}
					}
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					$scope.FetchUserId = null;
					console.log(data);
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
		function userSaved(evt,args){
			var data = args.response.data;
			alertsService.RenderSuccessMessage('User successfully saved!');
			$rootScope.Progress = 100;
			$scope.confirmFilter($scope.CurrentPage);
			$scope.NewUser = data.id;
		}
	   function getUsers(__page,__keyword,__filter,__limit){
		   $scope.LoadingUsers = true;
		   __filter =  angular.copy(__filter);
		   var entity = __filter.entity;
		   userService.getUsers(successGetUsers,errorGetUsers,$rootScope,{page:__page,keyword:__keyword,limit:__limit,filter:__filter});
			
		}
		function successGetUsers($response, $status, $rootScope){
			$scope.Users = $response.data;
			$scope.LoadingUsers = false; 
			$scope.GoToPage = null;
			paginate($response.meta.pages);
		}
		function errorGetUsers($response, $status, $scope){ }
		function successGetUser($response, $status, $scope){
			var data = {};
			$scope.openUserModal($response.data);
			$scope.FetchingUser =false;
		}
		function errorGetUser($response, $status, $scope){ }
		function paginate(__pages){
			$scope.EnablePagination = true;
			if($scope.Pages.length==0){
				for(var i=1;i<=__pages;i++){
					$scope.Pages.push(i);
				}
				$scope.LastPage = $scope.Pages.length;
			}
			updatePages();
		}
		function updatePages(){
			$scope.ActivePages=[];
			for(var i=$scope.CurrentOffset*NAV_PAGE_COUNT,ctr=1;i<$scope.Pages.length&&ctr<=NAV_PAGE_COUNT;i++,ctr++){
				$scope.ActivePages.push($scope.Pages[i]);
			}
		}
    }]);
});


