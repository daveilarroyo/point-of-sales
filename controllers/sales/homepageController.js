

define(['config','productModalController','productTypeaheadController','transactionModalController','transactionService','customerService'], function (app,jquery) {

    app.register.controller('SalesController',['$modal','$scope', '$timeout', '$rootScope','transactionService','customerService', function ($modal,$scope,$timeout,$rootScope,transactionService,customerService) {
		const DISCOUNT_LIMIT = 0.50;
		var __buffer;
		var __page;
       $scope.initializeController = function () {
			initSales();
			$rootScope.moduleName = 'Sales';
			$rootScope.AllowAddTemporaryProduct = false;
			$scope.typeAheadOptions = {
				highlight: true
			 };
			 $scope.typeAheadData = {
				displayKey: 'name',
				source: {},
			  };
			__buffer = [];
			__page = 1;
			customerService.getCustomers(successGetCustomers,errorGetCustomers,$scope,{page:__page});
			initModal();
			$scope.$on('productAdded',productAdded);
	   }
	   $scope.resetCustomerObject = function(){
		   $scope.CustomerObject = null;
		   $scope.CustomerObjectTypeAhead = null;
	   }
	   function productAdded(evt,args){
			var data = args.response.data;
			console.log(data);
		}
	   $scope.$on('addItem',function(event,data){
			data.amount = data.price * data.quantity;
			$scope.SalesAmount += data.amount;
			$scope.SalesItems.push(data);
	   });
	   $scope.resetSales = function(){
		  if(confirm('Are you sure you want to cancel this transaction?')){
			  initSales();
		  }
	   }
	   $scope.validateCustomer = function(customer){
		   if(customer){
			if(customer.id==undefined){
				alert('Invalid customer');
				$scope.resetCustomerObject();
			}
		   }else{
			   $scope.FocusTypeahead = true;
			   $scope.resetCustomerObject();
		   }
	   }
	   $scope.$on('typeahead:selected',function(event,data){
		   if(data.hasOwnProperty('last_bill'))
				$scope.CustomerObject = data;
	   });
	   $scope.$on('resetTransaction',function(){
		   initSales();
	   });
	   $scope.toggleEdit = function($mode){
		   switch($mode){
				case 'edit': $scope.editMode =  true; break;
				case 'save': 
					$scope.editMode =  false; 
					var totalAmount = 0;
					var items = $scope.SalesItems;
					for(var index in items){
						var item = items[index];
						item.amount = item.price * item.quantity ;
						totalAmount += item.amount;
						items[index] = item;
					}
					$scope.SalesItems = items;
					$scope.SalesAmount = totalAmount;
				break;
		   }
	   }
	   $scope.toggleDelete = function($mode){
		   switch($mode){
				case 'delete': $scope.deleteMode =  true; break;
				case 'save': 
					$scope.deleteMode =  false; 
					var totalAmount = 0;
					var items = $scope.SalesItems;
					var cache =[];
					for(var index in items){
						var item = items[index];
						if(!item.is_delete){
							cache.push(item);
							totalAmount += item.amount;
						}
					}
					$scope.SalesItems = cache;
					$scope.SalesAmount = totalAmount;
				break;
		   }
	   }
	   function initSales(){
			$timeout(function(){
				$scope.CustomerObject = '';
				$scope.CustomerObjectTypeahead  = '';
				$scope.SalesItems = [];
				$scope.SalesAmount = 0;
				}, 100);
	   }
	   function initModal(){
			var TransactionModalInstanceController = function ($scope, $modalInstance, transaction) {
		
				//Confirm handler
				$scope.confirm = function (transactionId) {
					$rootScope.$broadcast(transactionId?'updateTransaction':'addTransaction');
					$modalInstance.close(transactionId);
				};
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				
				if(transaction){
					for(var $index in transaction){
						$scope[$index] = transaction[$index];
					}
				}
				
			};
			//openTransactionModal function
			$scope.openTransactionModal = function () {
				var transaction = {header:{},details:[]};
				var invoice_date = new Date();
					invoice_date = invoice_date.getFullYear()+'-'+(invoice_date.getMonth()+1)+'-'+invoice_date.getDate();
				transaction.header.type='sales';
				transaction.header.entity_type='customer';
				transaction.header.entity=transaction.header.customer=$scope.CustomerObject;
				transaction.header.entity_id=$scope.CustomerObject.id;
				transaction.header.date=transaction.header.invoice_date=invoice_date;
				transaction.header.amount=transaction.header.total=$scope.SalesAmount;
				transaction.header.status = 'fulfilled';
				transaction.header.total_allowed_discount = 0;
				$.each($scope.SalesItems,function(i,o){
					o.quantity = o.quantity*o.area;
					var detail = {product_id:o.id,quantity:o.quantity,price:o.price,amount:o.amount};
					
					if(o.discountable){
						transaction.header.total_allowed_discount  += (o.markup*DISCOUNT_LIMIT);
					}
					transaction.details.push(detail);
				});
				var modalInstance = $modal.open({
					templateUrl: 'createTransactionModalContent.html',
					controller: TransactionModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 transaction: function() {return transaction;}
					}
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					console.log(data);
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
		function successGetCustomers($response,$status,$scope){
			 __buffer =  __buffer.concat($response.data);
				$rootScope.__Progress = (__page / $response.meta.pages) *100;
				if($response.meta.next){
					__page =  __page + 1;
					return customerService.getCustomers(successGetCustomers,errorGetCustomers,$scope,{page:__page});
				}else{
					$timeout(function(){
					//$rootScope.__Progress = 0;
					},1500);
				}
		   var customers = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: __buffer,
			  });

			  // initialize the bloodhound suggestion engine
			  customers.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  $scope.addValue = function () {
				customers.add({
				  name: 'twenty'
				});
			  };

			  // Typeahead options object
			  $scope.typeAheadOptions = {
				highlight: true
			  };

			  // Single dataset example
			  $scope.typeAheadData = {
				displayKey: 'name',
				source: customers.ttAdapter()
			  };
			  
			  $scope.CustomerObject = null;
	   }
	   function errorGetCustomers($response,$status,$scope){}
    }]);
});


