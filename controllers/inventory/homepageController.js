

define(['config','productModalController','productService','alertsService'], function (app) {

    app.register.controller('InventoryController',['$modal','$scope',  '$rootScope','productService','alertsService', function ($modal, $scope,  $rootScope,productService,alertsService) {
		const LIMIT = 10;
		const NAV_PAGE_COUNT = 10;
		$scope.initializeController = function () { 
			$rootScope.moduleName = 'Inventory';
			$scope.FilterKeys = {};
			$scope.Categories = [
									{id:'PART',value:'Plates'},
									{id:'TIRE',value:'Rods'},
									//{id:'BATT',value:'Batteries'},
									//{id:'OILS',value:'Oils'},
									{id:'OTHR',value:'Others'},
								];
			$scope.Quantities = [
									{id:'ZERO',value:'Zero'},
									{id:'MIN',value:'< Min'},
									{id:'MAX',value:'> Max'},	
									{id:'ADJ',value:'With Adjustments'},	
							];
			initInventory();
			initModal();
			$scope.$on('productAdded',productAdded);
			$rootScope.$on('productUpdated',productUpdated);
			$scope.$watch('Products',function(newvalue){
				$scope.Fillers = [];
				if(newvalue.length<NAV_PAGE_COUNT){
					for(var i=1;i<=NAV_PAGE_COUNT - newvalue.length;i++){
						$scope.Fillers.push('');
					}
					if(newvalue.length==0) $scope.Fillers.splice(0,1);
				}
			});
		};
		$scope.setFilterKey = function(field,value){
			$scope.FilterKeys[field] = value;
		}
		$scope.cancelFilter = function(){
			$scope.FilterKeys = {};
			$scope.Products = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.confirmFilter = function(){
			$scope.Products = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.toggleFilter = function(){ 
			$scope.FilterEnabled = !$scope.FilterEnabled;
		}
		$scope.searchFor =function(keyword){
			$scope.Products = [];
			$scope.Pages = [];
			$scope.SearchKeyword = angular.copy(keyword);
			$scope.movePage(1,$scope.SearchKeyword);
		}
		$scope.resetSearch =function(){
			$scope.Pages = [];
			$scope.SearchEnabled = false;
			$scope.productSearchBox=null;
			$scope.SearchKeyword=null;
			$scope.Products = [];
			$scope.FetchProductId=null;
			$scope.movePage(1);
		}
		$scope.productSearchFilter = function (product){
			var searchBox = $scope.productSearchBox;
			var keyword = new RegExp(searchBox, 'i');
			var test = keyword.test(product.display_title);
			return !searchBox || test ; //Return NO FILTER or filter by patient_name
		}
		$scope.editProduct =  function(productId){
			if($scope.LoadingProducts) return alert('Products are still being loaded. Please wait');
			if($scope.FetchingProduct) return alert('A product is still being loaded. Please wait');
			if($scope.FetchProductId) return alert('A product is still being saved. Please wait');
			$scope.FetchingProduct = true;
			$scope.FetchProductId = productId;
			$scope.NewProduct = null;
			productService.getProduct(successGetProduct,errorGetProduct,$scope,productId);
		}
		$scope.movePage= function(__page,__keyword,__filter){
			$scope.SearchEnabled = __keyword? true:false;
			//$scope.FilterEnabled = __filter? true:false;
			if(!__keyword) $scope.productSearchBox=null;
			if(!__filter) $scope.FilterKeys={};
			if(__page>$scope.LastPage&&!__keyword && $scope.LastPage>0){
				$scope.GoToPage = $scope.LastPage;
				return alert('Oops! You can only go up to page '+$scope.LastPage);
			}
			$scope.CurrentPage = parseInt(__page);
			$scope.CurrentOffset = Math.ceil(__page/NAV_PAGE_COUNT) - 1;
			getProducts(__page,__keyword,__filter,LIMIT);
		}
		$scope.exportData = function(__keyword,__filter){
			productService.exportData({keyword:__keyword,filter:__filter});
		}
		function initInventory(){
			$scope.Pages = [];
			$scope.Products = [];
			$scope.FetchProductId=null;
			$scope.movePage(1);
		}
		function initModal(){
			var ProductModalInstanceController = function ($scope, $modalInstance, product) {
				$scope.applyAdjustment = function(type){
					switch(type){
						case 'quantity':
							$scope.soh_quantity =  $scope.adjustment.adj_quantity;
						break;
						case 'price':
							$scope.srp =  $scope.adjustment.tmp_srp
							$scope.$broadcast('updateMarkup');
						break;
					}
					if(!$scope.adjusted) $scope.adjusted = {};
					$scope.adjusted[type] = true;
				}
				//Confirm handler
				$scope.confirm = function (productId) {
					$rootScope.Progress = 10;
					$rootScope.$broadcast(productId?'updateProduct':'addProduct');
					$modalInstance.close(productId);
				};
				$scope.archive = function (productId) {
					if(confirm('Are you sure you want to archive '+ $scope.display_title+'?')){
						$rootScope.Progress = 10;
						$rootScope.$broadcast('archiveProduct',{success:successDeleteProduct,error:errorDeleteProduct,scope:$scope,id:productId});
						$modalInstance.close(productId);
					}
				};
				$scope.activate = function (productId) {
					if(confirm('Are you sure you want to activate '+ $scope.display_title+'?')){
						$rootScope.Progress = 10;
						$rootScope.$broadcast('activateProduct',{success:successDeleteProduct,error:errorDeleteProduct,scope:$scope,id:productId});
						$modalInstance.close(productId);
					}
				};
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				
				if(product){
					console.log(product);
					for(var $index in product){
						$scope[$index] = product[$index];
					}
				}
				
			};
			//openProductModal function
			$scope.openProductModal = function (product) {
				var modalInstance = $modal.open({
					templateUrl: 'productModalContent.html',
					controller: ProductModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 product: function() {return product;}
					}
				});
				modalInstance.result.then(function (data) {
					console.log(data,'xx');
				}, function (data) {
					$scope.FetchProductId = null;
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
		function getProducts(__page,__keyword,__filter,_limit){
			$scope.LoadingProducts = true;
			productService.getProducts(successGetProducts,errorGetProducts,$rootScope,{page:__page,keyword:__keyword,limit:_limit,filter:__filter});
		}
		function successDeleteProduct($response, $status, $scope){
			initInventory();
		}
		function errorDeleteProduct($response, $status, $scope){
			
		}
		function successGetProducts($response, $status, $rootScope){
			$scope.Products = $response.data;
			$scope.LoadingProducts = false; 
			$scope.GoToPage = null;
			paginate($response.meta.pages);
		}
		function errorGetProducts($response, $status, $scope){ }
		function successGetProduct($response, $status, $scope){
			var data = {};
			$scope.openProductModal($response.data);
			$scope.FetchingProduct =false;
		}
		function errorGetProduct($response, $status, $scope){ }
		function productAdded(evt,args){
			var data = args.response.data;
			$scope.NewProduct = data.id;
			alertsService.RenderSuccessMessage('New product added successfully!');
			$rootScope.Progress = 100;
			initInventory();
		}
		function productUpdated(evt,args){
			alertsService.RenderSuccessMessage('Product updated successfully!');
			$scope.FetchProductId  = null;
			var data = args.response.data;
			data.display_title = data.particular + ' '+data.part_no+' '+data.description;
			$.each($scope.Products,function(index,product){
				if(product.id==data.id){
					$scope.$apply(function(){$scope.Products[index] =  data;});
				}
			})
		}
		function paginate(__pages){
			$scope.EnablePagination = true;
			if($scope.Pages.length==0){
				for(var i=1;i<=__pages;i++){
					$scope.Pages.push(i);
				}
				$scope.LastPage = $scope.Pages.length;
			}
			updatePages();
		}
		function updatePages(){
			$scope.ActivePages=[];
			for(var i=$scope.CurrentOffset*NAV_PAGE_COUNT,ctr=1;i<$scope.Pages.length&&ctr<=NAV_PAGE_COUNT;i++,ctr++){
				$scope.ActivePages.push($scope.Pages[i]);
			}
		}
	}]);
});


