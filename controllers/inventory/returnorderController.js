define(['config','productModalController','productTypeaheadController','transactionModalController','customerService','vendorService','alertsService'], function (app) {

    app.register.controller('ReturnController',['$modal','$scope',  '$rootScope','$timeout','customerService','vendorService','alertsService', function ($modal, $scope,  $rootScope,$timeout, customerService,vendorService,alertsService) {
			var __buffer;
			var __page;
			$scope.initializeController = function(){
				$scope.typeAheadOptions = {
					highlight: true
				 };
				 $scope.typeAheadData = {
					displayKey: 'name',
					source: {},
				  };
				  __buffer = [];
				  $scope.LoadType = 'supplier';
				  $scope.LoadLast = true;
				  $scope.setTransactionType('return');
				  initAssessment();
				  initTypeahead( $scope.LoadType,1);
				  initModal();
			}
			$scope.setTransactionType = function(type){
				$scope.transactionType = type;
			}
			$scope.resetEntityObject = function(){
				$scope.EntityObject = null;
				$scope.EntityObjectTypeahead = null;
			}
			$scope.validateSupplier = function(entity){
				if(entity){
					if(entity.id==undefined){
						alert('Invalid supplier');
						$scope.resetEntityObject();
					}
				}else{
					$scope.resetEntityObject();
				}
		   }
		    $scope.$on('typeahead:selected',function(event,data){
				if(data.hasOwnProperty('last_bill'))
					$scope.EntityObject = data;
		   });
			function productAdded(evt,args){
				var data = args.response.data;
				console.log(data);
			}
		   $scope.$on('addItem',function(event,data){
				data.amount = data.price * data.quantity;
				$scope.AssessAmount += data.amount;
				$scope.AssessItems.push(data);
		   });
		   $scope.resetSales = function(){
			  if(confirm('Are you sure you want to cancel this transaction?')){
				  initAssessment();
			  }
		   }
		   $scope.$on('resetTransaction',function(){
			   initAssessment();
		   });
		   $scope.toggleEdit = function($mode){
			   switch($mode){
					case 'edit': $scope.editMode =  true; break;
					case 'save': 
						$scope.editMode =  false; 
						var totalAmount = 0;
						var items = $scope.AssessItems;
						for(var index in items){
							var item = items[index];
							item.amount = item.price * item.quantity ;
							totalAmount += item.amount;
							items[index] = item;
						}
						$scope.AssessItems = items;
						$scope.AssessAmount = totalAmount;
					break;
			   }
		   }
		   $scope.toggleDelete = function($mode){
			   switch($mode){
					case 'delete': $scope.deleteMode =  true; break;
					case 'save': 
						$scope.deleteMode =  false; 
						var totalAmount = 0;
						var items = $scope.AssessItems;
						var cache =[];
						for(var index in items){
							var item = items[index];
							if(!item.is_delete){
								cache.push(item);
								totalAmount += item.amount;
							}
						}
						$scope.AssessItems = cache;
						$scope.AssessAmount = totalAmount;
					break;
			   }
		   }
		   function initModal(){
			var TransactionModalInstanceController = function ($scope, $modalInstance, transaction) {
				$scope.SetupModal=true;
				$scope.payCash = false;
				$scope.payCharge = true;
				$scope.ActiveTransactionTab = 'payment';
				//Confirm handler
				$scope.confirm = function (transactionId) {
					$rootScope.$broadcast(transactionId?'updateTransaction':'addTransaction');
					$modalInstance.close(transactionId);
				};
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				
				if(transaction){
					for(var $index in transaction){
						$scope[$index] = transaction[$index];
					}
				}
				
			};
			//openTransactionModal function
			$scope.openTransactionModal = function () {
				var transaction = {header:{},details:[]};
				var transac_date = new Date();
					transac_date = transac_date.getFullYear()+'-'+(transac_date.getMonth()+1)+'-'+transac_date.getDate();
				transaction.header.type=$scope.transactionType;
				transaction.header.entity_type=$scope.EntityObject.type;
				transaction.header.entity=transaction.header[$scope.EntityObject.type]=$scope.EntityObject;
				transaction.header.entity_id=$scope.EntityObject.id;
				transaction.header.date=transaction.header.transac_date=transac_date;
				transaction.header.amount=transaction.header.total=$scope.AssessAmount;
				transaction.header.status = 'fulfilled';
				
				$.each($scope.AssessItems,function(i,o){
					var detail = {product_id:o.id,quantity:o.quantity,price:o.price,amount:o.amount};
					transaction.details.push(detail);
				});
				var modalInstance = $modal.open({
					templateUrl: 'createTransactionModalContent.html',
					controller: TransactionModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 transaction: function() {return transaction;}
					}
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					console.log(data);
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
		   function initAssessment(){
				$timeout(function(){
					$scope.EntityObject = null;
					$scope.EntityObjectTypeahead = null;
					$scope.AssessItems = [];
					$scope.AssessAmount = 0;
					}, 100);
		   }
			function initTypeahead(type,page){
				__page = page;
				switch(type){
					case 'customer':
						return customerService.getCustomers(successGetEntities,errorGetEntities,$scope,{page:__page});
					break;
					case 'supplier':
						return vendorService.getVendors(successGetEntities,errorGetEntities,$scope,{page:__page});
					break;
				}
				
			}
			function successGetEntities($response,$status,$scope){
				 angular.forEach($response.data,function(entity,index){
					 var type = angular.copy($scope.LoadType);
					 $response.data[index].type = type;
				 });
				__buffer =  __buffer.concat($response.data);
				$rootScope.__Progress = (__page / $response.meta.pages) *100;
				if($response.meta.next){
					__page =  __page + 1;
					
					return initTypeahead( $scope.LoadType,__page);
				}else{
					$scope.LoadType = $scope.LoadType=='customer'?'supplier':'customer';
					if(!$scope.LoadLast){
						$scope.LoadLast = true;
						return initTypeahead($scope.LoadType,1);					
					}
				}
				var entities = new Bloodhound({
					datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					local: __buffer
				  });

				// initialize the bloodhound suggestion engine
				entities.initialize();
				// Single dataset example
				$scope.typeAheadData = {
					displayKey: 'name',
					source: entities.ttAdapter()
				};
			  $scope.EntityObject = null;
			}
			function errorGetEntities($response,$status,$scope){}
	}]);
});
