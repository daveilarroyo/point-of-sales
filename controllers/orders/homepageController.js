

define(['config','productTypeaheadController','transactionModalController','transactionService','vendorService'], function (app) {

    app.register.controller('OrdersController',['$modal','$scope', '$timeout', '$rootScope','transactionService','vendorService', function ($modal,$scope,$timeout,$rootScope,transactionService,vendorService) {
		var __buffer;
		var __page;
       $scope.initializeController = function () { 
			$rootScope.moduleName = 'Orders';
			$rootScope.AllowAddTemporaryProduct = false;
			initOrders();
			initModal();
			 $scope.typeAheadOptions = {
				highlight: true
			 };
			 $scope.typeAheadData = {
				displayKey: 'name',
				source: {},
			  };
			  __buffer = [];
			__page = 1;
			vendorService.getVendors(successGetVendors,errorGetVendors,$scope,{page:__page});
			
	   }
	    $scope.$on('addItem',function(event,data){
			data.amount = data.capital * data.quantity;
			data.product_id = data.id;
			data.is_delete = false;
			$scope.OrdersAmount += data.amount;
			$scope.OrdersItems.push(data);
	   });
	   $scope.resetVendorObject = function(){
		   $scope.VendorObject=null;
		    $scope.VendorObjectTypeahead=null;
	   }
	   $scope.validateVendor = function(vendor){
		  if(vendor){
				if(vendor.id==undefined){
					alert('Invalid supplier');
					$scope.resetVendorObject();
				}
			}else{
				$scope.resetVendorObject();
			}
	   }
	   $scope.$on('typeahead:selected',function(event,data){
		    if(data.hasOwnProperty('last_bill'))
				$scope.VendorObject = data;
	   });
	    $scope.resetOrders = function(){
		  if(confirm('Are you sure you want to cancel this transaction?')){
			  initOrders();
		  }
	   }
	   $scope.$on('resetTransaction',function(){
		   initOrders();
	   });
	    $scope.toggleEdit = function($mode){
		   switch($mode){
				case 'edit': $scope.editMode =  true; break;
				case 'save': 
					$scope.editMode =  false; 
					var totalAmount = 0;
					var items = $scope.OrdersItems;
					for(var index in items){
						var item = items[index];
						item.amount = item.capital * item.quantity ;
						totalAmount += item.amount;
						items[index] = item;
					}
					$scope.OrdersItems = items;
					$scope.OrdersAmount = totalAmount;
				break;
		   }
	   }
	   $scope.toggleDelete = function($mode){
		   switch($mode){
				case 'delete': $scope.deleteMode =  true; break;
				case 'save': 
					$scope.deleteMode =  false; 
					var totalAmount = 0;
					var items = $scope.OrdersItems;
					var cache =[];
					for(var index in items){
						var item = items[index];
						if(!item.is_delete){
							cache.push(item);
							totalAmount += item.amount;
						}
					}
					$scope.OrdersItems = cache;
					$scope.OrdersAmount = totalAmount;
				break;
		   }
	   }
	   function initOrders(){
		   	$scope.VendorObject = '';
		   	$scope.VendorObjectTypeahead = '';
			$scope.OrdersItems = [];
			$scope.OrdersAmount = 0;
	   }
	   function initModal(){
			var TransactionModalInstanceController = function ($scope, $modalInstance, transaction) {
				$scope.OptionalPayment=true;
				//Confirm handler
				$scope.confirm = function (transactionId) {
					$rootScope.$broadcast(transactionId?'updateTransaction':'addTransaction');
					$modalInstance.close(transactionId);
				};
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				
				if(transaction){
					for(var $index in transaction){
						$scope[$index] = transaction[$index];
					}
				}
				
			};
			//openTransactionModal function
			$scope.openTransactionModal = function () {
				var transaction = {header:{},details:[]};
				var order_date = new Date();
					order_date = order_date.getFullYear()+'-'+(order_date.getMonth()+1)+'-'+order_date.getDate();
				transaction.header.type='orders';
				transaction.header.entity_type='supplier';
				transaction.header.entity=transaction.header.supplier = $scope.VendorObject.name;
				transaction.header.entity_id=$scope.VendorObject.id;
				transaction.header.date= transaction.header.order_date = order_date;
				transaction.header.amount=transaction.header.total = $scope.OrdersAmount;
				transaction.header.status = 'ordered';
				transaction.details = $scope.OrdersItems;
		
				var modalInstance = $modal.open({
					templateUrl: 'createTransactionModalContent.html',
					controller: TransactionModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 transaction: function() {return transaction;}
					}
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					console.log(data);
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
	   function successGetVendors($response,$status,$scope){
		   __buffer =  __buffer.concat($response.data);
				$rootScope.__Progress = (__page / $response.meta.pages) *100;
				if($response.meta.next){
					__page =  __page + 1;
					return vendorService.getVendors(successGetVendors,errorGetVendors,$scope,{page:__page});
				}else{
					$timeout(function(){
					//$rootScope.__Progress = 0;
					},1500);
				}
		   var vendors = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: __buffer
			  });

			  // initialize the bloodhound suggestion engine
			  vendors.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  $scope.addValue = function () {
				vendors.add({
				  name: 'twenty'
				});
			  };

			  // Typeahead options object
			 

			  // Single dataset example
			  $scope.typeAheadData = {
				displayKey: 'name',
				source: vendors.ttAdapter()
			  };
			  
			  $scope.VendorObject = null;
	   }
	   function errorGetVendors($response,$status,$scope){}
	}]);
});


