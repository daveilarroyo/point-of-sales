"use strict";

define(['config','api','productTypeaheadController','productModalController','transactionModalController','transactionService','vendorService','alertsService'], function (app,api) {

    app.register.controller('DeliveriesController',['$modal','$scope','$timeout',  '$rootScope','$location','transactionService','vendorService','alertsService',function ($modal,$scope,$timeout,$rootScope,$location, transactionService,vendorService,alertsService) {
		var __buffer;
		var __page;
       $scope.initializeController = function () { 
			$rootScope.moduleName = 'Deliveries';
			$rootScope.AllowAddTemporaryProduct = true;
			initDeliveries();
			initModal();
			 $scope.typeAheadOptions = {
				highlight: true
			 };
			 $scope.typeAheadData = {
				displayKey: 'name',
				source: {},
			  };
			   __buffer = [];
			__page = 1;
			vendorService.getVendors(successGetVendors,errorGetVendors,$scope,{page:__page});
			$scope.$on('productAdded',productAdded);
			//Check url parameter
			var params = $location.$$search; //URL parameters
			$scope.ReloadModule=false;
			if(params.hasOwnProperty('id')){
				$scope.ReloadModule=true;
				loadTransaction($scope,params.id);
			}
	   }
	    $scope.$on("$destroy", function() {
			$rootScope.AllowAddTemporaryProduct = false;
		});
	    $scope.$on('addItem',function(event,data){
			data.delivered = data.quantity;
			if(!data.ordered) data.ordered = 0;
			data.product_id = data.id;
			data.is_delete = false;
			data.discount = 0;
			data.list_price = data.capital;
			data.amount = data.capital * data.delivered;
			$scope.DeliveriesAmount += data.amount;
			$scope.DeliveriesItems.push(data);
	   });
	    $scope.resetVendorObject = function(){
		   $scope.VendorObject=null;
		   $scope.VendorObjectTypeahead=null;
	   }
	   $scope.validateVendor = function(vendor){
			if(vendor){
				if(vendor.id==undefined){
					alert('Invalid supplier');
					$scope.resetVendorObject();
				}else{
					$scope.fetchOrderedItems(vendor.id);
				}
			}else{
				$scope.resetVendorObject();
			}
	   }
	   
	   $scope.$on('typeahead:selected',function(event,data){
		  if(data.hasOwnProperty('last_bill'))
		   $scope.VendorObject = data;
	   });
	   $scope.fetchOrderedItems = function(vendorId){
	   		var __filter = {};
				__filter.type ='orders'; // source doc type
				__filter.entity_id = vendorId;
				__filter.status = 'ordered';
	   		transactionService.getTransactions(successGetTransaction,errorGetTransaction,$scope,{filter:__filter});
	   }
	   $scope.resetDeliveries = function(){
		  if(confirm('Are you sure you want to cancel this transaction?')){
			  initDeliveries();
		  }
	   }
	     $scope.$on('addTemporaryProduct',function(event,data){
		   $scope.openProductModal();
	   });
	   $scope.$on('resetTransaction',function(){
		   initDeliveries();
		   if($scope.ReloadModule) api.redirect('#'+$location.$$path);
	   });
	    $scope.updateAmount = function(index){
		  var item = $scope.DeliveriesItems[index];
		   item.amount = item.list_price * item.delivered;
		   console.log($scope.DiscountType);
		   switch($scope.DiscountType){
			case 'peso':
				item.amount -= item.discount;
			break;
			case 'percent':
				item.amount -=   item.delivered * (item.list_price *(item.discount/100));
				console.log(item.delivered * (item.list_price *(item.discount/100)));
			break;
		   }
		  $scope.updateCapital(index);
	   }
	   $scope.updateCapital = function(index){
		  var item = $scope.DeliveriesItems[index];
		  if(item.amount>0){
			item.capital = item.amount / item.delivered;
		  }else{
			  item.discount = 0;			  
		  }
	   }
	   $scope.checkCapital = function(index){
			var item = $scope.DeliveriesItems[index];
			var computed_capital  = item.amount / item.delivered;
			if(item.capital<computed_capital){
				if(!confirm('Amount encoded is less than computed capital')){
					item.capital =  computed_capital;
				}
			}
	   }
	   $scope.applyDiscountType = function(type){
			$scope.DiscountType = type;
			for(var index in $scope.DeliveriesItems){		
				$scope.updateAmount(index);
			}
	   }
	   $scope.toggleEdit = function($mode){
		   switch($mode){
				case 'edit': $scope.editMode =  true; break;
				case 'save': 
					$scope.editMode =  false; 
					var totalAmount = 0;
					var items = $scope.DeliveriesItems;
					for(var index in items){
						var item = items[index];
						item.quantity  = item.delivered;
						//item.amount = item.capital * item.delivered ;
						totalAmount += item.amount;
						items[index] = item;
					}
					$scope.DeliveriesItems = items;
					$scope.DeliveriesAmount = Math.round(totalAmount * 100) / 100 ;
				break;
		   }
	   }
	   $scope.toggleDelete = function($mode){
		   switch($mode){
				case 'delete': $scope.deleteMode =  true; break;
				case 'save': 
					$scope.deleteMode =  false; 
					var totalAmount = 0;
					var items = $scope.DeliveriesItems;
					var cache =[];
					for(var index in items){
						var item = items[index];
						if(!item.is_delete){
							cache.push(item);
							totalAmount += item.amount;
						}
					}
					$scope.DeliveriesItems = cache;
					$scope.DeliveriesAmount = totalAmount;
				break;
		   }
	   }
	   function initDeliveries(){
		   	$scope.VendorObject = null;
		   	$scope.VendorObjectTypeahead = null;
			$scope.DeliveriesItems = [];
			$scope.DeliveriesAmount = 0;
			$scope.TransactionDocNo = null;
			$scope.TransactionSource = 'delivery';
			$scope.DiscountType = 'peso';
	   }
	   function initModal(){
			var TransactionModalInstanceController = function ($scope, $modalInstance, transaction) {
				$scope.SetupModal=true;
				$scope.payCash = false;
				$scope.payCharge = true;
				$scope.ActiveTransactionTab = 'payment';
				//Confirm handler
				$scope.confirm = function (transactionId) {
					$rootScope.$broadcast(transactionId?'updateTransaction':'addTransaction');
					$modalInstance.close(transactionId);
				};
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				
				if(transaction){
					for(var $index in transaction){
						$scope[$index] = transaction[$index];
					}
				}
				
			};
			//openTransactionModal function
			$scope.openTransactionModal = function () {
				var transaction = {header:{},details:[]};
				var delivery_date = new Date();
					delivery_date = delivery_date.getFullYear()+'-'+(delivery_date.getMonth()+1)+'-'+delivery_date.getDate();
				transaction.header.type='deliveries';
				transaction.header.doc_no = $scope.TransactionDocNo;
				transaction.header.source = $scope.TransactionSource;
				transaction.header.entity_type='supplier';
				transaction.header.entity=transaction.header.supplier = $scope.VendorObject;
				transaction.header.entity_id=$scope.VendorObject.id;
				transaction.header.date=transaction.header.delivery_date=delivery_date;
				transaction.header.amount=transaction.header.total=$scope.DeliveriesAmount;
				transaction.header.status = 'delivered';
				transaction.details = $scope.DeliveriesItems;
		
				var modalInstance = $modal.open({
					templateUrl: 'createTransactionModalContent.html',
					controller: TransactionModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 transaction: function() {return transaction;}
					}
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					console.log(data);
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
			var PurchaseOrderModalInstanceController = function ($scope, $modalInstance) {
				//Confirm handler
				$scope.confirm = function (transactionId) {
					//Load transaction 
					loadTransaction($scope,transactionId);
					//$rootScope.$broadcast(transactionId?'updateTransaction':'addTransaction');
					$modalInstance.close(transactionId);
				};
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				
				
			};
			//openTransactionModal function
			$scope.openPurchaseOrderModal = function () {
				var modalInstance = $modal.open({
					templateUrl: 'viewTransactionModalContent.html',
					controller: PurchaseOrderModalInstanceController,
					windowClass: 'app-modal-window',
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					console.log(data);
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
			var ProductModalInstanceController = function ($scope, $modalInstance, product) {
				$scope.applyAdjustment = function(){
					$scope.soh_quantity =  $scope.adjustment.adj_quantity;
				}
				//Confirm handler
				$scope.confirm = function (productId) {
					$rootScope.Progress = 10;
					$rootScope.$broadcast(productId?'updateProduct':'addProduct');
					$modalInstance.close(productId);
				};
				//Cancel handler
				$scope.cancel = function () {
				   $modalInstance.dismiss('cancel');
				};
				
				if(product){
					console.log(product);
					for(var $index in product){
						$scope[$index] = product[$index];
					}
				}
				
			};
			//openProductModal function
			$scope.openProductModal = function (product) {
				var modalInstance = $modal.open({
					templateUrl: 'productModalContent.html',
					controller: ProductModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 product: function() {return product;}
					}
				});
				modalInstance.result.then(function (data) {
					console.log(data,'xx');
				}, function (data) {
					$scope.FetchProductId = null;
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
		function loadTransaction($scope,transactionId){
			var data = {};
				data.type ='orders'; // source doc type
				data.ref_no = transactionId;
			transactionService.getTransaction(successGetTransaction,errorGetTransaction,$scope,data);
		}
		function successGetTransaction($response, $status){
			if($response.data.length){
				var src =  $response.data[0];
					$scope.TransactionDocNo = src.ref_no;
					$scope.TransactionSource = src.type=='orders'?'order':'returnslip';
				for(var $i in $response.data){
					var data = $response.data[$i];
					for(var index in data.TransactionDetail){
						var detail  = data.TransactionDetail[index];
						var item =detail.Product;
						item.ordered = detail.quantity;
						item.capital = detail.price;
						$scope.$emit('addItem',item);	
					}
				}
			}	
		}
		function errorGetTransaction($response, $status, $scope){}
		function successGetVendors($response,$status,$scope){
		    __buffer =  __buffer.concat($response.data);
				$rootScope.__Progress = (__page / $response.meta.pages) *100;
				if($response.meta.next){
					__page =  __page + 1;
					return vendorService.getVendors(successGetVendors,errorGetVendors,$scope,{page:__page});
				}else{
					$timeout(function(){
					//$rootScope.__Progress = 0;
					},1500);
				}
		   var vendors = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: __buffer
			  });

			  // initialize the bloodhound suggestion engine
			  vendors.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  $scope.addValue = function () {
				vendors.add({
				  name: 'twenty'
				});
			  };

			  // Typeahead options object
			 

			  // Single dataset example
			  $scope.typeAheadData = {
				displayKey: 'name',
				source: vendors.ttAdapter()
			  };
			  
			  $scope.VendorObject = null;
	   }
	   function errorGetVendors($response,$status,$scope){}
	   function productAdded(evt,args){
			var data = args.response.data;
			var item = {};
			item.id = data.id;
			item.quantity = data.soh_quantity;
			item.price = data.price;
			item.markup = data.markup;
			item.capital = data.capital;
			item.discount = 0;
			item.discountable = data.discountable;
			item.description = data.description;
			item.particular = data.particular;
			item.display_title = data.particular + ' '+data.part_no+' '+data.description;
			item.unit = data.unit;
			item.timestamp = new Date().getTime();
			$scope.$emit('addItem',item);
			$rootScope.Progress = 100;
			alertsService.RenderSuccessMessage('Product updated successfully!')
			$scope.$apply();
		}
    }]);
});


