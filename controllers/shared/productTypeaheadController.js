

define(['config','productService','jquery','typeahead','angular-typeahead'], function (app) {

    app.register.controller('productTypeaheadController',['$scope',  '$rootScope','$http','$timeout','productService', function ($scope,  $rootScope,$http,$timeout, productService) {
		var __buffer;
		var __page;
		$scope.initializeController = function () { 
			$rootScope.ProductLoading = true;
			__buffer = [];
			__page = 1;
			$rootScope.__Progress = 0;
			productService.getProducts(successGetProducts,errorGetProducts,$scope,{page:__page,filter:{status:'active'}});
			$scope.defaultQuantity=1;
			$scope.setFilterKey('description');
		}
		$scope.addTemporaryProduct = function(){
			$rootScope.$broadcast('addTemporaryProduct');
		}
		$scope.getProductByDescription = function(value) {
			var route = 'http://localhost/jnr/common/test.json';
			 return $http.get(route, {
				  params: {
					'description': value,
				  }
				}).then(function(response){
					console.log(response.data.response.data.products);
				  return response.data.response.data.products.map(function(item){
					return item.description;
				  });
				});
		  };
		function successGetProducts($response, $status, $scope){
				__buffer =  __buffer.concat($response.data);
				$rootScope.__Progress = (__page / $response.meta.pages) *100;
				if($response.meta.next){
					__page =  __page + 1;
					return productService.getProducts(successGetProducts,errorGetProducts,$scope,{page:__page,filter:{status:'active'}});
				}else{
					$timeout(function(){
					$rootScope.__Progress = 0;
					},1500);
				}
			 var products = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.display_title); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local:__buffer
			  });

			  // initialize the bloodhound suggestion engine
			  products.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  /* $scope.addValue = function () {
				numbers.add({
				  num: 'twenty'
				});
			  }; */

			  // Typeahead options object
			  $scope.typeAheadOptions = {
				highlight: true
			  };

			  // Single dataset example
			  $scope.typeAheadData = {
				displayKey: 'display_title',
				source: products.ttAdapter()
			  };
			  
			  $scope.productObject = null;
			  $rootScope.ProductLoading = false;
		}
		$scope.$on('typeahead:selected',function(event,data){
			var item = {};
			item.id = data.id;
			item.quantity = parseInt($scope.defaultQuantity);
			item.length = parseInt($scope.defaultLength);
			item.width = parseInt($scope.defaultWidth);
			item.area = item.length*item.width;
			item.price = data.tmp_srp || data.srp;
			item.markup = data.markup;
			item.capital = data.capital;
			item.code_lord = data.code_lord;
			item.discountable = data.discountable;
			item.description = data.description;
			item.part_no = data.part_no;
			item.particular = data.particular;
			item.display_title = data.display_title;
			item.unit = data.unit;
			item.timestamp = new Date().getTime();
			$scope.$parent.$emit('addItem',item);
			$scope.productObject = null;
			$scope.$apply();//Apply changes
			
		});
		$scope.toggleDropDown = function(){ $scope.dropDownToggle = !$scope.dropDownToggle;}
		$scope.setFilterKey = function(value){
			switch(value){
				case 'barcode': $scope.filterIcon = 'barcode'; break;
				case 'description': $scope.filterIcon = 'font'; break;
			}
			$scope.filterKey = value;
			 $scope.dropDownToggle = false;
		}
		$scope.openQuantity = function(){
			$scope.quantityEntry = !$scope.quantityEntry;			
		}
		$scope.openDimension = function(){
			$scope.dimensionEntry = !$scope.dimensionEntry;			
		}
		function errorGetProducts($response, $status, $scope){ }
		
    }]);
});


