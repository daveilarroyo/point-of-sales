

define(['config','transactionService','alertsService'], function (app) {

    app.register.controller('transactionModalController',['$scope',  '$rootScope','$timeout','transactionService','alertsService', function ($scope,  $rootScope, $timeout, transactionService,alertsService) {
		const CARD_INTEREST = 0.05;
       $scope.initializeController = function () { 
			 $scope.BackLogEnable = false;
			if(!$scope.OptionalPayment&&!$scope.SetupModal){
				$scope.payCash = true;
				$scope.ActiveTransactionTab = 'payment';
			}
			$scope.TotalCharges = $scope.GrossCommission = $scope.Commission = $scope.Tax =  $scope.Discount = $scope.Interest =  0;
			$scope.TotalPayments = $scope.CashChange = $scope.CashReceived = $scope.ChequeReceived = $scope.CardReceived = $scope.ChargeReceived = 0;
			$scope.ChequeDetails = $scope.CardDetails = $scope.ChargeDetails = $scope.CashDetails = null;
			if($scope.header) $scope.TotalCharges = $scope.header.amount;
			if($scope.payCash) $scope.CashReceived = $scope.TotalCharges;
			else if($scope.payCharge) $scope.ChargeReceived = $scope.TotalCharges;
			$scope.$on('addTransaction',addTransaction);
			$scope.$on('updateTransaction',updateTransaction);
			$scope.$on('cancelTransaction',cancelTransaction);
			initBackLog();
	   }
	   $scope.$watch('payCheque',function(value){ 	if(!value){ $scope.ChequeReceived = $scope.ChequeDetails = null; $scope.updatePayments();}  });
	   $scope.$watch('payCard',function(value){ 	if(!value){ $scope.CardReceived = $scope.CardDetails = null;  $scope.updateInterest(false); $scope.updatePayments();}else{ $scope.updateInterest(true);}  });
	   $scope.$watch('payCharge',function(value){ 	if(!value){ $scope.ChargeReceived = $scope.ChargeDetails = null; $scope.updatePayments();}  });
	   $scope.$watch('payCash',function(value){ 	if(!value){ $scope.CashReceived   = $scope.CashAmount = $scope.CashChange = null; $scope.CashDetails = null; $scope.updatePayments();} });
	   $scope.enableBackLog = function(flag){
		$scope.BackLogEnable = flag;
	   }
		$scope.$watch('TotalPayments',function(value){ 
			return $scope.PaymentValid = value>=$scope.TotalCharges; 
			if($scope.payCard){
				paymentValid = paymentValid && $scope.CardDetails;
			}
			if($scope.payCheque){
				paymentValid = paymentValid && $scope.ChequeDetails;
			}
		});
		$scope.$watch('TotalCharges',function(value){ 
			return $scope.PaymentValid = $scope.TotalPayments>=value; 
		});
		$scope.updateTax = function(){
			$scope.Tax = roundUp($scope.GrossCommission * 0.12);
		}
		$scope.updateCommission = function(){
			$timeout(function(){
				if($scope.header.source!='delivery') 
					$scope.Commission = $scope.GrossCommission -  $scope.Tax;
			},300);
		}
		$scope.$watch('GrossCommission',function(value){ 
			$scope.updateTax();
		});
		$scope.$watch('Tax',function(value){ 
			$scope.updateCommission();
		});
		$scope.$watch('Commission',function(value){ 
			$scope.updateCharges();
		});
		$scope.checkDiscount = function(){
			var discount = !$scope.Discount? 0:parseFloat($scope.Discount);
			if($scope.header.source!='delivery') {
				var total_allowed_discount = Math.round($scope.header.total_allowed_discount);
				if(discount>total_allowed_discount){
					if(!confirm('Add discount? Allowed discount is up to P'+ total_allowed_discount+ ' only.')){
						discount = $scope.Discount = 0;
					}
				}
			}else{
				$scope.ChargeReceived -= discount;
			}
			$scope.updateCharges();
		}
		$scope.updateInterest = function(applyInterest){
			var interest =  applyInterest?$scope.header.amount * CARD_INTEREST:0;
			$scope.Interest = interest;
			$scope.updateCharges();
		}
		$scope.updateCharges = function(){
			var gross_commission = !$scope.GrossCommission? 0:parseFloat($scope.GrossCommission);
			var tax = !$scope.Tax? 0:parseFloat($scope.Tax);
			var interest = !$scope.Interest? 0:parseFloat($scope.Interest);
			var commission = !$scope.Commission? 0:parseFloat($scope.Commission);
			var discount = !$scope.Discount? 0:parseFloat($scope.Discount);
			if($scope.header == undefined) $scope.header={};
			$scope.header.commission = commission;
			$scope.header.tax = tax;
			$scope.header.interest = interest;
			
			$scope.header.discount = discount;
			
			$scope.TotalCharges =$scope.header.amount;
			//$scope.TotalCharges +=commission;
			$scope.TotalCharges +=tax;
			$scope.TotalCharges +=interest;
			$scope.TotalCharges -=discount;
			
			$scope.updatePayments('cash');
		}
		$scope.updatePayments = function (source){
			var cash = !$scope.CashReceived? 0:parseFloat($scope.CashReceived);
			var cheque = !$scope.ChequeReceived? 0:parseFloat($scope.ChequeReceived);
			var card = !$scope.CardReceived? 0:parseFloat($scope.CardReceived);
			var charge = !$scope.ChargeReceived? 0:parseFloat($scope.ChargeReceived);
			var totalAmount = $scope.TotalCharges;
			var nonCashBalance = totalAmount - cash;
			var nonCashPayment  = cheque+card+charge;
			if(source=='cheque'){
				if(cheque){
					$scope.TransactionAddForm.ChequeDetail.$setDirty();
					$scope.TransactionAddForm.ChequeDetail.$validate();
				}
				if(nonCashPayment>nonCashBalance) {
					alert('Check payment greater than balance');
					cheque = 0;
					$scope.ChequeReceived = cheque;
				}
			}else if(source=='card'){
				if(card){
					$scope.TransactionAddForm.CardDetail.$setDirty();
					$scope.TransactionAddForm.CardDetail.$validate();
				}
				if(nonCashPayment>nonCashBalance) {
					alert('Card payment greater than balance');
					card = 0;
					$scope.CardReceived = card;
				}
			}else if(source=='charge'){
				if(charge){
					$scope.TransactionAddForm.ChargeDetail.$setDirty();
					$scope.TransactionAddForm.ChargeDetail.$validate();
				}
				if(nonCashPayment>nonCashBalance) {
					alert('Charge payment greater than balance');
					charge = 0;
					$scope.ChargeReceived = charge;
				}
			}
			var nonCashPayments = cheque+card+charge;
			var balance = totalAmount - nonCashPayments;
			
			
			if(cash) $scope.CashChange =  cash - balance;
			else $scope.CashChange = 0;
			
			$scope.CashAmount = cash;
			if(cash>balance) $scope.CashAmount  = balance;
			
			$scope.TotalPayments = 0;
			$scope.TotalPayments += cheque;
			$scope.TotalPayments += card;
			$scope.TotalPayments += charge;
			$scope.TotalPayments += cash;
		}
		function initBackLog(){
			var initDate = new Date(angular.copy($scope.header.date));
			$scope.header.display_date =  initDate;
			$scope.backLogDate = initDate;
			$scope.$watch('BackLogEnable',function(flag){
				if(!flag){
					var backlog_date = angular.copy($scope.backLogDate);
					backlog_date = backlog_date.getFullYear()+'-'+(backlog_date.getMonth()+1)+'-'+backlog_date.getDate();
					$scope.header.date = backlog_date;
					var dates  = ['invoice','delivery','order'];
					for(var i in dates){
						var dateKey = dates[i]+'_date';
						if($scope.header.hasOwnProperty(dateKey))
							$scope.header[dateKey] = backlog_date;
					}
				}else{
					$scope.backLogDate = new Date(angular.copy($scope.header.date));
				}
				$scope.header.display_date = new Date(angular.copy($scope.header.date));
			});
			$scope.updateBackLog = function(value){
				$scope.backLogDate  = value;
			}
		}
		function roundUp(i){return(parseInt(i/10, 10)+(i%10==0?0:1))*10;}
		function addTransaction(){
			var data = {};
			data.header = $scope.$parent.header;
			data.header.commission += data.header.tax; 
			data.details = $scope.$parent.details;
			data.payments = [];
			if($scope.payCash){
				data.payments.push({payment_type:"CASH",detail:$scope.CashDetails,amount:$scope.CashAmount});
			}
			if($scope.payCard){
				data.payments.push({payment_type:"CARD",detail:$scope.CardDetails,amount:$scope.CardReceived});
			}
			if($scope.payCheque){
				data.payments.push({payment_type:"CHCK",detail:$scope.ChequeDetails,amount:$scope.ChequeReceived});
			}
			if($scope.payCharge){
				data.payments.push({payment_type:"CHRG",detail:$scope.ChargeDetails,amount:$scope.ChargeReceived});
			}
			transactionService.addTransaction(successAddTransaction,errorAddTransaction,$scope.$parent,data);
		}
		function updateTransaction(){
			transactionService.updateTransaction(successUpdateTransaction,errorUpdateTransaction,$scope);
		}
		function cancelTransaction(){
			var data = {};
			data.header = $scope.$parent.header;
			data.payments = $scope.$parent.payments;
			data.details = $scope.$parent.details;
			transactionService.cancelTransaction(successCancelTransaction,errorCancelTransaction,$scope,data);
		}
		function successAddTransaction($response, $status, $scope){
			$rootScope.$broadcast('resetTransaction');
			alertsService.RenderSuccessMessage('Transaction saved successfully!');
		}
		function errorAddTransaction($response, $status, $scope){ 
			alertsService.RenderErrorMessage('Error saving transaction.');
		}
		function successUpdateTransaction($response, $status, $scope){
			$scope.$broadcast('getTransactions');
		}
		function errorUpdateTransaction($response, $status, $scope){ }
		function successCancelTransaction($response, $status, $scope){
			$rootScope.$broadcast('transactionCancelled',{response:$response});
		}
		function errorCancelTransaction($response, $status, $scope){ 
			alertsService.RenderErrorMessage('Error cancelling transaction.');
		}
    }]);
});


