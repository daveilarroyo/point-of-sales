
define(['config', 'cashflowService'], function(app) {
    
    app.register.controller('cashflowModalController', ['$scope', '$rootScope', '$timeout', 'cashflowService', function($scope, $rootScope, $timeout, cashflowService) {
        $scope.initializeController = function() {
            $scope.$on('addCashflow', saveCashflow);
            $scope.$on('updateCashflow', saveCashflow);
            $scope.flag = '-';
        }
        $scope.setFlag = function(flag) {
            $scope.flag = flag;
        }
        function saveCashflow() {
            var cashflow = {};
            cashflow.ref_no = $scope.ref_no;
            var transaction_date = $scope.transaction_date;
            var transaction_time = $scope.transaction_time;
            transaction_date = transaction_date.getFullYear() + '-' + (transaction_date.getMonth() + 1) + '-' + transaction_date.getDate();
            transaction_time = transaction_time.getHours() + ':' + transaction_time.getMinutes() + ':00';
            cashflow.timestamp = transaction_date + ' ' + transaction_time;
            cashflow.particulars = $scope.particulars;
            cashflow.amount = $scope.amount;
            cashflow.flag = $scope.flag=='-'?'d':'c';
            cashflowService.saveCashflow(successSaveCashflow, errorSaveCashflow, $scope, cashflow);
        }
        function successSaveCashflow($response, $status, $scope) {
            $rootScope.$broadcast('cashflowSaved', {
                response: $response
            });
        }
        function errorSaveCashflow($response, $status, $scope) {}
    }
    ]);
});
