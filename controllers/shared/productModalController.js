

define(['config','productService'], function (app) {

    app.register.controller('productModalController',['$scope',  '$rootScope','productService', function ($scope,  $rootScope, productService) {
       $scope.initializeController = function () { 
			$scope.ActiveProductTab = $scope.ActiveProductTab||'specification';
			$scope.Categories = [{id:'PART',name:'Parts'},{id:'OILS',name:'Oils'},{id:'BATT',name:'Batteries'},{id:'TIRE',name:'Tires'},{id:'OTHR',name:'Others'}]
			$scope.Categories = [
									{id:'PART',name:'Plates'},
									{id:'TIRE',name:'Rods'},
									{id:'OTHR',name:'Others'}
								];
			$scope.$on('addProduct',addProduct);
			$scope.$on('updateProduct',updateProduct);
			$scope.$on('updateProductQuantity',updateProductQuantity);
			$scope.$on('archiveProduct',archiveProduct);
			$scope.$on('activateProduct',activateProduct);
			$scope.$on('updateMarkup',updateMarkup);
			$scope.$watch('capital',updateSRP);
			$scope.updateSrp = updateSRP;
			$scope.updateMarkup = updateMarkup;
			$scope.copyToSOH = function(){
				$scope.soh_quantity = $scope.length*$scope.width;
				$scope.ActiveProductTab = 'quantity';
			}
	   }
	  
		function updateSRP(){
			if($scope.adjustment){
				if($scope.adjustment.tmp_srp) return;
			}
			$scope.srp = $scope.capital + $scope.markup;
		}
		function updateMarkup(){
			$scope.markup = $scope.srp - $scope.capital;
		}

		
		function addProduct(){
			productService.addProduct(successAddProduct,errorAddProduct,$scope);
		}
		function updateProduct(){
			productService.updateProduct(successUpdateProduct,errorUpdateProduct,$scope);
		}
		function updateProductQuantity(){
			productService.updateProductQuantity(successUpdateProductQuantity,errorUpdateProductQuantity,$scope);
		}
		function archiveProduct(evts,args){
			productService.archiveProduct(args.success,args.error,args.scope,args.id);
		}
		function activateProduct(evts,args){
			productService.activateProduct(args.success,args.error,args.scope,args.id);
		}
		function successAddProduct($response, $status, $scope){
			$rootScope.$broadcast('productAdded',{response:$response});
		}
		function errorAddProduct($response, $status, $scope){ }
		function successUpdateProduct($response, $status, $scope){
			$rootScope.$broadcast('productUpdated',{response:$response});
		}
		function errorUpdateProduct($response, $status, $scope){ }
		function successUpdateProductQuantity($response, $status, $scope){
			$rootScope.$broadcast('productQuantityUpdated',{response:$response});
		}
		function errorUpdateProductQuantity($response, $status, $scope){ }
    }]);
});


