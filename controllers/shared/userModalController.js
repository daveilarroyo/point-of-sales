
define(['config','userService'], function (app) {

    app.register.controller('userModalController',['$scope',  '$rootScope','$timeout', 'userService', function ($scope,  $rootScope,$timeout, userService) {
		var _module_buffer , _module_page, __vendor_buffer , __vendor_page ;
		 $scope.initializeController = function () { 
			if(!$scope.type) $scope.type = 'staff';
			$scope.$on('addUser',saveUser);
			$scope.$on('updateUser',saveUser);
			$scope.$on('openUser',openUser);
			$scope.$on('closeUser',closeUser);
			initTypeAhead();
		 }
		 $scope.setUserType = function(type){
			 $scope.type = type;
		 }
		 $scope.addAccess = function(module){
			$scope.access.push(module);
			$scope.AccessObject = null;
			$scope.AccessObjectTypeAhead = null;
		}
		
		$scope.removeAccess = function(index){
			$scope.access.splice(index,1);
		}
		 function saveUser(){
			var modules = [];
			 var user = {};
				 user.id = $scope.id;	
				 user.username = $scope.username;	
				 user.password = $scope.password;	
				 user.last_name = $scope.last_name;	
				 user.first_name = $scope.first_name;	
				 user.type = $scope.type;
				 angular.forEach($scope.access,function(module,index){
					modules.push(module.id);
				 });
				 user.access = modules;
				 console.log(user);
				 userService.saveUser(successSaveUser,errorSaveUser,$scope,user);

		 }
		 function openUser(){
			 var user = {};
				 user.id = $scope.id;
				 user.status = 'open';
				 userService.saveUser(successSaveUser,errorSaveUser,$scope,user);
		 }
		 function closeUser(){
			 var user = {};
				 user.id = $scope.id;
				 user.status = 'close';
				 userService.saveUser(successSaveUser,errorSaveUser,$scope,user);
		 }
		 function initTypeAhead(){
			 _module_buffer = [];
			_module_page = 1;
			
			  // Typeahead options object
			  $scope.typeAheadOptions = {
				highlight: true
			  };
			$scope.typeAheadAccessData = {
				displayKey: 'title',
				source: {},
			  };
			userService.getModules(successGetModules,errorGetModules,$scope,{page:_module_page});
			$scope.$on('typeahead:selected',function(event,data){
              $scope.AccessObject = data;
                    
           });
		 }
		 function successGetModules($response,$status,$scope){
			 _module_buffer =  _module_buffer.concat($response.data);
				$rootScope.__Progress = (_module_page / $response.meta.pages) *100;
				if($response.meta.next){
					_module_page =  _module_page + 1;
					return userService.getModules(successGetModules,errorGetModules,$scope,{page:_module_page});
				}else{
					$timeout(function(){
					//$rootScope.__Progress = 0;
					},1500);
				}
		   var users = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.title); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: _module_buffer,
			  });

			  // initialize the bloodhound suggestion engine
			  users.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  $scope.addValue = function () {
				users.add({
				  title: 'twenty'
				});
			  };

			  // Typeahead options object
			  $scope.typeAheadOptions = {
				highlight: true
			  };

			  // Single dataset example
			  $scope.typeAheadAccessData = {
				displayKey: 'title',
				source: users.ttAdapter()
			  };
			  
			  $scope.AccessObject = null;
			  $scope.AccessObjectTypeAhead = null;
	   }
	   function errorGetModules($response,$status,$scope){}
	   function successSaveUser($response,$status,$scope){
		   $rootScope.$broadcast('userSaved',{response:$response});
	   }
	   function errorSaveUser($response,$status,$scope){}
	 }]);
});