

define(['config','productService'], function (app) {

    app.register.controller('SearchController',['$scope',  '$rootScope','productService', function ($scope,  $rootScope,productService) {
		const LIMIT = 6;
		const NAV_PAGE_COUNT = 10;
		$scope.initializeController = function () { 
			$rootScope.moduleName = 'Inventory';
			$scope.FilterKeys = {};
			$scope.Categories = [
									{id:'PART',value:'Parts'},
									{id:'TIRE',value:'Tires'},
									{id:'BATT',value:'Batteries'},
									{id:'OILS',value:'Oils'},
									{id:'OTHR',value:'Others'},
								];
			$scope.Quantities = [
									{id:'ZERO',value:'Zero'},
									{id:'MIN',value:'< Min'},
									{id:'MAX',value:'> Max'},	
									{id:'ADJ',value:'With Adjustments'},	
							];
			$scope.$watch('Products',function(newvalue){
				$scope.Fillers = [];
				if(newvalue.length<NAV_PAGE_COUNT){
					for(var i=1;i<=NAV_PAGE_COUNT - newvalue.length;i++){
						$scope.Fillers.push('');
					}
					if(newvalue.length==0) $scope.Fillers.splice(0,1);
				}
			});
			$scope.$on('initSearch',initSearch);
			$scope.$on('clearSearch',clearSearch);
		};
		$scope.setFilterKey = function(field,value){
			$scope.FilterKeys[field] = value;
		}
		$scope.cancelFilter = function(){
			$scope.FilterKeys = {};
			$scope.Products = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.confirmFilter = function(){
			$scope.Products = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.toggleFilter = function(){ 
			$scope.FilterEnabled = !$scope.FilterEnabled;
		}
		$scope.searchFor =function(keyword){
			$scope.Products = [];
			$scope.Pages = [];
			$scope.SearchKeyword = angular.copy(keyword);
			$scope.movePage(1,$scope.SearchKeyword);
		}
		$scope.resetSearch =function(){
			$scope.Pages = [];
			$scope.SearchEnabled = false;
			$scope.productSearchBox=null;
			$scope.SearchKeyword=null;
			$scope.Products = [];
			$scope.FetchProductId=null;
			$scope.movePage(1);
		}
		$scope.productSearchFilter = function (product){
			var searchBox = $scope.productSearchBox;
			var keyword = new RegExp(searchBox, 'i');
			var test = keyword.test(product.display_title);
			return !searchBox || test ; //Return NO FILTER or filter by patient_name
		}
		$scope.movePage= function(__page,__keyword,__filter){
			$scope.SearchEnabled = __keyword? true:false;
			//$scope.FilterEnabled = __filter? true:false;
			if(!__keyword) $scope.productSearchBox=null;
			if(!__filter) $scope.FilterKeys={};
			if(__page>$scope.LastPage&&!__keyword && $scope.LastPage>0){
				$scope.GoToPage = $scope.LastPage;
				return alert('Oops! You can only go up to page '+$scope.LastPage);
			}
			$scope.CurrentPage = parseInt(__page);
			$scope.CurrentOffset = Math.ceil(__page/NAV_PAGE_COUNT) - 1;
			getProducts(__page,__keyword,__filter,LIMIT);
		}
		$scope.exportData = function(__keyword,__filter){
			productService.exportData({keyword:__keyword,filter:__filter});
		}
		$scope.loadMore = function(){
			$scope.AppendResults = true;
			$scope.movePage($scope.CurrentPage+1,$scope.SearchKeyword);
		}
		function initSearch(){
			$scope.Pages = [];
			$scope.Products = [];
			$scope.FetchProductId=null;
			$scope.movePage(1);
			$scope.MoreProducts = false;
			$scope.AppendResults = false;
		}
		function getProducts(__page,__keyword,__filter,_limit){
			$scope.LoadingProducts = true;
			productService.getProducts(successGetProducts,errorGetProducts,$rootScope,{page:__page,keyword:__keyword,limit:_limit,filter:__filter});
		}
		function successGetProducts($response, $status, $rootScope){
			if(!$scope.AppendResults)
				$scope.Products = $response.data;
			else
				$scope.Products = $scope.Products.concat($response.data);
			$scope.LoadingProducts = false; 
			$scope.GoToPage = null;
			$scope.MoreProducts = $response.meta.next!=null;
			//paginate($response.meta.pages);
		}
		function errorGetProducts($response, $status, $scope){ }
		function paginate(__pages){
			$scope.EnablePagination = true;
			if($scope.Pages.length==0){
				for(var i=1;i<=__pages;i++){
					$scope.Pages.push(i);
				}
				$scope.LastPage = $scope.Pages.length;
			}
			updatePages();
		}
		function updatePages(){
			$scope.ActivePages=[];
			for(var i=$scope.CurrentOffset*NAV_PAGE_COUNT,ctr=1;i<$scope.Pages.length&&ctr<=NAV_PAGE_COUNT;i++,ctr++){
				$scope.ActivePages.push($scope.Pages[i]);
			}
		}
		function clearSearch(){
			$scope.Pages = [];
			$scope.Products = [];
			$scope.FetchProductId=null;
			$scope.MoreProducts = false;
			$scope.AppendResults = false;
		}
	}]);
});


