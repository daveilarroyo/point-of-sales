
define(['config','customerService','vendorService'], function (app) {

    app.register.controller('ledgerModalController',['$scope',  '$rootScope','$timeout', 'customerService', 'vendorService', function ($scope,  $rootScope,$timeout, customerService,vendorService) {
		var __customer_buffer , __customer_page, __vendor_buffer , __vendor_page ;
		 $scope.initializeController = function () { 
			if(!$scope.transacteeType) $scope.transacteeType = 'customer';
			$scope.$on('addLedger',saveLedger);
			$scope.$on('updateLedger',saveLedger);
			$scope.flag = '-';
			initTypeAhead();
		 }
		 $scope.setTransacteeType = function(type){
			 $scope.transacteeType = type;
			 $scope.resetTransactee();
		 }
		 $scope.setFlag = function(flag){
			 $scope.flag=flag;
		 }
		 $scope.resetTransactee =  function(){
			$scope.CustomerObject = null;
			$scope.VendorObject = null;
			$scope.CustomerObjectTypeAhead = null;
			$scope.VendorObjectTypeAhead = null;
		 }
		  $scope.$on('typeahead:selected',function(event,data){
			   if(data.hasOwnProperty('last_bill')){
				   switch($scope.transacteeType){
					   case 'customer':
							$scope.CustomerObject = data;
					   break;
					   case 'supplier':
							$scope.VendorObject = data;
					   break;
				   }
			   }
					
		   });
		 function saveLedger(){
			 var ledger = {};
				 ledger.ref_no = $scope.ref_no;
				 var transaction_date = $scope.transaction_date;
				 var transaction_time = $scope.transaction_time;
					transaction_date = transaction_date.getFullYear()+'-'+(transaction_date.getMonth()+1)+'-'+transaction_date.getDate();
					transaction_time = transaction_time.getHours()+':'+transaction_time.getMinutes()+':00';
				 ledger.timestamp = transaction_date+' '+transaction_time ;
				 ledger.particulars = $scope.particulars;
				 ledger.amount = $scope.amount;
				 ledger.flag = $scope.flag;
			 switch($scope.transacteeType){
				 case 'customer':
					ledger.id=$scope.CustomerObject.id;
					customerService.saveLedger(successSaveLedger,errorSaveLedger,$scope,ledger);
				 break;
				 case 'supplier':
					ledger.id=$scope.VendorObject.id;
					vendorService.saveLedger(successSaveLedger,errorSaveLedger,$scope,ledger);
				 break;
			 }
		 }
		 function initTypeAhead(){
			 __customer_buffer = [];
			__customer_page = 1;
			__vendor_buffer = [];
			__vendor_page = 1;
			  // Typeahead options object
			  $scope.typeAheadOptions = {
				highlight: true
			  };
			$scope.typeAheadCustomerData = {
				displayKey: 'name',
				source: {},
			  };
			  $scope.typeAheadVendorData = {
				displayKey: 'name',
				source: {},
			  };
			customerService.getCustomers(successGetCustomers,errorGetCustomers,$scope,{page:__customer_page});
			vendorService.getVendors(successGetVendors,errorGetVendors,$scope,{page:__vendor_page});
		 }
		 function successGetCustomers($response,$status,$scope){
			 __customer_buffer =  __customer_buffer.concat($response.data);
				$rootScope.__Progress = (__customer_page / $response.meta.pages) *100;
				if($response.meta.next){
					__customer_page =  __customer_page + 1;
					return customerService.getCustomers(successGetCustomers,errorGetCustomers,$scope,{page:__customer_page});
				}else{
					$timeout(function(){
					//$rootScope.__Progress = 0;
					},1500);
				}
		   var customers = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: __customer_buffer,
			  });

			  // initialize the bloodhound suggestion engine
			  customers.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  $scope.addValue = function () {
				customers.add({
				  name: 'twenty'
				});
			  };

			  // Typeahead options object
			  $scope.typeAheadOptions = {
				highlight: true
			  };

			  // Single dataset example
			  $scope.typeAheadCustomerData = {
				displayKey: 'name',
				source: customers.ttAdapter()
			  };
			  
			  $scope.CustomerObject = null;
	   }
	   function errorGetCustomers($response,$status,$scope){}
	   function successGetVendors($response,$status,$scope){
		    __vendor_buffer =  __vendor_buffer.concat($response.data);
				$rootScope.__Progress = (__vendor_page / $response.meta.pages) *100;
				if($response.meta.next){
					__vendor_page =  __vendor_page + 1;
					return vendorService.getVendors(successGetVendors,errorGetVendors,$scope,{page:__vendor_page});
				}else{
					$timeout(function(){
					//$rootScope.__Progress = 0;
					},1500);
				}
		   var vendors = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: __vendor_buffer
			  });

			  // initialize the bloodhound suggestion engine
			  vendors.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  $scope.addValue = function () {
				vendors.add({
				  name: 'twenty'
				});
			  };

			  // Typeahead options object
			 

			  // Single dataset example
			  $scope.typeAheadVendorData = {
				displayKey: 'name',
				source: vendors.ttAdapter()
			  };
			  
			  $scope.VendorObject = null;
	   }
	   function errorGetVendors($response,$status,$scope){}
	   function successSaveLedger($response,$status,$scope){
		   $rootScope.$broadcast('ledgerSaved',{response:$response});
	   }
	   function errorSaveLedger($response,$status,$scope){}
	 }]);
});