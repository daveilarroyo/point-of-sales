
define(['config','customerService','vendorService','transactionService'], function (app) {

    app.register.controller('accountModalController',['$scope',  '$rootScope','$timeout', 'customerService', 'vendorService', 'transactionService', function ($scope,  $rootScope,$timeout, customerService,vendorService,transactionService) {
		var __customer_buffer , __customer_page, __vendor_buffer , __vendor_page ;
		 $scope.initializeController = function () { 
			if(!$scope.accountType) $scope.accountType = 'customer';
			$scope.$on('addAccount',saveAccount);
			$scope.$on('updateAccount',saveAccount);
			$scope.$on('openAccount',openAccount);
			$scope.$on('closeAccount',closeAccount);
			$scope.$on('postAccount',postAccount);
			$scope.$watch('begin_balance',function(value){
				$scope.current_balance = value + $scope.charges - $scope.payments;
			});
			$scope.flag = '-';
			initTypeAhead();
		 }
		 $scope.setAccountType = function(type){
			 $scope.accountType = type;
		 }
		 $scope.setFlag = function(flag){
			 $scope.flag=flag;
		 }
		 $scope.exportData = function(__keyword,__format){
			 __keyword = encodeURIComponent(__keyword);
			 var __types ='sales,return';
			 if($scope.accountType=='supplier')  __types ='deliveries,return';
			transactionService.exportData({keyword:__keyword,format:__format,sort:'oldest',soa:true,last_bill:$scope.last_bill,filter:{coverage:'SOA',type:__types}});
		}
		 function saveAccount(){
			 var account = {};
				 account.id = $scope.id;
				 account.name = $scope.name;
				 account.begin_balance = $scope.begin_balance;
				 account.current_balance = $scope.current_balance;
				 account.last_bill = $scope.last_bill;
			 switch($scope.accountType){
				 case 'customer':
					customerService.saveCustomer(successSaveAccount,errorSaveAccount,$scope,account);
				 break;
				 case 'supplier':
					vendorService.saveVendor(successSaveAccount,errorSaveAccount,$scope,account);
				 break;
			 }
		 }
		 function openAccount(){
			 var account = {};
				 account.id = $scope.id;
				 account.status = 'open';
			 switch($scope.accountType){
				 case 'customer':
					customerService.saveCustomer(successSaveAccount,errorSaveAccount,$scope,account);
				 break;
				 case 'supplier':
					vendorService.saveVendor(successSaveAccount,errorSaveAccount,$scope,account);
				 break;
			 }
		 }
		 function closeAccount(){
			 var account = {};
				 account.id = $scope.id;
				 account.status = 'close';
			 switch($scope.accountType){
				 case 'customer':
					customerService.saveCustomer(successSaveAccount,errorSaveAccount,$scope,account);
				 break;
				 case 'supplier':
					vendorService.saveVendor(successSaveAccount,errorSaveAccount,$scope,account);
				 break;
			 }
		 }
		 function postAccount(){
			 var account = {};
				 account.id = $scope.id;
				 account.begin_balance = $scope.begin_balance;
				 account.current_balance = $scope.current_balance;
				 account.last_bill = $scope.last_bill;
				 account.action = 'post';
			 switch($scope.accountType){
				 case 'customer':
					customerService.saveCustomer(successSaveAccount,errorSaveAccount,$scope,account);
				 break;
				 case 'supplier':
					vendorService.saveVendor(successSaveAccount,errorSaveAccount,$scope,account);
				 break;
			 }
		 }
		 function initTypeAhead(){
			 __customer_buffer = [];
			__customer_page = 1;
			__vendor_buffer = [];
			__vendor_page = 1;
			  // Typeahead options object
			  $scope.typeAheadOptions = {
				highlight: true
			  };
			$scope.typeAheadCustomerData = {
				displayKey: 'name',
				source: {},
			  };
			  $scope.typeAheadVendorData = {
				displayKey: 'name',
				source: {},
			  };
			customerService.getCustomers(successGetCustomers,errorGetCustomers,$scope,{page:__customer_page});
			vendorService.getVendors(successGetVendors,errorGetVendors,$scope,{page:__vendor_page});
		 }
		 function successGetCustomers($response,$status,$scope){
			 __customer_buffer =  __customer_buffer.concat($response.data);
				$rootScope.__Progress = (__customer_page / $response.meta.pages) *100;
				if($response.meta.next){
					__customer_page =  __customer_page + 1;
					return customerService.getCustomers(successGetCustomers,errorGetCustomers,$scope,{page:__customer_page});
				}else{
					$timeout(function(){
					//$rootScope.__Progress = 0;
					},1500);
				}
		   var customers = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: __customer_buffer,
			  });

			  // initialize the bloodhound suggestion engine
			  customers.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  $scope.addValue = function () {
				customers.add({
				  name: 'twenty'
				});
			  };

			  // Typeahead options object
			  $scope.typeAheadOptions = {
				highlight: true
			  };

			  // Single dataset example
			  $scope.typeAheadCustomerData = {
				displayKey: 'name',
				source: customers.ttAdapter()
			  };
			  
			  $scope.CustomerObject = null;
	   }
	   function errorGetCustomers($response,$status,$scope){}
	   function successGetVendors($response,$status,$scope){
		    __vendor_buffer =  __vendor_buffer.concat($response.data);
				$rootScope.__Progress = (__vendor_page / $response.meta.pages) *100;
				if($response.meta.next){
					__vendor_page =  __vendor_page + 1;
					return vendorService.getVendors(successGetVendors,errorGetVendors,$scope,{page:__vendor_page});
				}else{
					$timeout(function(){
					//$rootScope.__Progress = 0;
					},1500);
				}
		   var vendors = new Bloodhound({
				datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: __vendor_buffer
			  });

			  // initialize the bloodhound suggestion engine
			  vendors.initialize();

			  // Allows the addition of local datum
			  // values to a pre-existing bloodhound engine.
			  $scope.addValue = function () {
				vendors.add({
				  name: 'twenty'
				});
			  };

			  // Typeahead options object
			 

			  // Single dataset example
			  $scope.typeAheadVendorData = {
				displayKey: 'name',
				source: vendors.ttAdapter()
			  };
			  
			  $scope.VendorObject = null;
	   }
	   function errorGetVendors($response,$status,$scope){}
	   function successSaveAccount($response,$status,$scope){
		   $rootScope.$broadcast('accountSaved',{response:$response});
	   }
	   function errorSaveAccount($response,$status,$scope){}
	 }]);
});