

define(['config','transactionService','transactionModalController','alertsService'], function (app) {

    app.register.controller('TransactionsController',['$modal','$scope',  '$rootScope','$timeout','transactionService', 'alertsService',function ($modal,$scope,$rootScope,$timeout,transactionService,alertsService) {
		const LIMIT = 10;
		const NAV_PAGE_COUNT = 10;
       $scope.initializeController = function () { 
			$scope.TransactionTypes = [
								{id:'sales',value:'Sales'},
								{id:'tradein',value:'Trade In'},
								{id:'return',value:'Returns'},
								{id:'deliveries',value:'Deliveries'},
								{id:'orders',value:'Orders'},
							];
			$scope.Coverages = [
								{id:'today',value:'Today'},
								{id:'7D',value:'Last 7 days'},
								{id:'30D',value:'Last 30 days'},
								{id:'custom',value:'Custom'},
							];
			initTransaction();
			initModal();
			$rootScope.$on('transactionCancelled',transactionCancelled);
			$scope.$watch('Transactions',function(newvalue){
				$scope.Fillers = [];
				if(newvalue.length<NAV_PAGE_COUNT){
					for(var i=1;i<=NAV_PAGE_COUNT - newvalue.length;i++){
						$scope.Fillers.push('');
					}
					if(newvalue.length==0) $scope.Fillers.splice(0,1);
				}
			});
	   }
	   $scope.setFilterKey = function(field,value){
			$scope.FilterKeys[field] = value;
		}
		$scope.cancelFilter = function(){
			$scope.FilterKeys = {};
			$scope.Transactions = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.confirmFilter = function(){
			$scope.Transactions = [];
			$scope.Pages = [];
			$scope.movePage(1,null,$scope.FilterKeys);
			$scope.FilterEnabled = false;
			//$scope.toggleFilter();
		}
		$scope.toggleFilter = function(){ 
			$scope.FilterEnabled = !$scope.FilterEnabled;
		}
		$scope.searchFor =function(keyword){
			$scope.Transactions = [];
			$scope.Pages = [];
			$scope.SearchKeyword = angular.copy(keyword);
			$scope.movePage(1,$scope.SearchKeyword);
		}
		$scope.resetSearch =function(){
			$scope.Pages = [];
			$scope.SearchEnabled = false;
			$scope.transactionSearchBox=null;
			$scope.SearchKeyword=null;
			$scope.Transactions = [];
			$scope.FetchProductId=null;
			$scope.movePage(1);
		}
		$scope.transactionSearchFilter = function (transaction){
			var searchBox = $scope.transactionSearchBox;
			var keyword = new RegExp(searchBox, 'i');
			if(transaction.entity==undefined) transaction.entity = {name:null};
			var test = keyword.test(transaction.entity.name);
			return !searchBox || test ; //Return NO FILTER or filter by patient_name
		}
	   $scope.movePage= function(__page,__keyword,__filter){
			$scope.SearchEnabled = __keyword? true:false;
			if(!__keyword) $scope.productSearchBox=null;
			if(__filter)
				__filter = customFilter(__filter);
			if(!__filter) $scope.FilterKeys={};
			if(__page>$scope.LastPage&&!__keyword && $scope.LastPage>0){
				$scope.GoToPage = $scope.LastPage;
				return alert('Oops! You can only go up to page '+$scope.LastPage);
			}
			$scope.CurrentPage = parseInt(__page);
			$scope.CurrentOffset = Math.ceil(__page/NAV_PAGE_COUNT) - 1;
			getTransactions(__page,__keyword,__filter,LIMIT);
		}
		$scope.exportData = function(__keyword,__filter){
			__filter = customFilter(__filter);
			transactionService.exportData({keyword:__keyword,filter:__filter});
		}
		function initTransaction(){
			$scope.Pages = [];
			$scope.Transactions = [];
			$scope.FetchTransactionId=null;
			$scope.movePage(1);
		}
		 function initModal(){
			var TransactionModalInstanceController = function ($scope, $modalInstance, transaction) {
				$scope.OptionalPayment=true;
				$scope.ReadOnly = true;
				//Confirm handler
				$scope.cancel = function (transactionId) {
					$rootScope.$broadcast('cancelTransaction');
					$modalInstance.close(transactionId);
				};
				//Close handler
				$scope.close = function () {
				   $modalInstance.dismiss('cancel');
				};
				
				if(transaction){
					for(var $index in transaction){
						$scope[$index] = transaction[$index];
					}
				}
				
			};
			//openTransactionModal function
			$scope.openTransactionModal = function (__transaction) {
				$scope.FetchTransactionId  = __transaction.id;
				var transaction = {header:{},payments:[],details:[]};
				var today = new Date();
				var transac_date = __transaction.hasOwnProperty('timestamp')?__transaction.timestamp:today;
					today = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
					transac_date = transac_date.getFullYear()+'-'+(transac_date.getMonth()+1)+'-'+transac_date.getDate();
				
				var entity = __transaction.entity_type=='customer'?__transaction.Customer:__transaction.Supplier;
				transaction.header.id=__transaction.id;
				transaction.header.type=__transaction.type;
				transaction.header.ref_no=__transaction.ref_no;
				transaction.header.entity_type=__transaction.entity_type;
				transaction.header.entity=transaction.header[__transaction.entity_type] = entity;
				transaction.header.entity_id=__transaction.entity_id;
				transaction.header.date= transaction.header.transac_date = transac_date;
				transaction.header.amount=transaction.header.total = __transaction.amount;
				transaction.header.status =  __transaction.status;
				transaction.payments =  __transaction.TransactionPayment;
				transaction.details =  __transaction.TransactionDetail;
				transaction.cancellable = today == transac_date;
				var modalInstance = $modal.open({
					templateUrl: 'createTransactionModalContent.html',
					controller: TransactionModalInstanceController,
					windowClass: 'app-modal-window',
					resolve:{
						 transaction: function() {return transaction;}
					}
				});
				modalInstance.result.then(function (data) {
					//console.log(data,'xx');
				}, function (data) {
					$scope.FetchTransactionId = null;
					//$log.info('Modal dismissed at: ' + new Date());
				});
			};
		}
	   function getTransactions(__page,__keyword,__filter,__limit){
		   $scope.LoadingTransactions = true;
			transactionService.getTransactions(successGetTransactions,errorGetTransactions,$rootScope,{page:__page,keyword:__keyword,limit:__limit,filter:__filter});
		}
		function transactionCancelled(evt,args){
			alertsService.RenderSuccessMessage('Transaction cancelled successfully!');
			var data = args.response.data;
			$scope.FetchTransactionId  = null;
			$scope.NewTransaction  = data.header.id;
			$.each($scope.Transactions,function(index,transaction){
				if(transaction.id==data.header.id){
					$scope.$apply(function(){
						$scope.Transactions[index]['status'] =  data.header.status;
						});
				}
			});
			$timeout(function(){$scope.NewTransaction=null;},1500);
		}
		function successGetTransactions($response, $status, $rootScope){
			$scope.Transactions = [];
			for(var index in $response.data){
				var transaction =  $response.data[index];
				switch(transaction.entity_type){
					case 'customer':
						transaction.entity = transaction.Customer;
					break;
					case 'supplier':
						transaction.entity = transaction.Supplier;
					break;
				}
				transaction.created = new Date(transaction.created);
				transaction.timestamp = new Date(transaction.timestamp);
				transaction.backlogged = +transaction.timestamp !== +transaction.created;
				
				$scope.Transactions.push(transaction);
			}
			$scope.LoadingTransactions = false; 
			$scope.GoToPage = null;
			paginate($response.meta.pages);
		}
		function errorGetTransactions($response, $status, $scope){ }
		function paginate(__pages){
			$scope.EnablePagination = true;
			if($scope.Pages.length==0){
				for(var i=1;i<=__pages;i++){
					$scope.Pages.push(i);
				}
				$scope.LastPage = $scope.Pages.length;
			}
			updatePages();
		}
		function updatePages(){
			$scope.ActivePages=[];
			for(var i=$scope.CurrentOffset*NAV_PAGE_COUNT,ctr=1;i<$scope.Pages.length&&ctr<=NAV_PAGE_COUNT;i++,ctr++){
				$scope.ActivePages.push($scope.Pages[i]);
			}
		}
		function customFilter(__filter){
			if(__filter.coverage=='custom'){
				var __f =  angular.copy(__filter);
				delete __f.coverage;
				var Datefrom =  new Date($scope.CustomDate.from);
				var Dateto =  new Date($scope.CustomDate.to);
				__f.from =  Datefrom.getFullYear()+'-'+(Datefrom.getMonth()+1)+'-'+Datefrom.getDate();
				__f.to =  Dateto.getFullYear()+'-'+(Dateto.getMonth()+1)+'-'+Dateto.getDate();
				__filter = __f;
			}
			return __filter;
		}
    }]);
});


