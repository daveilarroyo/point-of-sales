define(function() {
  return {
	 user:{'access_token':'f4k3axxt0k3n','user_id':9999,'gmail_account':'test@gmail.com','full_name':'Demo User','avatar':'assets/img/temp-logo.png'},
	 http:{'status':'00','response':{}},
	 products:[
		{'id':1,'part_no':'ABC-123','barcode':'12345','description':'Sample Item 1','particular':'Item 1','capital':10.00,'price':11.75,'quantity':33},
		{'id':2,'part_no':'DEF-456','barcode':'12346','description':'Sample Item 2','particular':'Item 2','capital':20.00,'price':22.25,'quantity':44},
		{'id':3,'part_no':'GHI-789','barcode':'12347','description':'Sample Item 3','particular':'Item 3','capital':30.00,'price':33.00,'quantity':55},
		{'id':4,'part_no':'JKL-012','barcode':'12348','description':'Sample Item 4','particular':'Item 4','capital':40.00,'price':44.50,'quantity':66},
	 ],
	categories:[],
	vendors:[
		{id:1,name:'Supplier A'},
		{id:3,name:'Supplier B'},
		{id:3,name:'Supplier C'},
	 ],
	 customers:[
		{id:0,name:'Cash'},
		{id:1,name:'Juan Dela Cruz'},
		{id:2,name:'Andres Bonifacio'},
		{id:3,name:'Pedro Masipag'},
	 ],
	 transactions:[
		{	
			id:1,
			header:{type:'orders',entity_type:'vendor',entity:'Sample vendor',entity_id:'Sample vendor',date:'2015-01-01',amount:180,status:'ordered'},
			details:[
				{amount: 60,capital: 30,description: "Sample Item 2",id:2,ordered: 2,quantity: 2,timestamp: 1428541605693},
				{amount: 60,capital: 30,description: "Sample Item 3",id:3,ordered: 2,quantity: 2,timestamp: 1428541605693},
				{amount: 60,capital: 30,description: "Sample Item 1",id:1,ordered: 2,quantity: 2,timestamp: 1428541605693},
			],
			payments:[]
		}
	 ],
    '/getModules':function(){
		this.http.response={};
		this.http.response.Modules  = [
			{ 'icon':'glyphicon glyphicon-home','title':'Home ','link':'#/'},
			{ 'icon':'glyphicon glyphicon-shopping-cart','title':'Sales ','link':'#/sales/homepage'},
			{ 'icon':'glyphicon glyphicon-tags','title':'Assessment ','link':'#/inventory/assessment'},
			{ 'icon':'glyphicon glyphicon-repeat','title':'Return Order ','link':'#/inventory/returnorder'},
			{ 'icon':'glyphicon glyphicon-log-in','title':'Deliveries','link':'#/deliveries/homepage'},
			{ 'icon':'glyphicon glyphicon-log-out','title':'Orders','link':'#/orders/homepage'},
			{ 'icon':'glyphicon glyphicon-inbox','title':'Inventory','link':'#/inventory/homepage'},
			{ 'icon':'glyphicon glyphicon-object-align-bottom','title':'Stock Room','link':'#/inventory/stockroom'},
			{ 'icon':'glyphicon glyphicon-transfer','title':'Transactions','link':'#/transactions/homepage'},
			{ 'icon':'glyphicon glyphicon-user','title':'Accounts','link':'#/accounts/homepage'},
			{ 'icon':'glyphicon glyphicon-th-list','title':'Ledgers','link':'#/accounts/ledger'},
			{ 'icon':'glyphicon glyphicon-pawn','title':'Ledgers','link':'#/accounts/users'},
			{ 'icon':'glyphicon glyphicon-off','title':'Log out','link':'#/logout'},
			];
		return this.http;
	},
	'/authenticate':function(){
		this.http.response={};
		this.http.response.code  = '00';
		this.http.response.message  = 'Authentication successful.';
		this.http.response.auth_token  = 'ffwoeijo434f43ff3';
		return this.http;
	},
	'/authenticate/route':function(){
		this.http.response={};
		this.http.response.IsAuthenicated  = true;
		this.http.response.code  = '00';
		this.http.response.message  = 'Access Granted.';
		this.http.response.auth_token  = 'ffwoeijo434f43ff3';
		return this.http;
	},
  '/getProduct':function(data){
	  var product = data;
	  var code = '00';
	  var message = 'Product updated';
	  //Check data if valid
	  if(!product.hasOwnProperty('id')){
		  code = '02';
		  message = 'Required field id not found,';
	  }else if(!this.products[product.id-1]){
		  code = '02';
		  message = 'Product id supplied could not be found.';
	  }else{
		 data =  this.products[product.id-1] ; //Return if found
	  }
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = code;
	  this.http.response.message  = message;
	  this.http.response.data  = data;
	  return this.http;
  },
  '/getProducts':function(){
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Product list retrieved.';
	  this.http.response.data  = this.products;
	  return this.http;
  },
  '/getProductByDescription':function(){
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Product list retrieved.';
	  this.http.response.data  = this.products;
	  return this.http;
  },
  '/addProduct':function(data){
	  //Product object
	  var product = data;
	  product.id = this.products.length+1;
	  this.products.push(product);
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Product added.';
	  this.http.response.data  = product;
	  return this.http;
  },
  '/updateProduct':function(data){
	  var product = data;
	  var code = '00';
	  var message = 'Product updated';
	  //Check data if valid
	  if(!product.hasOwnProperty('id')){
		  code = '02';
		  message = 'Required field id not found,';
	  }else if(!this.products[product.id-1]){
		  code = '02';
		  message = 'Product id supplied could not be found.';
	  }else{
		  this.products[product.id-1]  = data; //Update if found
	  }
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = code;
	  this.http.response.message  = message;
	  this.http.response.data  = data;
	  return this.http;
  },
  '/changeProductStatus':function(data){
	  return this['/updateProduct'](data);	  
  },
  '/addTransaction':function(data){
	  //Transaction object
	  var transaction = data;
	  transaction.id = this.transactions.length+1;
	  var productFlag = 0;
	  //Assign product flag based on transaction header type
	  switch(transaction.header.type){
			case 'sales':
				productFlag = -1;
			break;
			case 'orders':
				productFlag = 0;
			break;
			case 'deliveries':
				productFlag = 1;
			break;
	  }
	  if(!transaction.header.entity_id){
		  var entity = transaction.header.entity;
		  switch(transaction.header.entity_type){
			  case 'vendor': 	this['/addVendor']({name:entity}); 	break;
			  case 'customer': 	this['/addCustomer']({name:entity});break;
		  }
	  }
	  //Loop on transaction details
	  if(productFlag!=0){
		  for(index in transaction.details){
			var item = transaction.details[index];
			var product  = this.products[item.id-1];
			var quantity = parseFloat(item.quantity);
			product.quantity = parseFloat(product.quantity) + (quantity*productFlag); //Compute for new quantity
			if(transaction.header.type=='deliveries'){
				var old_price =  parseFloat(product.price);
				var old_capital =  parseFloat(product.capital);
				var new_capital = parseFloat(item.capital);
				//Update only if new_capital is greater than old_capital
				if(new_capital > old_capital ){
					var difference = new_capital - old_capital;
					product.capital = new_capital;
					product.price = old_price + difference; //Carry over difference for price adjustments
				}
			}
			this.products[item.id-1] = product; //Applly changes
		  }
	  }
	  //Save to transaction logs
	  this.transactions.push(transaction);
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Transaction added.';
	  this.http.response.data  = transaction;
	  return this.http;
  },
  '/getTransaction':function(data){
	  var transaction = data;
	  var code = '00';
	  var message = 'Transaction found';
	  //Check data if valid
	  if(!transaction.hasOwnProperty('id')){
		  code = '02';
		  message = 'Required field id not found,';
	  }else if(!this.transactions[transaction.id-1]){
		  code = '02';
		  message = 'Transaction id supplied could not be found.';
	  }else{
		 data =  this.transactions[transaction.id-1] ; //Return if found
	  }
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = code;
	  this.http.response.message  = message;
	  this.http.response.data  = data;
	  return this.http;
  },
  '/getTransactions':function(){
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Transaction list retrieved.';
	  this.http.response.data  = this.transactions;
	  return this.http;
  },
  '/addVendor':function(data){
	  //Vendor object
	  var vendor = data;
	  vendor.id = this.vendors.length+1;
	  this.vendors.push(vendor);
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Vendor added.';
	  this.http.response.data  = vendor;
	  return this.http;
  },
   '/addCustomer':function(data){
	  //Vendor object
	  var customer = data;
	  customer.id = this.customers.length+1;
	  this.customers.push(customer);
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Customer added.';
	  this.http.response.data  = customer;
	  return this.http;
  },
  '/getVendors':function(){
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Vendor list retrieved.';
	  this.http.response.data  = this.vendors;
	  return this.http;
  },
  '/getCustomers':function(){
	  //HTTP Response
	  this.http.response={};
	  this.http.response.code  = '00';
	  this.http.response.message  = 'Customer list retrieved.';
	  this.http.response.data  = this.customers;
	  return this.http;
  },
  }
});