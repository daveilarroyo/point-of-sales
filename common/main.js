require.config({
    baseUrl:'',
	urlArgs : "bust="+new Date().getTime(),
	waitSeconds: 60,
    // alias libraries paths
    paths: {
        'api': 'common/api',       
        'responses': 'common/responses',       
        'messages': 'common/messages',       
        'settings': 'common/settings',       
        'test': 'common/test',       
        'config': 'common/config',       
        'ui': 'controllers/main/UIController',
        'jquery': 'libs/bower_components/jquery/dist/jquery.min',
        'typeahead': 'libs/bower_components/typeahead.js/dist/typeahead.bundle',
        'angular': 'libs/bower_components/angular/angular.min',
		'angular-typeahead': 'libs/bower_components/angular-typeahead/angular-typeahead',
        'angular-route': 'libs/bower_components/angular-route/angular-route.min',
		'angular-cookies': 'libs/bower_components/angular-cookies/angular-cookies.min',
        'angularAMD': 'libs/bower_components/angularAMD/angularAMD.min',
        'ui-bootstrap' : 'libs/bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
        'blockUI': 'libs/bower_components/angular-block-ui/dist/angular-block-ui.min',
        'chart':'libs/bower_components/Chart.js/Chart',
        'angular-chart':'libs/bower_components/angular-chart.js/angular-chart',
        'ngload': 'libs/bower_components/angularAMD/ngload.min', 
		'angular-sanitize': 'libs/bower_components/angular-sanitize/angular-sanitize.min',
		'ajaxService':'services/AjaxServices',
		'productService':'services/ProductServices',
		'vendorService':'services/VendorServices',
		'customerService':'services/CustomerServices',
		'userService':'services/UserServices',
		'alertsService':'services/AlertsServices',
        'cashflowService':'services/cashflowServices',
		'productModalController':'controllers/shared/productModalController',
		'productTypeaheadController':'controllers/shared/productTypeaheadController',
		'transactionService':'services/TransactionServices',
		'transactionModalController':'controllers/shared/transactionModalController',
		'ledgerModalController':'controllers/shared/ledgerModalController',
		'accountModalController':'controllers/shared/accountModalController',
        'userModalController':'controllers/shared/userModalController',
        'cashflowModalController':'controllers/shared/cashflowModalController',
		'searchController':'controllers/shared/searchController',
    },
    // Add angular modules that does not support AMD out of the box, put it in a shim
    shim: {
		'jquery' : {exports : 'jQuery'},
        'angular' : {exports : 'angular'},
		'typeahead' : ['jquery'],
        'angular-typeahead': ['jquery','typeahead','angular'],
        'angularAMD': ['angular'],
        'angular-route': ['angular'],
        'blockUI': ['angular'],
        'angular-chart':['angular','chart'],
        'angular-sanitize': ['angular'],
        'ui-bootstrap': ['angular'],
		'angular-cookies': ['angular'], 
         
    },
    // kick start application
    deps: ['config']
});

window.onerror = function(error, url, line) {
    alert(JSON.stringify({acc:'error', data:'ERR:'+error+' URL:'+url+' L:'+line}));
};