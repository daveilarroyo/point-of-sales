define(function() {
  return {
	debugMode: false,
	demoMode: true,
	forceDemo: false,
	forceLive: false,
	authPage: '#/login',
	apiBaseUrl:window.location.origin+'/inventory/api/api',
	demoAlwaysSuccess: true,
	delayRequest: 100,
	dataType: 'json',
	sessionTimer: 1000*60*20,
	renewTimer: 1000*10,
  }
});