define(function() {
  return {
	'SUCCESS': 0,
	'INTERNAL_ERR': '99',
	'INVALID_INPUT': '01',
	'MISSING_INPUT': '02',
	'INVALID_AUTHORIZATION': 404,
	'INVALID_ACCESS': '100',
  }
});