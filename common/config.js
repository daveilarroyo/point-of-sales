"use strict";
define(['api','ui','angularAMD', 'angular-route', 'ui-bootstrap','angular-sanitize', 'blockUI','jquery','typeahead','angular-typeahead' ,'angular-chart', 'angular-cookies'], 
function (api,ui,angularAMD) {
    var app = angular.module("mainModule", 
        ['ngRoute', 'blockUI', 'ngSanitize', 'ui.bootstrap','siyfion.sfTypeahead','chart.js','ngCookies']);
          
    app.config(['$routeProvider', function ($routeProvider) {
   
    $routeProvider

    .when("/", angularAMD.route({
                         
        templateUrl: function (rp) {  return 'views/main/default.php';  },               
                controllerUrl: "controllers/main/DefaultController"            
    }))
	
	.when("/login", angularAMD.route({
                         
        templateUrl: function (rp) {  return 'views/main/login.php?bust='+new Date().getTime()  },               
                controllerUrl: "controllers/main/LoginController"            
    }))
	
	.when("/logout", angularAMD.route({
                         
        templateUrl: function (rp) {  return 'views/main/login.php?bust='+new Date().getTime()  },               
                controllerUrl: "controllers/main/LoginController"            
    }))
	
    .when("/dashboard", angularAMD.route({
                         
        templateUrl: function (rp) {  return 'views/main/dashboard.php?bust='+new Date().getTime()  },               
                controllerUrl: "controllers/main/DashboardController"            
    }))

    .when("/cashflow", angularAMD.route({
                         
        templateUrl: function (rp) {  return 'views/main/cashflow.php?bust='+new Date().getTime()  },               
                controllerUrl: "controllers/main/CashflowController"            
    }))

    .when("/:section/:tree", angularAMD.route({

        templateUrl: function (rp) { 
                     return 'views/' + rp.section + '/' + rp.tree + '.php?bust='+new Date().getTime(); },

        resolve: {
        load: ['$q', '$rootScope', '$location', 
            function ($q, $rootScope, $location) {
                
                 var path = $location.path();
                 var parsePath = path.split("/");
                 var parentPath = parsePath[1];
                 var controllerName = parsePath[2];
                 var loadController = "controllers/" + parentPath + "/" + 
                                       controllerName + "Controller";

                 var deferred = $q.defer();
                 require([loadController], function () {
                        $rootScope.$apply(function () {
                        deferred.resolve();
                 });
            });
            return deferred.promise;
            }]
        }
    }))
    
    .when("/:section/:tree/:id", angularAMD.route({

        templateUrl: function (rp) { 
                     return 'views/' + rp.section + '/' + rp.tree + '.php'; },

        resolve: {
        load: ['$q', '$rootScope', '$location', 
            function ($q, $rootScope, $location) {
                var path = $location.path();
                var parsePath = path.split("/");
                var parentPath = parsePath[1];
                var controllerName = parsePath[2];
                var loadController = "controllers/" + parentPath + "/" + 
                                      controllerName + "Controller";
                                             
                var deferred = $q.defer();
                require([loadController], function () {
                    $rootScope.$apply(function () {
                        deferred.resolve();
                        });
            });
            return deferred.promise;
            }]
            }
        }))
        .otherwise({ redirectTo: '/' }) 
    }]);                
	 var indexController = function ($scope, $rootScope, $http, $location, blockUI,$cookie) {
		//Event handler when URL changes
        $scope.$on('$routeChangeStart', function (scope, next, current) {
			$rootScope.activeModule = '#'+$location.path();
			checkUser();
            if (!$rootScope.IsloggedIn) api.reauthorize();
			else $scope.authenicateUser($location.path(),$scope.authenicateUserComplete, $scope.authenicateUserError);
        });
		//Event handler when URL change finishes
        $scope.$on('$routeChangeSuccess', function (scope, next, current) {
			$rootScope.__Progress = 0 ; //Reset __Progress to remove progress bar
			if(api.getSettings().debugMode) console.log(next,current);
        });
		//Initialize controller
        $scope.initializeController = function () {
			//if(!$rootScope.IsloggedIn) api.reauthorize();
        }
		//Success function when initialization is completed
        $scope.initializeApplicationComplete = function (response) {
			//$rootScope.IsloggedIn = true;  
            //$rootScope.Modules = response.Modules;
			$rootScope.IsInitialized = true;
        }
		//Error function when initialization is denied
		$scope.initializeApplicationError = function (response) {
		   api.error(response);
        }
		//Request initialization on API
        $scope.initializeApplication = function (successFunction, errorFunction) {
			api.POST("/getModules", successFunction, errorFunction,$scope);
        };
        //Authorize user when accessing a route      
        $scope.authenicateUser = function (route, successFunction, errorFunction) {
            var authenication = {};
            authenication.route = route;
			var validRoute=false;
			angular.forEach($rootScope.Modules,function(module,index){
				validRoute = validRoute || route=='/'+module.link;
			});
			if(route==''||route=='/'||route=='/logout' ||route=='/login' || route=='/dashboard' || route=='/cashflow') validRoute = true;
			if(validRoute){
				successFunction({IsAuthenicated:true});
			}else{
				api.redirect('#/login');
			}
            //api.POST( "/authenticate/route", successFunction, errorFunction,$rootScope,authenication);
        };
        //Success function when authorization is completed   
        $scope.authenicateUserComplete = function (response) {
            if (!response.IsAuthenicated) api.reauthorize();
			else if(!$rootScope.IsInitialized) $scope.initializeApplication($scope.initializeApplicationComplete, $scope.initializeApplicationError);
        }
		//Error function when authorization is denied
        $scope.authenicateUserError = function (response) {
           api.error(response);
        }
		$scope.closeAlert=function(index,delay){
			$rootScope.alerts.splice(index, 1);
		}
		function checkUser(){
			if(!$rootScope.User){
				$rootScope.IsloggedIn = typeof $cookie.get('__user') == 'object';
				if($rootScope.IsloggedIn){
					$rootScope.User = $cookie.get('__user');
					$rootScope.Modules= $cookie.get('__modules');
                    api.initSessionTimer();
				}
			}
		}
    };

    indexController.$inject = ['$scope', '$rootScope', '$http', '$location', 'blockUI','$cookieStore'];
    ui.$inject = ['$scope', '$rootScope','$modal'];
	var rootModal={};
	rootModal = function($scope,$sce){
		$scope.initializeController = function(){
			//console.log('init root modal');
		}
		$scope.renderHtml = function(html_code){
				return $sce.trustAsHtml(html_code);
		};
	}
	rootModal.$inject = ['$scope','$sce'];
    app.controller("indexController", indexController);
    app.controller("uiController", ui);
    app.controller("rootModalController", rootModal);
	
	var autoSelect = function ($window) {
		 return {
			restrict: 'A',
			link: function (scope, element) {
				var focusedElement;
				element.on('click', function () {
					if (focusedElement != this) {
						this.select();
						focusedElement = this;
					}
				});
				element.on('blur', function () {
					focusedElement = null;
				});
			}
		};
	};
	autoSelect.$inject = ['$window'];
	app.directive("autoSelect", autoSelect);
	
    // Bootstrap Angular when DOM is ready
    angularAMD.bootstrap(app);
	
    return app;
});