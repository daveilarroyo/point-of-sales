define(['jquery','settings','responses','messages','test'], function(jquery,settings,resp,msg,test){
		var GET = 'get';
		var POST = 'post';
		var SESSION_TIMER;
		var SESSION_INTERVAL,CALLBACK_TIMEOUT,CALLBACK_INTERVAL;
		var BASE_URL = settings.apiBaseUrl;
		var api = {};
		
		//Public function
		api.authenticate = function(validUserFn,invalidUserFn,$scope,data){
			this.POST('/authenticate',validUserFn,invalidUserFn,$scope,data);
		}
		api.initSessionTimer = function(){
			SESSION_TIMER=settings.sessionTimer;
			console.log('timer starter',SESSION_TIMER);
			api.updateSessionTimer();
		}
		api.updateSessionTimer = function(){
			SESSION_INTERVAL = setInterval(function(){
				SESSION_TIMER-=1000;
				if(SESSION_TIMER==0){
					$scope = angular.element($('#wrapper')).scope();
					$scope.$emit('openRootModal',
						{
							title:'Oops!',message:"Timeout. Logging out in...", "time":settings.renewTimer/1000,
							callback:function($scope,$modalInstance){
								CALLBACK_TIMEOUT = setTimeout(
								function(){
									$modalInstance.close();
									clearInterval(CALLBACK_INTERVAL);
									clearInterval(SESSION_INTERVAL);
									api.redirect('#/logout');
								},settings.renewTimer);
								CALLBACK_INTERVAL = setInterval(function(){
									$scope.$apply(function(){
										$scope.time--;	
									});
								},1000);
								
							}
						}
						,{
							buttons:{
								'Continue?':function(){
									api.renewSession();
									clearTimeout(CALLBACK_INTERVAL);
									clearTimeout(CALLBACK_TIMEOUT);
								},
							}
						})
				}
				
			},1000);
			
		}
		api.clearSessionTimer = function(route){
			console.log(route);
			clearInterval(SESSION_INTERVAL);
			api.initSessionTimer();
		}
		api.logout = function(validUserFn,invalidUserFn,$scope,data){
			this.POST('/logout',validUserFn,invalidUserFn,$scope,data);
		}
		api.redirect = function(url){
			window.location = url;
		}
		api.reauthorize = function(){
			api.redirect(settings.authPage);
		}
		api.getSettings = function(){
			return settings;
		}
		api.error = function(response){
			var message = "ERROR:" + response;
			alert(message);
			api.reauthorize();
		}
		api.renewSession = function(){
			api.GET('/renew',null,null,null,{forceLive:true});
		}
        api.GET = function (route, successFunction, errorFunction,$scope,data) {
			if(settings.debugMode)  console.log('GET ', route);
			excuteRequest(GET,route, successFunction, errorFunction,$scope,data);
        };
		api.POST = function (route, successFunction, errorFunction,$scope,data) {
			if(settings.debugMode) console.log('POST ', route);
			excuteRequest(POST,route, successFunction, errorFunction,$scope,data);
        };
		//Private function 
		function excuteRequest(requestType,route, successFunction, errorFunction,$scope,data){
			 setTimeout(function () {
				 if(typeof data == 'undefined') data = {};
				 if(!data.hasOwnProperty('forceDemo')) data.forceDemo = settings.forceDemo;
				 if(!data.hasOwnProperty('forceLive')) data.forceLive = settings.forceLive;
				if(!settings.demoMode||data.forceLive){
					if(route!='/logout') api.clearSessionTimer(route);//Reset timer
					if(!data.fullURL) route = BASE_URL+route; //Prepend API Base URL
					
					delete data.forceDemo;
					delete data.forceLive;
					
					data  = {data:data}; //Wrap data for CakePHP format

					switch(requestType){
						case GET: //GET request handling
						case POST: //POST request handling
						case PUT: //POST request handling
							return jquery[requestType](route,data,function (http, status, headers, config) {                 
								handleResponse(http,successFunction, errorFunction,$scope); //Handle response base on live data
							},settings.dataType);
						break;
					}
				}
				if(settings.demoMode||data.forceDemo){
						http = runDemo(route, data); //Run demo function
						return handleResponse(http,successFunction, errorFunction,$scope); //Handle response base on test data
				}
			}, settings.delayRequest); //Simulate latency
		}
		function runDemo(route,data){
			var http = {};
			if(test.hasOwnProperty(route)){
					http = test[route](data);
			}else{
				return alert(msg.INVALID_ROUTE+route);
			}
			//Bypass status on test.js base on settings.js
			if(settings.demoAlwaysSuccess){
				http.status = resp.SUCCESS;
			}else{
				http.status = resp.INTERNAL_ERR; //Default error
			}
			return http;
		}
		function handleResponse(http,successFunction, errorFunction,$scope){
			if(settings.debugMode) console.log(http);				
			switch(http.status){
				case resp.SUCCESS: //Success
					if(successFunction) successFunction(http.response, http.status,$scope);
					if($scope)$scope.$apply();
				break;
				case resp.INTERNAL_ERR: //Errors
				case resp.INVALID_INPUT:
				case resp.MISSING_INPUT:
				case resp.INVALID_AUTHORIZATION:
				case resp.INVALID_ACCESS:
					if(errorFunction)  errorFunction(http.response,http.status,$scope);
					if($scope)$scope.$apply();
				break;
				default: //Unknown response
					console.log(msg.UNKNOWN_CODE);
				break;
			}
		}
		return api;
});