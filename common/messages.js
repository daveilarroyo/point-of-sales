define(function() {
  return {
	'INVALID_ROUTE': 'Invalid route:',
	'UNKNOWN_CODE': 'Unknown response code.',
  }
});