define(['config','api'], function (app,api) {
	app.register.service('userService', ['$rootScope', function ($rootScope) {
		const ALLOW_CACHE = false;
		const CONFIG = {page:1,limit:500,keyword:null,fields:['username','first_name','last_name'],'export':['created','ref_no','amount','flag','user_id'],filter:null,sort:'latest'};
		const EXPORT_FORMAT = 'csv';
		this.addUser = function(successAddUser,errorAddUser,$scope){
			var user = {};
			api.POST("/addUser",successAddUser,errorAddUser,$scope,user);
		};
		this.updateUser = function(successUpdateUser,errorUpdateUser,$scope){
			var user = {};
			api.POST("/updateUser",successUpdateUser,errorUpdateUser,$scope,user);
			
		};
		this.saveUser = function(successSaveUser,errorSaveUser,$scope,data){
			var user = data;
				user.forceLive=true;
			api.POST("/users",successSaveUser,errorSaveUser,$scope,user);
		};
		this.getUser = function (successGetUser,errorGetUser,$scope,id){
			var user = {};
				user.id = id;
				user.forceLive=true;
				api.GET("/users",successGetUser,errorGetUser,$scope,user);
		}
		
		this.getUsers = function (successGetUsers,errorGetUsers,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(key=='sort') continue;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			api.GET("/users?"+config_str.join('&')+"&page="+config.page,successGetUsers,errorGetUsers,$scope,{forceLive:true});
		}
		this.getUserLedgers = function (successGetUserLedgers,errorUserLedgers,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			api.GET("/user_ledgers?"+config_str.join('&')+"&page="+config.page,successGetUserLedgers,errorUserLedgers,$scope,{forceLive:true});
		}
		this.getModules = function (successGetModules,errorGetModules,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(key=='fields') value = 'name';
					if(key=='sort') continue;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			api.GET("/modules?"+config_str.join('&')+"&page="+config.page,successGetModules,errorGetModules,$scope,{forceLive:true});
		}
		this.getUserByDescription= function (successGetUsers,errorGetUsers,$scope){
			api.GET("/getUserByDescription",successGetUsers,errorGetUsers,$scope);
		}
		this.saveLedger = function(successSaveLedger,errorSaveLedger,$scope,data){
			var ledger = {};
				ledger.user_id = data.id;
				ledger.ref_no = data.ref_no;
				ledger.particulars = data.particulars;
				ledger.amount = data.amount;
				ledger.flag = data.flag=='+'?'c':'d';
				ledger.forceLive = true;
			api.POST('/user_ledgers',successSaveLedger,errorSaveLedger,$scope,ledger);
		}
		this.exportData = function(config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.format==undefined) config.format = EXPORT_FORMAT;
				if(config['export']==undefined) config['export'] = CONFIG['export'].join(',');
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			window.open(api.getSettings().apiBaseUrl+"/user_ledgers?"+config_str.join('&'));
		}
	}]);
});