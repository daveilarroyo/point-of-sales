define(['config','api'], function (app,api) {
	app.register.service('productService', ['$rootScope', function ($rootScope) {
		const ALLOW_CACHE = false;
		const CONFIG = {page:1,limit:500,keyword:null,fields:['display_title'],'export':['soh_quantity','tmp_quantity','status','display_title','capital','srp'],filter:null,sort:null};
		const EXPORT_FORMAT = 'csv';
		var __products;
		var __callback ;
		var __successGetProducts =   function(response,status,$scope){
			__products = response;
			if(__callback) __callback(response,status,$scope);
		}
		this.addProduct = function(successAddProduct,errorAddProduct,$scope){
			var product = {};
				product.category_id = $scope.category_id;
				product.part_no = $scope.part_no;
				product.unit = $scope.unit;
				product.length = $scope.length;
				product.width = $scope.width;
				product.particular = $scope.particular;
				product.description = $scope.description;
				product.capital = $scope.capital;
				product.markup = $scope.markup;
				product.srp = $scope.srp;
				product.soh_quantity = $scope.soh_quantity;
				product.min_quantity = $scope.min_quantity;
				product.max_quantity = $scope.max_quantity;
				product.discountable = $scope.discountable;
				product.forceLive=true;
			api.POST("/products",successAddProduct,errorAddProduct,$scope,product);
			
		};
		this.updateProduct = function(successUpdateProduct,errorUpdateProduct,$scope){
			var product = {};
				console.log($scope);
				product.id = $scope.id;
				product.category_id = $scope.category_id;
				product.part_no = $scope.part_no;
				product.unit = $scope.unit;
				product.length = $scope.length;
				product.width = $scope.width;
				product.particular = $scope.particular;
				product.description = $scope.description;
				product.capital = $scope.capital;
				product.markup = $scope.markup;
				product.srp = $scope.srp;
				product.soh_quantity = $scope.soh_quantity;
				product.adjusted = $scope.adjusted;
				product.min_quantity = $scope.min_quantity;
				product.max_quantity = $scope.max_quantity;
				product.discountable = $scope.discountable;
				product.forceLive=true;
			api.POST("/products",successUpdateProduct,errorUpdateProduct,$scope,product);
			
		};
		this.updateProductQuantity = function(successUpdateProductQuantity,errorUpdateProductQuantity,$scope){
			var product = {};
				product.id = $scope.id;
				product.action = 'update';
				product.tmp_quantity = $scope.tmp_quantity;
				product.forceLive=true;
			api.POST("/products",successUpdateProductQuantity,errorUpdateProductQuantity,$scope,product);
		}
		this.getProduct = function (successGetProduct,errorGetProduct,$scope,id){
			var product = {};
				product.id = id;
				product.forceLive=true;
				api.GET("/products",successGetProduct,errorGetProduct,$scope,product);
		}
		this.archiveProduct = function (successDeleteProduct,errorDeleteProduct,$scope,id){
			var product = {};
				product.id = id;
				product.action = 'archive';
				product.forceLive=true;
				api.POST("/products",successDeleteProduct,errorDeleteProduct,$scope,product);
		}
		this.activateProduct = function (successDeleteProduct,errorDeleteProduct,$scope,id){
			var product = {};
				product.id = id;
				product.action = 'activate';
				product.forceLive=true;
				api.POST("/products",successDeleteProduct,errorDeleteProduct,$scope,product);
		}
		this.getProducts = function (successGetProducts,errorGetProducts,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			__callback = successGetProducts;
			if(__products&&ALLOW_CACHE){
				__callback(__products,"00",$scope);
			}else{
				var config_str = [];
				for(var key in config){
					if(key!='page' && config[key] ){
						var value = config[key];
						if(typeof value == "object") value = JSON.stringify(value);
						config_str.push(key+'='+value);
					}
				}
				api.GET("/products?"+config_str.join('&')+"&page="+config.page,__successGetProducts,errorGetProducts,$scope,{forceLive:true});
			}
		}
		this.getProductByDescription= function (successGetProducts,errorGetProducts,$scope){
			api.GET("/getProductByDescription",successGetProducts,errorGetProducts,$scope);
		}
		this.exportData = function(config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.format==undefined) config.format = EXPORT_FORMAT;
				if(config['export']==undefined) config['export'] = CONFIG['export'].join(',');
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			window.open(api.getSettings().apiBaseUrl+"/products?"+config_str.join('&'));
			//api.GET("/products?"+config_str.join('&'),successExport,errorExport,$scope,{forceLive:true});
		}
	}]);
});