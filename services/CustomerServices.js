define(['config','api'], function (app,api) {
	app.register.service('customerService', ['$rootScope', function ($rootScope) {
		const ALLOW_CACHE = false;
		const CONFIG = {page:1,limit:500,keyword:null,fields:['particulars','ref_no','entity_name'],'export':['timestamp','ref_no','amount','flag','customer_id'],filter:null,sort:'latest'};
		const EXPORT_FORMAT = 'csv';
		this.addCustomer = function(successAddCustomer,errorAddCustomer,$scope){
			var customer = {};
			api.POST("/addCustomer",successAddCustomer,errorAddCustomer,$scope,customer);
		};
		this.updateCustomer = function(successUpdateCustomer,errorUpdateCustomer,$scope){
			var customer = {};
			api.POST("/updateCustomer",successUpdateCustomer,errorUpdateCustomer,$scope,customer);
			
		};
		this.saveCustomer = function(successSaveCustomer,errorSaveCustomer,$scope,data){
			var customer = data;
				customer.forceLive=true;
			api.POST("/customers",successSaveCustomer,errorSaveCustomer,$scope,customer);
		};
		this.getCustomer = function (successGetCustomer,errorGetCustomer,$scope,id){
			var customer = {};
				customer.id = id;
				customer.forceLive=true;
				api.GET("/customers",successGetCustomer,errorGetCustomer,$scope,customer);
		}
		this.getCustomers = function (successGetCustomers,errorGetCustomers,$scope){
			api.GET("/getCustomers",successGetCustomers,errorGetCustomers,$scope);
		}
		this.getCustomers = function (successGetCustomers,errorGetCustomers,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(key=='fields') value = 'name';
					if(key=='sort') continue;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			api.GET("/customers?"+config_str.join('&')+"&page="+config.page,successGetCustomers,errorGetCustomers,$scope,{forceLive:true});
		}
		this.getCustomerLedgers = function (successGetCustomerLedgers,errorCustomerLedgers,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			api.GET("/customer_ledgers?"+config_str.join('&')+"&page="+config.page,successGetCustomerLedgers,errorCustomerLedgers,$scope,{forceLive:true});
		}
		this.getCustomerByDescription= function (successGetCustomers,errorGetCustomers,$scope){
			api.GET("/getCustomerByDescription",successGetCustomers,errorGetCustomers,$scope);
		}
		this.saveLedger = function(successSaveLedger,errorSaveLedger,$scope,data){
			var ledger = {};
				ledger.customer_id = data.id;
				ledger.ref_no = data.ref_no;
				ledger.timestamp = data.timestamp;
				ledger.particulars = data.particulars;
				ledger.amount = data.amount;
				ledger.flag = data.flag=='+'?'c':'d';
				ledger.forceLive = true;
			api.POST('/customer_ledgers',successSaveLedger,errorSaveLedger,$scope,ledger);
		}
		this.exportData = function(config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.format==undefined) config.format = EXPORT_FORMAT;
				if(config['export']==undefined) config['export'] = CONFIG['export'].join(',');
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			window.open(api.getSettings().apiBaseUrl+"/customer_ledgers?"+config_str.join('&'));
		}
	}]);
});