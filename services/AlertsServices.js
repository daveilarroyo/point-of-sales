﻿define(['config'], function (app) {

    app.register.service('alertsService', ['$rootScope','$timeout', function ($rootScope,$timeout) {

        $rootScope.alerts = [];
        $rootScope.MessageBox = "";

        this.SetValidationErrors = function (scope, validationErrors) {

            for (var prop in validationErrors) {
                var property = prop + "InputError";
                scope[property] = true;
            }        

        }

        this.RenderErrorMessage = function (message) {

            var messageBox = formatMessage(message);
            $rootScope.alerts = [];
            $rootScope.MessageBox = messageBox;
            $rootScope.alerts.push({ 'type': 'danger', 'msg': message, 'timestamp': getTimeStamp() });

        };

        this.RenderSuccessMessage = function (message) {

            var messageBox = formatMessage(message);
            $rootScope.alerts = [];
            $rootScope.MessageBox = messageBox;
            $rootScope.alerts.push({ 'type': 'success', 'msg': message, 'timestamp': getTimeStamp()});	
			$timeout(function(){$rootScope.alerts.splice(0, 1);},2500);			
		
        };

        this.RenderWarningMessage = function (message) {

            var messageBox = formatMessage(message);
            $rootScope.alerts = [];
            $rootScope.MessageBox = messageBox;
            $rootScope.alerts.push({ 'type': 'warning', 'msg': message, 'timestamp': getTimeStamp() });
        };

        this.RenderInformationalMessage = function (message) {

            var messageBox = formatMessage(message);
            $rootScope.alerts = [];
            $rootScope.MessageBox = messageBox;
            $rootScope.alerts.push({ 'type': 'info', 'msg': message, 'timestamp': getTimeStamp() });
        };

        this.closeAlert = function (index) {
            $rootScope.alerts.splice(index, 1);
        };
		function getTimeStamp(){
			return new Date().getTime();
		}
        function formatMessage(message) {
            var messageBox = "";
            if (angular.isArray(message) == true) {
                for (var i = 0; i < message.length; i++) {
                    messageBox = messageBox + message[i] + "<br/>";
                }
            }
            else {
                messageBox = message;
            }

            return messageBox;

        }

    }]);
});