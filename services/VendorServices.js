define(['config','api'], function (app,api) {
	app.register.service('vendorService', ['$rootScope', function ($rootScope) {
		const ALLOW_CACHE = false;
		const CONFIG = {page:1,limit:500,keyword:null,fields:['particulars','ref_no','entity_name'],'export':['timestamp','ref_no','amount','flag','supplier_id'],filter:null,sort:'latest'};
		const EXPORT_FORMAT = 'csv';
		this.addVendor = function(successAddVendor,errorAddVendor,$scope){
			var vendor = {};
			api.POST("/addVendor",successAddVendor,errorAddVendor,$scope,vendor);
		};
		this.updateVendor = function(successUpdateVendor,errorUpdateVendor,$scope){
			var vendor = {};
			api.POST("/updateVendor",successUpdateVendor,errorUpdateVendor,$scope,vendor);
			
		};
		this.saveVendor = function(successSaveVendor,errorSaveVendor,$scope,data){
			var vendor = data;
				vendor.forceLive=true;
			api.POST("/suppliers",successSaveVendor,errorSaveVendor,$scope,vendor);
		};
		this.getVendor = function (successGetVendor,errorGetVendor,$scope,id){
			var vendor = {};
				vendor.id = id;
				vendor.forceLive=true;
			api.GET("/suppliers",successGetVendor,errorGetVendor,$scope,vendor);
		}
		this.getVendors = function (successGetVendors,errorGetVendors,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(key=='fields') value = 'name';
					if(key=='sort') continue;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			api.GET("/suppliers?"+config_str.join('&')+"&page="+config.page,successGetVendors,errorGetVendors,$scope,{forceLive:true});
		}
		this.getVendorLedgers = function (successGetVendorLedgers,errorVendorLedgers,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			api.GET("/supplier_ledgers?"+config_str.join('&')+"&page="+config.page,successGetVendorLedgers,errorVendorLedgers,$scope,{forceLive:true});
		}
		this.getVendorByDescription= function (successGetVendors,errorGetVendors,$scope){
			api.GET("/getVendorByDescription",successGetVendors,errorGetVendors,$scope);
		}
		this.saveLedger = function(successSaveLedger,errorSaveLedger,$scope,data){
			var ledger = {};
				ledger.supplier_id = data.id;
				ledger.ref_no = data.ref_no;
				ledger.timestamp = data.timestamp;
				ledger.particulars = data.particulars;
				ledger.amount = data.amount;
				ledger.flag = data.flag=='+'?'c':'d';
				ledger.forceLive = true;
			api.POST('/supplier_ledgers',successSaveLedger,errorSaveLedger,$scope,ledger);
		}
		this.exportData = function(config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.format==undefined) config.format = EXPORT_FORMAT;
				if(config['export']==undefined) config['export'] = CONFIG['export'].join(',');
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(value.entity) delete value.entity;
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			window.open(api.getSettings().apiBaseUrl+"/supplier_ledgers?"+config_str.join('&'));
		}
	}]);
});