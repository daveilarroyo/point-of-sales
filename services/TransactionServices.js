define(['config','api'], function (app,api) {
	app.register.service('transactionService', ['$rootScope','$interval', function ($rootScope,$interval) {
		const ALLOW_CACHE = false;
		const CONFIG = {page:1,limit:2,keyword:null,fields:['entity_name'],'export':['timestamp','type','status','user','amount','entity_type','entity_id','Supplier.name','Customer.name'],filter:null,sort:'latest'};
		const EXPORT_FORMAT = 'csv';
		var __products;
		var __callback ;
		var __successGetTransactions =   function(response,status,$scope){
			__products = response;
			if(__callback) __callback(response,status,$scope);
		}
		this.addTransaction = function(successAddTransaction,errorAddTransaction,$scope,data){
			var transaction = {};
				transaction.header = data.header;
				transaction.details = data.details;
				transaction.payments =data.payments;
				transaction.forceLive = true;
				$rootScope.transactionInProgress = true;
				$rootScope.__Progress = 0;
				$interval(function(){
					if($rootScope.__Progress < 100)
						$rootScope.__Progress +=10;
				},300,5);
			api.POST("/transactions",
				function($response, $status, $scope){
					$rootScope.transactionInProgress = false;
					$rootScope.__Progress = 100;
					successAddTransaction($response, $status, $scope);
				},
				function($response, $status, $scope){
					$rootScope.transactionInProgress = false;
					$rootScope.__Progress = 100;
					errorAddTransaction($response, $status, $scope);
				},$scope,transaction);
			
		};
		this.updateTransaction = function(successUpdateTransaction,errorUpdateTransaction,$scope){
			var transaction = {};
			api.POST("/updateTransaction",successUpdateTransaction,errorUpdateTransaction,$scope,transaction);
			
		};
		this.cancelTransaction = function(successCancelTransaction,errorCancelTransaction,$scope,data){
			var transaction = {};
				transaction.header = data.header;
				transaction.details = data.details;
				transaction.payments =data.payments;
				transaction.action = 'cancel';
				transaction.forceLive = true;
				$rootScope.transactionInProgress = true;
				$rootScope.__Progress = 0;
				$interval(function(){
					if($rootScope.__Progress < 100)
						$rootScope.__Progress +=10;
				},300,5);
			api.POST("/transactions",
				function($response, $status, $scope){
					$rootScope.transactionInProgress = false;
					$rootScope.__Progress = 100;
					successCancelTransaction($response, $status, $scope);
				},
				function($response, $status, $scope){
					$rootScope.transactionInProgress = false;
					$rootScope.__Progress = 100;
					errorCancelTransaction($response, $status, $scope);
				},$scope,transaction);
			
		};
		this.getTransaction = function (successGetTransaction,errorGetTransaction,$scope,data){
			var transaction = {};
				transaction.type = data.type;
				transaction.ref_no = data.ref_no;
				transaction.forceLive = true;
			api.GET("/transactions",successGetTransaction,errorGetTransaction,$scope,transaction);
		}
		this.getTransactions = function (successGetTransactions,errorGetTransactions,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
				if(config.dashboard){ config.sort='oldest'; delete config.limit;}
			}
			__callback = successGetTransactions;
			if(__products&&ALLOW_CACHE){
				__callback(__products,"00",$scope);
			}else{
				var config_str = [];
				for(var key in config){
					if(key!='page' && config[key] ){
						var value = config[key];
						if(typeof value == "object") value = JSON.stringify(value);
						config_str.push(key+'='+value);
					}
				}
				api.GET("/transactions?"+config_str.join('&')+"&page="+config.page,__successGetTransactions,errorGetTransactions,$scope,{forceLive:true});
			}
		}
		this.getTransactionByDescription= function (successGetTransactions,errorGetTransactions,$scope){
			api.GET("/getTransactionByDescription",successGetTransactions,errorGetTransactions,$scope);
		}
		this.exportData = function(config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.format==undefined) config.format = EXPORT_FORMAT;
				if(config['export']==undefined) config['export'] = CONFIG['export'].join(',');
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			window.open(api.getSettings().apiBaseUrl+"/transactions?"+config_str.join('&'));
			//api.GET("/products?"+config_str.join('&'),successExport,errorExport,$scope,{forceLive:true});
		}
	}]);
});