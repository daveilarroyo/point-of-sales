define(['config','api'], function (app,api) {
	app.register.service('cashflowService', ['$rootScope','$interval', function ($rootScope,$interval) {
		const ALLOW_CACHE = false;
		const CONFIG = {page:1,limit:2,keyword:null,fields:['entity_name'],'export':['timestamp','type','status','user','amount','entity_type','entity_id','Supplier.name','Cashflow.name'],filter:null,sort:'latest'};
		const EXPORT_FORMAT = 'csv';
		var __cashflows;
		var __callback ;
		var __successGetCashflows =   function(response,status,$scope){
			__cashflows = response;
			if(__callback) __callback(response,status,$scope);
		}
		this.addCashflow = function(successAddCashflow,errorAddCashflow,$scope,data){
			var cashflow = {};
				cashflow.header = data.header;
				cashflow.forceLive = true;
				$rootScope.cashflowInProgress = true;
				$rootScope.__Progress = 0;
				$interval(function(){
					if($rootScope.__Progress < 100)
						$rootScope.__Progress +=10;
				},300,5);
			api.POST("/cash_flows",
				function($response, $status, $scope){
					$rootScope.cashflowInProgress = false;
					$rootScope.__Progress = 100;
					successAddCashflow($response, $status, $scope);
				},
				function($response, $status, $scope){
					$rootScope.cashflowInProgress = false;
					$rootScope.__Progress = 100;
					errorAddCashflow($response, $status, $scope);
				},$scope,cashflow);
			
		};
		this.updateCashflow = function(successUpdateCashflow,errorUpdateCashflow,$scope){
			var cashflow = {};
			api.POST("/updateCashflow",successUpdateCashflow,errorUpdateCashflow,$scope,cashflow);
			
		};
		this.saveCashflow = function(successSaveCashflow,errorSaveCashflow,$scope,data){
			var cashflow = data;
				cashflow.forceLive=true;
			api.POST("/cash_flows",successSaveCashflow,errorSaveCashflow,$scope,cashflow);
		};
		this.cancelCashflow = function(successCancelCashflow,errorCancelCashflow,$scope,data){
			var cashflow = {};
				cashflow.header = data.header;
				cashflow.action = 'cancel';
				cashflow.forceLive = true;
				$rootScope.cashflowInProgress = true;
				$rootScope.__Progress = 0;
				$interval(function(){
					if($rootScope.__Progress < 100)
						$rootScope.__Progress +=10;
				},300,5);
			api.POST("/cash_flows",
				function($response, $status, $scope){
					$rootScope.cashflowInProgress = false;
					$rootScope.__Progress = 100;
					successCancelCashflow($response, $status, $scope);
				},
				function($response, $status, $scope){
					$rootScope.cashflowInProgress = false;
					$rootScope.__Progress = 100;
					errorCancelCashflow($response, $status, $scope);
				},$scope,cashflow);
			
		};
		this.getCashflow = function (successGetCashflow,errorGetCashflow,$scope,data){
			var cashflow = {};
				cashflow.type = data.type;
				cashflow.ref_no = data.ref_no;
				cashflow.forceLive = true;
			api.GET("/cash_flows",successGetCashflow,errorGetCashflow,$scope,cashflow);
		}
		this.getCashflows = function (successGetCashflows,errorGetCashflows,$scope,config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.page==undefined) config.page = CONFIG.page;
				if(config.limit==undefined) config.limit = CONFIG.limit;
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.keyword) config.fields = CONFIG.fields.join(',');
				if(config.filter.entity) delete config.filter.entity;
			}
			__callback = successGetCashflows;
			if(__cashflows&&ALLOW_CACHE){
				__callback(__cashflows,"00",$scope);
			}else{
				var config_str = [];
				for(var key in config){
					if(key!='page' && config[key] ){
						var value = config[key];
						if(typeof value == "object") value = JSON.stringify(value);
						config_str.push(key+'='+value);
					}
				}
				api.GET("/cash_flows?"+config_str.join('&')+"&page="+config.page,__successGetCashflows,errorGetCashflows,$scope,{forceLive:true});
			}
		}
		this.getCashflowByDescription= function (successGetCashflows,errorGetCashflows,$scope){
			api.GET("/getCashflowByDescription",successGetCashflows,errorGetCashflows,$scope);
		}
		this.exportData = function(config){
			if(config==undefined || typeof config != "object")  config = CONFIG;
			else{
				if(config.sort==undefined) config.sort = CONFIG.sort;
				if(config.format==undefined) config.format = EXPORT_FORMAT;
				if(config['export']==undefined) config['export'] = CONFIG['export'].join(',');
				if(config.keyword) config.fields = CONFIG.fields.join(',');
			}
			var config_str = [];
			for(var key in config){
				if(key!='page' && config[key] ){
					var value = config[key];
					if(typeof value == "object") value = JSON.stringify(value);
					config_str.push(key+'='+value);
				}
			}
			window.open(api.getSettings().apiBaseUrl+"/cashflows?"+config_str.join('&'));
			//api.GET("/products?"+config_str.join('&'),successExport,errorExport,$scope,{forceLive:true});
		}
	}]);
});