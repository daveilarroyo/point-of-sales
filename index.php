<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
    <title>EasySales</title>
    <meta name="description" content="e-Learning">
   	<meta name="viewport" content="user-scalable=no,width=device-width" />
	<script data-main="common/main.js<?php echo '?x='.rand();?>" src="libs/bower_components/requirejs/require.js"> </script>
	 <link rel="stylesheet" href="assets/css/bootstrap.min.css">
	 <link href="assets/css/simple-sidebar.css" rel="stylesheet">
	<link rel="stylesheet" href="assets/fonts/font-awesome-4.3.0/font-awesome.min.css">
	<link rel="stylesheet" href="libs/bower_components/angular-block-ui/dist/angular-block-ui.min.css" type="text/css"/>
	<link rel="stylesheet" href="libs/bower_components/angular-chart.js/dist/angular-chart.css" />
	<link href="assets/css/custom.css" rel="stylesheet">
	<link href="assets/css/login.css" rel="stylesheet" ng-if="!IsloggedIn">
</head>
<body class="bg-cover" ng-controller="indexController" ng-init="initializeController()" >
	<div id="bootstrap-message" ng-if="!IsInitialized"> Loading..</div>
	 <div id="wrapper" ng-class="{toggled: isToggled }"ng-controller="uiController" ng-init="initializeController()">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
				<li>
					<a href="#/"  >
						<i class="glyphicon glyphicon-home"></i>Home
					</a>
				</li>
				<li ng-repeat="module in Modules">
					<a ng-class={'active':activeModule==module.link} href="{{'#/'+module.link}}"  >
						<i ng-class="'glyphicon glyphicon-'+module.icon"></i>{{module.title}}
					</a>
				</li>
				<li>
					<a href="#/logout"  >
						<i class="glyphicon glyphicon-off"></i>Logout
					</a>
				</li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
		 <!-- Navbar -->
		<div class="navbar navbar-default navbar-fixed-top" id="navbar-toggle">
			<button type="button" class="navbar-toggle"  id="menu-toggle" ng-click="toggle()">
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand hide" ng-class="{hide: !User}">Hello, {{User.username}}!</a>
		</div>
		<div id="progress-bar" ng-if="__Progress" style="height:{{__Progress==100||__Progress==0? 0: 1}}rem;width:{{__Progress}}%;opacity:{{__Progress==100||__Progress==0? 0: 1}}"></div>
		<div class="alerts-tray" ng-show="alerts.length" ng-class="{'alerts-open' : IsInitialized}">
			<div class="alert alert-{{alert.type}}"  ng-repeat="alert in alerts" >
			  <button type="button" class="close" ng-click="closeAlert($index)"><span>&times;</span></button>
			  {{alert.msg}}
			</div>
		</div>
		<div class="page-content-blind"></div>
        <!-- Page Content -->
        <div id="page-content-wrapper">
			
            <div class="container-fluid">
                <div ng-view></div>
            </div>
				<!--
				<button type="button" class="btn btn-primary btn-md btn-fab bottom left"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
				<button type="button" class="btn btn-primary btn-md btn-fab bottom right"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
				<button type="button" class="btn btn-primary btn-md btn-fab top right"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
				-->
        </div>
        <!-- /#page-content-wrapper -->

    </div>
	<!-- Root Modal -->
	<script type="text/ng-template" id="rootModalContent.html">

		<div ng-controller="rootModalController" ng-init="initializeController()">
		
		  <div class="modal-header" ng-show="title">
			<h4 class="modal-title" >{{title}}</h4>
		  </div>
		  <div class="modal-body">
			<div class="text-center">
				<div ng-bind-html="renderHtml(message)"></div>
				<center><h4>{{time}}</h4></center>
			</div>
		  </div>
		<div class="modal-footer" ng-show="buttons.length">
			<div >
				<button class="btn" ng-repeat="btn in buttons" ng-click="btn.callback()">{{btn.label}}</button>
			</div>
		</div>
		  
		</div>
		
	</script>
    <!-- /#wrapper -->
</body>
</html>